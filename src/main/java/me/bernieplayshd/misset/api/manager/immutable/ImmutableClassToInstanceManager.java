package me.bernieplayshd.misset.api.manager.immutable;

import com.google.common.collect.ImmutableClassToInstanceMap;

import me.bernieplayshd.misset.api.manager.simple.SimpleManager;

public class ImmutableClassToInstanceManager<T> extends SimpleManager<ImmutableClassToInstanceMap<T>> {
	
	public ImmutableClassToInstanceManager(String name, ImmutableClassToInstanceMap<T> map) {
		super(name, map);
	}
	
	public ImmutableClassToInstanceManager(String name, ImmutableClassToInstanceMap.Builder<T> builder) {
		super(name, builder.build());
	}
	
	@Override
	public void setManaged(ImmutableClassToInstanceMap<T> managed) {
		throw new UnsupportedOperationException();
	}
}
