package me.bernieplayshd.misset.api.manager.immutable;

import com.google.common.collect.ImmutableMap;

import me.bernieplayshd.misset.api.manager.simple.SimpleManager;

public class ImmutableMapManager<K, V> extends SimpleManager<ImmutableMap<K, V>> {

	public ImmutableMapManager(String name, ImmutableMap<K, V> map) {
		super(name, map);
	}
	
	public ImmutableMapManager(String name, ImmutableMap.Builder<K, V> builder) {
		super(name, builder.build());
	}

	@Override
	public void setManaged(ImmutableMap<K, V> managed) {
		throw new UnsupportedOperationException();
	}
}
