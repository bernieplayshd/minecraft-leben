package me.bernieplayshd.misset.api.manager.simple;

import java.util.HashMap;
import java.util.Map;

public class SimpleMapManager<K, V> extends SimpleManager<Map<K, V>> {

	public SimpleMapManager(String name) {
		this(name, new HashMap<>());
	}
	
	public SimpleMapManager(String name, Map<K, V> elements) {
		super(name, elements);
	}
}
