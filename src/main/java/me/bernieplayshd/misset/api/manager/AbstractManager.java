package me.bernieplayshd.misset.api.manager;

import java.util.logging.Logger;

import me.bernieplayshd.misset.api.module.ToggleableModule;

public abstract class AbstractManager<T> extends ToggleableModule implements Manager<T> {

	private final String name;
	private final Logger logger;
	
	public AbstractManager(String name) {
		this.name   = name;
		this.logger = Logger.getLogger(name);
	}
	
	public String getName() {
		return name;
	}

	@Override
	public Logger getLogger() {
		return logger;
	}
}
