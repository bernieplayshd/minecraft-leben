package me.bernieplayshd.misset.api.manager.simple;

import me.bernieplayshd.misset.api.database.sql.SQLDatabase;

public class DatabaseManager<T> extends SimpleListManager<T> {
	
	private final SQLDatabase database;

	public DatabaseManager(String name, SQLDatabase database) {
		super(name);
		this.database = database;
	}
	
	protected SQLDatabase getDatabase() {
		return database;
	}
}
