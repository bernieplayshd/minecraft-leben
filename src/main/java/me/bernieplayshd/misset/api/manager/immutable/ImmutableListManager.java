package me.bernieplayshd.misset.api.manager.immutable;

import java.util.List;

import com.google.common.collect.ImmutableList;

import me.bernieplayshd.misset.api.manager.simple.SimpleListManager;

public class ImmutableListManager<T> extends SimpleListManager<T> {

	public ImmutableListManager(String name, ImmutableList<T> list) {
		super(name, list);
	}
	
	public ImmutableListManager(String name, ImmutableList.Builder<T> builder) {
		super(name, builder.build());
	}
	
	@Override
	public void setManaged(List<T> managed) {
		throw new UnsupportedOperationException();
	}
}
