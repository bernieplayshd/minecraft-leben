package me.bernieplayshd.misset.api.manager.simple;

import java.util.ArrayList;
import java.util.List;

/*
 * Idea: Add `onAdd` and `onRemove` events on List modification
 */
public class SimpleListManager<T> extends SimpleManager<List<T>> {

	public SimpleListManager(String name) {
		super(name, new ArrayList<T>());
	}
	
	public SimpleListManager(String name, List<T> list) {
		super(name, list);
	}
}
