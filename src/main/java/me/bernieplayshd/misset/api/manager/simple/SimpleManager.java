package me.bernieplayshd.misset.api.manager.simple;

import java.util.ArrayList;
import java.util.List;

import me.bernieplayshd.misset.plugin.MissetPlugin;
import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import me.bernieplayshd.misset.api.manager.AbstractManager;
import us.dev.api.command.Command;

public class SimpleManager<T> extends AbstractManager<T> {

	private final List<Listener> listeners = new ArrayList<>();
	private final List<Command>  commands  = new ArrayList<>();

	private T managed;

	public SimpleManager(String name, T elements) {
		super(name);
		this.managed = elements;
	}

	@Override
	public T getManaged() {
		return managed;
	}

	@Override
	public void setManaged(T managed) {
		this.managed = managed;
	}
	
	protected <X extends Command> void registerCommand(X command) {
		commands.add(command);
	}

	public List<Command> getCommands() {
		return commands;
	}
	
	protected <X extends Listener> void registerListener(X listener) {
		listeners.add(listener);
	}
	
	public List<Listener> getListeners() {
		return listeners;
	}

	@Override
	public void onEnable() {
		listeners.forEach(listener -> Bukkit.getServer().getPluginManager()
				.registerEvents(listener, MissetPlugin.getPlugin()));
		commands.forEach((command) -> MissetPlugin.getPlugin().getCommandManager().register(command));
	}

	@Override
	public void onDisable() {
		listeners.forEach(HandlerList::unregisterAll);
		commands.stream().map(cmd -> cmd.getLabel().toLowerCase()).forEach(name -> MissetPlugin.getPlugin()
				.getCommandManager().unregisterBukkit(name));
	}
}
