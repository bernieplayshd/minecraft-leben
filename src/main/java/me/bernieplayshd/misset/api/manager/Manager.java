package me.bernieplayshd.misset.api.manager;

public interface Manager<T> {

    /**
     * Returns the managed object(s)
     */
    T getManaged();

    /**
     * Sets the managed object(s)
     *
     * @throws UnsupportedOperationException If the {@link Manager} is immutable
     */
    void setManaged(T managed);
}
