package me.bernieplayshd.misset.api.event;

import org.bukkit.entity.Player;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.PlayerDeathEvent;

public class TestEvent {

    public static void test() {
        EventFunction<PlayerDeathEvent> function = EventFunctions.newMapped(PlayerDeathEvent.class, Player.class)
                .priority(EventPriority.NORMAL)
                .map(PlayerDeathEvent::getEntity)
                .filter(player -> "bernieplayshd".equals(player.getName()))
                .then(player -> player.sendMessage("Moin bernie"))
                .build();
    }
}
