package me.bernieplayshd.misset.api.event;

import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;

public interface EventFunction<T extends Event> {

    /**
     * Returns the priority in which the function should be executed
     */
    EventPriority getPriority();

    /**
     * Returns whether the function was executed
     */
    boolean execute(T event);

    interface Builder<T extends Event> {
        EventFunction<T> build();
    }
}
