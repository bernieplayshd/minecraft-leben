package me.bernieplayshd.misset.api.event;

import org.bukkit.event.Event;

public class EventFunctions {

    private EventFunctions() { }

    public static <T extends Event> EventFunctionImpl.Builder<T, T> newSimple(Class<T> of) {
        return new EventFunctionImpl.Builder<>();
    }

    public static <T extends Event, F> EventFunctionImpl.Builder<T, F> newMapped(Class<T> of, Class<F> to) {
        return new EventFunctionImpl.Builder<>();
    }
}
