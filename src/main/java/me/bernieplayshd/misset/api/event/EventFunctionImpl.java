package me.bernieplayshd.misset.api.event;

import org.bukkit.event.Event;
import org.bukkit.event.EventPriority;

import java.util.*;
import java.util.function.BiConsumer;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;

public class EventFunctionImpl<T extends Event, F> implements EventFunction<T> {

    private final EventPriority priority;

    private final Map<Predicate<F>, Boolean> filters;

    private final Function<T, F> mappingFunction;

    private final BiConsumer<T, F> onExecute;

    @SuppressWarnings("unchecked")
    public EventFunctionImpl(EventPriority priority, Map<Predicate<F>, Boolean> filters, Consumer<T> exec) {
        this.priority = priority;
        this.mappingFunction = event -> (F) event;
        this.onExecute = (event, __) -> exec.accept(event);
        this.filters = filters;
    }

    public EventFunctionImpl(EventPriority priority, Function<T, F> mappingFunc,
                             Map<Predicate<F>, Boolean> filters, BiConsumer<T, F> onExecute)
    {
        this.priority = priority;
        this.mappingFunction = mappingFunc;
        this.onExecute = onExecute;
        this.filters = filters;
    }

    @Override
    public EventPriority getPriority() {
        return priority;
    }

    @Override
    public boolean execute(T event) {
        F mapped = mappingFunction.apply(event);
        boolean cancelEvent = filters.keySet().stream()
                .filter(tester -> tester.test(mapped))
                .findAny()
                .map(filters::get)
                .orElse(false);

        if (!cancelEvent) {
            onExecute.accept(event, mapped);
            return false;
        }
        return true;
    }

    public static class Builder<T extends Event, F> implements EventFunction.Builder<T> {
        private EventPriority prior = EventPriority.NORMAL;
        private Map<Predicate<F>, Boolean> filters = new HashMap<>();
        @SuppressWarnings("unchecked")
        private Function<T, F> mapFunc = event -> (F) event;
        private BiConsumer<T, F> onExec = (event, __) -> {};

        public Builder<T, F> priority(EventPriority priority) {
            this.prior = priority;
            return this;
        }

        public Builder<T, F> filter(Predicate<F> filter) {
            filters.put(filter, false);
            return this;
        }

        public Builder<T, F> filter(Predicate<F> filter, boolean cancel) {
            filters.put(filter, cancel);
            return this;
        }

        public Builder<T, F> map(Function<T, F> mappingFunction) {
            this.mapFunc = mappingFunction;
            return this;
        }

        public Builder<T, F> then(BiConsumer<T, F> onExecute) {
            this.onExec = onExecute;
            return this;
        }

        public Builder<T, F> then(Consumer<F> onExecute) {
            this.onExec = (__, mapped) -> onExecute.accept(mapped);
            return this;
        }

        public EventFunctionImpl<T, F> build() {
            return new EventFunctionImpl<T, F>(prior, mapFunc, filters, onExec);
        }
    }
}
