package me.bernieplayshd.misset.api.database;

import javax.annotation.Nullable;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public interface DatabaseListener {

    void onConnect();

    void onDisconnect();

    void onTransaction(PreparedStatement statement);

    void onTransactionError(@Nullable PreparedStatement statement, SQLException exception);
}
