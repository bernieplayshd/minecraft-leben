package me.bernieplayshd.misset.api.database.sql.impl;

import com.google.common.collect.EvictingQueue;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.pool.HikariProxyPreparedStatement;
import me.bernieplayshd.misset.api.database.DatabaseListener;
import me.bernieplayshd.misset.api.database.sql.SQLCredentials;
import me.bernieplayshd.misset.api.database.sql.SQLDatabase;
import me.bernieplayshd.misset.api.lang.ThrowingFunction;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.*;
import java.util.function.Supplier;

public class MySQLDatabase implements SQLDatabase {

    private static final int MAX_FUTURES_IN_CACHE = 10;

    private final SQLCredentials credentials;

    private final DatabaseListener listener;

    private HikariDataSource dataSource;

    private final ExecutorService executorService;
    private final Queue<Future<?>> tasks = EvictingQueue.create(MAX_FUTURES_IN_CACHE);

    /**
     * A global instance of my local MySQL server
     */
    public static final MySQLDatabase LOCALHOST = new MySQLDatabase(
            new SQLCredentials.Builder()
                .of("localhost")
                .on(3306)
                .database("misset-test")
                .user("root")
                .password("")
            .build()
    );

    /**
     * Builds a {@link HikariDataSource} via the {@link SQLCredentials} it's given
     */
    private HikariDataSource buildDataSource() {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(String.format("jdbc:mysql://%s:%d/%s", credentials.getHost(),
                credentials.getPort(), credentials.getDatabase()));
        config.setUsername(credentials.getUser());
        config.setPassword(credentials.getPassword());
        config.setMaximumPoolSize(10);
        config.setLeakDetectionThreshold(10_000L);

        /*
         * DataSource Properties optimized for MySQL from
         * https://github.com/brettwooldridge/HikariCP/wiki/MySQL-Configuration
         */
        Properties dataSource = new Properties();
        dataSource.setProperty("cachePrepStmts", "true");
        dataSource.setProperty("prepStmtCacheSize", "250");
        dataSource.setProperty("prepStmtCacheSqlLimit", "2048");
        dataSource.setProperty("useServerPrepStmts", "true");
        dataSource.setProperty("useLocalSessionState", "true");
        dataSource.setProperty("rewriteBatchedStatements", "true");
        dataSource.setProperty("cacheResultSetMetadata", "true");
        dataSource.setProperty("cacheServerConfiguration", "true");
        dataSource.setProperty("elideSetAutoCommits", "true");
        dataSource.setProperty("maintainTimeStats", "false");

        // Don't use SSL to suppress warnings
        dataSource.setProperty("useSSL", "false");
        // Auto reconnect in case of a fatal failure
        dataSource.setProperty("autoReconnect", "true");

        config.setDataSourceProperties(dataSource);

        return new HikariDataSource(config);
    }

    public MySQLDatabase(SQLCredentials credentials) {
        this(credentials, null);
    }

    public MySQLDatabase(SQLCredentials credentials, DatabaseListener listener) {
        this(Executors.newCachedThreadPool(), credentials, listener);
    }

    public MySQLDatabase(ExecutorService execService, SQLCredentials credentials, DatabaseListener listener) {
        this.executorService = execService;
        this.credentials = credentials;
        this.listener = listener;
    }

    /**
     * Returns the Executor Service which runs our SQL queries
     */
    public ExecutorService getExecutorService() {
        return executorService;
    }

    /**
     * Returns a (probably new) connection from the connection pool
     *
     * @throws SQLException If a connection could not be established
     */
    @Override
    public Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    /**
     * Builds the {@link HikariDataSource}, connects to our server and sets all tables up
     */
    public void connect() {
        if (dataSource == null) {
            this.dataSource = buildDataSource();
        } else if (!dataSource.isRunning()) {
            HikariConfig newConfig = new HikariConfig();
            dataSource.copyStateTo(newConfig);
            this.dataSource = new HikariDataSource(newConfig);
        }
        if (listener != null) {
            listener.onConnect();
        }
        setupTables();
    }

    public void disconnect() {
        try {
            // Wait for the pending tasks to execute
            // before closing the connection to the database server
            for (Future<?> future : tasks) {
                if (!future.isDone()) {
                    future.get(5L, TimeUnit.SECONDS);
                }
            }
            tasks.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (listener != null) {
            listener.onDisconnect();
        }
        dataSource.close();
    }

    @Override
    public Supplier<String[]> getTableSetupStatements() {
        return DefaultTableProvider.INSTANCE;
    }

    /**
     * Returns a {@link PreparedStatement} from the query statement and the parameters
     *
     * @param sql The plain sql query
     * @param params The parameters for the query
     *
     * @throws SQLException If the statement could not be prepared
     */
    private PreparedStatement prepareStatement(String sql, Object ... params) throws SQLException {
        HikariProxyPreparedStatement prepared = (HikariProxyPreparedStatement) getConnection().prepareStatement(sql);
        for (int x = 0; x < params.length; x++) {
            prepared.setObject(x + 1, params[x]);
        }
        return prepared;
    }

    /**
     * Returns the Callable which the ExecutorService processes
     *
     * @param toComplete The future which gets completed after execution
     * @param execFunction The function which processes the statement
     * @param statement The prepared statement gets processed
     * @param <T> The type of the future's end result
     *
     * @return A Callable to submit to an {@link ExecutorService}
     */
    private <T> Callable<Void> toCallable(
            CompletableFuture<T> toComplete,
            ThrowingFunction<PreparedStatement, T, SQLException> execFunction,
            PreparedStatement statement)
    {
        return () -> {
            try {
                toComplete.complete(execFunction.apply(statement));
                if (listener != null) {
                    listener.onTransaction(statement);
                }
                return null;
            } catch (SQLException e) {
                if (listener != null) {
                    listener.onTransactionError(statement, e);
                }
                return null;
            } finally {
                // abandon the connection from the pool
                statement.getConnection().close();
            }
        };
    }

    /**
     * Executes a SQL query via the {@code execFunction} and returns a Future
     */
    private <T> CompletableFuture<T> executeQuery(
            ThrowingFunction<PreparedStatement, T, SQLException> execFunction,
            String statement, Object ... params)
    {
        CompletableFuture<T> future = new CompletableFuture<>();
        try {
            // Prepare the statement, ...
            PreparedStatement stmt = prepareStatement(statement, params);
            // and send a Callable which executes it to our Executor Service
            tasks.add(executorService.submit(toCallable(future, execFunction, stmt)));
        } catch (SQLException e) {
            // abort and mark the Future as failed
            future.completeExceptionally(e);
            if (listener != null) {
                listener.onTransactionError(null, e);
            }
        }
        return future;
    }

    @Override
    public CompletableFuture<Integer> update(String statement, Object ... params) {
        return executeQuery(PreparedStatement::executeUpdate, statement, params);
    }

    /**
     * <b>Note:</b> Remember to manually close the ResultSet or/and it's Statement
     */
    @Override
    public CompletableFuture<ResultSet> querySet(String statement, Object ... params) {
        return executeQuery(PreparedStatement::executeQuery, statement, params);
    }

    @Override
    public <T> CompletableFuture<T> querySet(
            String statement,
            ThrowingFunction<ResultSet, T, SQLException> mappingFunc,
            Object ... params)
    {
        return executeQuery(stmt -> mappingFunc.apply(stmt.executeQuery()), statement, params);
    }

    /**
     * Returns all fetched values of the columns from a row
     */
    private Object[] getRow(ResultSet resultSet) throws SQLException {
        try {
            final int fetchSize = resultSet.getFetchSize();
            Object[] out = new Object[fetchSize];

            for (int x = 0; x < fetchSize; x++) {
                out[x] = resultSet.getObject(x);
            }
            return out;
        } finally {
            resultSet.getStatement().close();
        }
    }

    @Override
    public CompletableFuture<Object[]> query(String statement, Object ... params) {
        return executeQuery(stmt -> getRow(stmt.executeQuery()), statement, params);
    }

    /**
     * Returns all fetched values of the columns from every row
     */
    private Object[][] getRows(ResultSet resultSet) throws SQLException {
        try {
            List<Object[]> out = new LinkedList<>();
            while (resultSet.next()) {
                final int columns = resultSet.getMetaData().getColumnCount();
                Object[] row = new Object[columns];

                for (int x = 0; x < columns; x++) {
                    row[x] = resultSet.getObject(x + 1);
                }
                out.add(row);
            }
            return out.toArray(new Object[out.size()][]);
        } finally {
            resultSet.getStatement().close();
        }
    }

    @Override
    public CompletableFuture<Object[][]> queryRows(String statement, Object ... params) {
        return executeQuery(stmt -> getRows(stmt.executeQuery()), statement, params);
    }

    /**
     * Class providing the default SQL table creation statements
     */
    private static class DefaultTableProvider implements Supplier<String[]> {

        public static final DefaultTableProvider INSTANCE = new DefaultTableProvider();

        private static final String[] CREATE_STATEMENTS = {
                "CREATE TABLE IF NOT EXISTS `spawners` ("
                        + " `id` int(11) NOT NULL AUTO_INCREMENT,"
                        + " `player_uuid` char(36) NOT NULL,"
                        + " `name` varchar(16) NOT NULL,"
                        + " `blockx` int(11) NOT NULL,"
                        + " `blocky` int(11) NOT NULL,"
                        + " `blockz` int(11) NOT NULL,"
                        + " `block_world` text NOT NULL,"
                        + " PRIMARY KEY(`id`))"
                        + " DEFAULT CHARSET = utf8mb4;",
                "CREATE TABLE IF NOT EXISTS `backpacks` ("
                        + " `id` int(11) NOT NULL AUTO_INCREMENT,"
                        + " `player_uuid` char(36) NOT NULL,"
                        + " `type` int(1) NOT NULL,"
                        // + " `type` enum('Small', 'Medium', 'Large') NOT NULL,"
                        + " `content` text NOT NULL,"
                        + " `name` varchar(16),"
                        + " PRIMARY KEY(`id`))"
                        + " DEFAULT CHARSET = utf8mb4;",
                "CREATE TABLE IF NOT EXISTS `reports` ("
                        + " `id` int(11) NOT NULL AUTO_INCREMENT,"
                        + " `reported` char(36) NOT NULL,"
                        + " `sender`   char(36) NOT NULL,"
                        // TODO: maybe also use an enum type?
                        + " `template` int(2) NOT NULL,"
                        + " `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,"
                        + " `finished` boolean DEFAULT FALSE,"
                        + " PRIMARY KEY(`id`))"
                        + " DEFAULT CHARSET = utf8mb4;"
        };

        @Override
        public String[] get() {
            return CREATE_STATEMENTS;
        }
    }
}
