package me.bernieplayshd.misset.api.database.sql;

import me.bernieplayshd.misset.api.lang.ThrowingFunction;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

public interface SQLDatabase {

    /**
     * Returns a SQL connection
     *
     * @throws SQLException If a connection could not be established
     */
    Connection getConnection() throws SQLException;

    /**
     * Executes an Update on a SQL query and returns a Future
     *
     * @param statement The plain SQL query
     * @param params The parameters for the query
     */
    CompletableFuture<Integer> update(String statement, Object ... params);

    /**
     * Executes a SQL query and returns a {@link ResultSet} within a Future
     *
     * @param statement The plain SQL query
     * @param params The parameters for the query
     */
    CompletableFuture<ResultSet> querySet(String statement, Object ... params);

    /**
     * Executes a SQL query and maps the result via the mapping function
     *
     * @param statement The plain SQL query
     * @param mappingFunc The mapping function
     * @param params The parameters for the query
     * @param <T> The type of the result from the mapping function
     *
     * @see #querySet(String, Object...)
     */
    <T> CompletableFuture<T> querySet(
            String statement,
            ThrowingFunction<ResultSet, T, SQLException> mappingFunc,
            Object ... params);

    /**
     * Executes a SQL query and returns a single row with its columns
     *
     * @param statement The plain SQL query
     * @param params The parameters for the query
     */
    CompletableFuture<Object[]> query(String statement, Object ... params);

    /**
     * Executes a SQL query and returns all rows with their columns
     *
     * @param statement The plain SQL query
     * @param params The parameters for the query
     *
     * @see #query(String, Object...)
     */
    CompletableFuture<Object[][]> queryRows(String statement, Object ... params);

    /**
     * Provides the SQL (create table) statements used to setup the required tables
     */
    Supplier<String[]> getTableSetupStatements();

    /**
     * Creates all required database tables
     */
    default void setupTables() {
        Arrays.stream(getTableSetupStatements().get()).forEach(this::update);
    }
}
