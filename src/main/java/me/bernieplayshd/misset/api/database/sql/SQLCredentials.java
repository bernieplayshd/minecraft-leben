package me.bernieplayshd.misset.api.database.sql;

public class SQLCredentials {

    private final String host, database, user, password;
    private final int port;

    public SQLCredentials(String host, int port, String database, String user, String password) {
        this.host = host;
        this.port = port;
        this.database = database;
        this.user = user;
        this.password = password;
    }

    public String getHost() {
        return host;
    }

    public int getPort() {
        return port;
    }

    public String getDatabase() {
        return database;
    }

    public String getUser() {
        return user;
    }

    public String getPassword() {
        return password;
    }

    public static class Builder {
        private String host = "localhost", db = "", user = "root", password = "";
        private int port = 3306;

        public Builder of(String host) {
            this.host = host;
            return this;
        }

        public Builder on(int port) {
            this.port = port;
            return this;
        }

        public Builder database(String db) {
            this.db = db;
            return this;
        }

        public Builder user(String user) {
            this.user = user;
            return this;
        }

        public Builder password(String password) {
            this.password = password;
            return this;
        }

        public SQLCredentials build() {
            return new SQLCredentials(host, port, db, user, password);
        }
    }
}
