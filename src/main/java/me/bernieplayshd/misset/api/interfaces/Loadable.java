package me.bernieplayshd.misset.api.interfaces;

public interface Loadable {
	
	void load();
	
	void unload();

	boolean isLoaded();

	default void reload() {
		unload();
		load();
	}
}
