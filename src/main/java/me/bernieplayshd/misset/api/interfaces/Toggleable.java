package me.bernieplayshd.misset.api.interfaces;

public interface Toggleable {
	
	void onEnable();
	
	void onDisable();
	
	
	boolean isRunning();
	
	void setRunning(boolean running);
	
	default void toggle() {
		setRunning(!isRunning());
	}
}
