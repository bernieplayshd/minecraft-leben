package me.bernieplayshd.misset.api.lang;

@FunctionalInterface
public interface ThrowingFunction<T, R, E extends Throwable> {

    R apply(T arg) throws E;

    interface Anything<T, R> extends ThrowingFunction<T, R, Throwable> { }
}
