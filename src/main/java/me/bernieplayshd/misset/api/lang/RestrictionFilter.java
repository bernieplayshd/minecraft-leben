package me.bernieplayshd.misset.api.lang;

import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Predicate;

public interface RestrictionFilter<T, F> {

    Predicate<F> getFilter();

    Optional<Consumer<T>> getMethod();
}
