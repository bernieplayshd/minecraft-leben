package me.bernieplayshd.misset.api.module.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.function.BiConsumer;

import me.bernieplayshd.misset.api.module.ModulePriority;
import org.bukkit.entity.Player;

import me.bernieplayshd.misset.api.module.AbstractModule;
import me.bernieplayshd.misset.api.module.ToggleableModule;

@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ModuleData {

	String name();
	
	String description() default "";
	
	Class<? extends BiConsumer<Player, Boolean>> onToggle() default ToggleableModule.DefaultToggleConsumer.class;
	
	Class<? extends AbstractModule>[] requires() default {};

	ModulePriority priority() default ModulePriority.NORMAL;
}
