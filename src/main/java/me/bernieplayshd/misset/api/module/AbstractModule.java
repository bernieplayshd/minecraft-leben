package me.bernieplayshd.misset.api.module;

import java.util.logging.Logger;

import me.bernieplayshd.misset.api.module.annotations.ModuleData;

public class AbstractModule implements Module {
	
	private final Logger logger;
	
	private final String name, description;
	private final Class<? extends AbstractModule>[] dependencies;
	private final ModulePriority priority;

	public AbstractModule() {
		if (!getClass().isAnnotationPresent(ModuleData.class)) {
			throw new RuntimeException("Could not load AbstractModule because ModuleData is not present");
		}
		ModuleData data = getClass().getAnnotation(ModuleData.class);
		this.name = data.name();
		this.description = data.description();
		this.dependencies = data.requires();
		this.priority = data.priority();
		
		this.logger = Logger.getLogger(name);
	}
	
	public Logger getLogger() {
		return logger;
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public String getDescription() {
		return description;
	}

	@Override
	public Class<? extends AbstractModule>[] getDependencies() {
		return dependencies;
	}

	@Override
	public ModulePriority getPriority() {
		return priority;
	}
}
