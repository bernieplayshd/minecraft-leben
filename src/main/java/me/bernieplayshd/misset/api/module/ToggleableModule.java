package me.bernieplayshd.misset.api.module;

import java.util.function.BiConsumer;
import java.util.logging.Level;

import javax.annotation.Nullable;

import org.bukkit.entity.Player;

import me.bernieplayshd.misset.api.interfaces.Toggleable;
import me.bernieplayshd.misset.api.module.annotations.ModuleData;
import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.manager.impl.internal.ConfigManager;

/**
 * This class enables a {@link Module} to be enabled/disabled
 */
public abstract class ToggleableModule extends AbstractModule implements Toggleable {
	
	private BiConsumer<Player, Boolean> toggleConsumer;
	
	private boolean running;
	
	public ToggleableModule() {
		super();
		
		ModuleData data = getClass().getAnnotation(ModuleData.class);
		try {
			this.toggleConsumer = data.onToggle().newInstance();
		} catch (InstantiationException | IllegalAccessException e) {
			getLogger().log(Level.WARNING, "Failed to use custom toggle consumer, falling back...", e);
			this.toggleConsumer = DefaultToggleConsumer.INSTANCE;
		}
	}

	@Override
	public boolean isRunning() {
		return running;
	}

	@Override
	public void setRunning(boolean running) {
		setRunning(running, null);
	}
	
	public void setRunning(boolean running, @Nullable Player player) {
		ConfigManager configManager = MissetPlugin.getPlugin().getConfigManager();
		if (running) {
			if (!configManager.getManaged().containsKey(this)) {
				configManager.load(this);
			}
			this.running = true;
			onEnable();
		} else {
			if (configManager.getManaged().containsKey(this)) {
				configManager.unload(this);
			}
			this.running = false;
			onDisable();
		}
		
		if (player != null) {
			toggleConsumer.accept(player, running);
		}
	}

	public static class DefaultToggleConsumer implements BiConsumer<Player, Boolean> {

		public static final DefaultToggleConsumer INSTANCE = new DefaultToggleConsumer();

		@Override
		public void accept(Player player, Boolean state) {
			player.sendMessage(MissetPlugin.PREFIX + (state ? "§aEnabled" : "§cDisabled")
					+ " module §9" + ToggleableModule.class.getSimpleName());
		}
	}
}
