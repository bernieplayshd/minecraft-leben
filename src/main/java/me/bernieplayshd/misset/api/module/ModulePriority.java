package me.bernieplayshd.misset.api.module;

public enum ModulePriority {
    LOWEST,
    LOW,
    NORMAL,
    HIGH,
    HIGHEST,
    MONITOR
}
