package me.bernieplayshd.misset.api.module;

public interface Module {

    /**
     * Returns the name of the module
     */
    String getName();

    /**
     * Returns the description of the module
     */
    String getDescription();

    /**
     * Returns other modules on which the module depends
     */
    Class<? extends Module>[] getDependencies();

    /**
     * Returns the priority of the module (default = normal)
     */
    ModulePriority getPriority();
}
