package me.bernieplayshd.misset.api.item;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemBuilder {

	private ItemBuilder() { }
	
	private static ItemStack combine(ItemStack stack, ItemMeta meta) {
		stack.setItemMeta(meta);
		return stack;
	}
	
	public static ItemStack build(Material type, String name) {
		return build(type, 1, name);
	}
	
	public static ItemStack build(Material type, int amount, String name) {
		return build(type, amount, (short) 0, name);
	}
	
	public static ItemStack build(Material type, String name, String ... lore) {
		return build(type, 1, (short) 0, name, lore);
	}
	
	public static ItemStack build(Material type, int amount, short damage, String name, String ... lore) {
		ItemStack stack = new ItemStack(type, amount, damage);
		
		ItemMeta meta = stack.getItemMeta();
		meta.setDisplayName(name);
		if (lore.length > 0) {
			meta.setLore(Arrays.asList(lore));
		}
		
		return combine(stack, meta);
	}
	
	public static ItemStack build(Material type, int amount, short damage, ItemMeta meta) {
		ItemStack stack = new ItemStack(type, amount, damage);
		return combine(stack, meta);
	}
}
