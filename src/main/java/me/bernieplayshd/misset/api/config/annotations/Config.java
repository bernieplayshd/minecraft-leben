package me.bernieplayshd.misset.api.config.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Config {

	String name() default "";
	
	String[] desc() default {};
	
	/**
	 * Indicates whether the setting should be {@code final}
	 */
	boolean constant() default false;
}
