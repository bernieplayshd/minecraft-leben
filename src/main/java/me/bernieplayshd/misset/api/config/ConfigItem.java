package me.bernieplayshd.misset.api.config;

import java.lang.reflect.Field;

import me.bernieplayshd.misset.api.module.Module;

public class ConfigItem {
	
	private final Module module;
	private final Field field;
	private final String name, description[];
	
	public ConfigItem(Module module, Field field, String name) {
		this(module, field, name, new String[0]);
	}
	
	public ConfigItem(Module module, Field field, String name, String[] desc) {
		this.module = module;
		this.name = name;
		this.field = field;
		this.description = desc;
	}

	/**
	 * Returns the module of the Config Item
	 */
	public Module getModule() {
		return module;
	}

	/**
	 * Returns the Reflection Field to the Module holding it
	 */
	public Field getField() {
		return field;
	}

	/**
	 * Returns the value of the field
	 *
	 * @throws Exception If there was an Access Error
	 */
	public Object getValue() throws Exception {
		return field.get(module);
	}

	/**
	 * Sets the fields value to the input
	 *
	 * @param value The new value of the field
	 *
	 * @throws Exception If there was an Access Error
	 */
	public void setValue(Object value) throws Exception {
		field.set(module, value);
	}

	/**
	 * Returns the name of the Config Item
	 */
	public String getName() {
		return name;
	}

	/**
	 * Returns the (multi-line) description of the Config Item
	 */
	public String[] getDescription() {
		return description;
	}
}
