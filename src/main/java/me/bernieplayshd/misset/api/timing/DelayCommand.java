package me.bernieplayshd.misset.api.timing;

import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.manager.impl.internal.CommandManager;
import org.bukkit.command.CommandSender;
import us.dev.api.command.Command;

import javax.annotation.Nullable;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

public abstract class DelayCommand extends Command {
    // maybe fallback to uuid, with random for non players, when failing
    private final Delay<CommandSender> delay;

    public DelayCommand(CommandManager manager, String label, long delay) {
        this(manager, label, null, delay, DEFAULT_MESSAGE);
    }

    public DelayCommand(CommandManager manager, String label, @Nullable String permission, long delay) {
        this(manager, label, permission, delay, DEFAULT_MESSAGE);
    }

    public DelayCommand(CommandManager manager, String label, @Nullable String permission,
                        long delay, Predicate<CommandSender> filter)
    {
        this(manager, label, permission, delay, filter, DEFAULT_MESSAGE);
    }

    public DelayCommand(CommandManager manager, String label, @Nullable String permission,
                        long delay, BiConsumer<CommandSender, Long> consumer)
    {
        this(manager, label, permission, delay, PERMISSION_FILTER, consumer);
    }

    public DelayCommand(CommandManager manager, String label, @Nullable String permission,
                        long delay, Predicate<CommandSender> filter, BiConsumer<CommandSender, Long> consumer)
    {
        super(manager, label, permission);
        this.delay = new Delay<>(delay, filter, consumer);
    }

    public Delay<CommandSender> getDelay() {
        return delay;
    }

    /**
     * @see Delay#checkCooldown(Object)
     */
    protected boolean checkCooldown(CommandSender sender) {
        return getDelay().checkCooldown(sender);
    }

    public static final Predicate<CommandSender> PERMISSION_FILTER = sender -> sender.hasPermission("system.admin");

    public static final BiConsumer<CommandSender, Long> DEFAULT_MESSAGE
            = (sender, delay) -> sender.sendMessage(String.format(MissetPlugin.DELAY, 1000L > delay ? 1 : delay / 1000));
}
