package me.bernieplayshd.misset.api.timing;

import java.util.HashMap;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Predicate;

public class Delay<T> {

    private long delay;

    private final Map<T, Long> delays = new HashMap<>();

    private final Predicate<T> toExclude;
    private final BiConsumer<T, Long> onDelay;

    /**
     * Constructs a new Delay
     *
     * @param delay The time in <b>ms</b>, what is the delay
     * @param toExclude The filter which checks if the entity should be excluded
     * @param consumer The consumer which executes on delay
     */
    public Delay(long delay, Predicate<T> toExclude, BiConsumer<T, Long> consumer) {
        this.delay = delay;
        this.toExclude = toExclude;
        this.onDelay = consumer;
    }

    public long getDelay() {
        return delay;
    }

    public void setDelay(long delay) {
        this.delay = delay;
    }

    public Map<T, Long> getDelays() {
        return delays;
    }

    /**
     * Checks if the entity has a cooldown
     *
     * @param entity The entity to be checked
     *
     * @return If the there is no delay active for the entity
     */
    public boolean checkCooldown(T entity) {
        if (delays.containsKey(entity)) {
            // Time, when delay is completed
            long time = delays.get(entity) + delay;
            if (time > System.currentTimeMillis()) {
                // -> it is in the future
                onDelay.accept(entity, time - System.currentTimeMillis());
                return true;
            }
            // -> it is in the past
            delays.remove(entity);
        } else if (!toExclude.test(entity)) {
            delays.put(entity, System.currentTimeMillis());
        }
        return false;
    }

    public static class Builder<T> {
        private long delay = 0L;
        private Predicate<T> toExclude = entity -> false;
        private BiConsumer<T, Long> onDelay = (entity, delay) -> {};

        public Builder<T> of(long delay) {
            this.delay = delay;
            return this;
        }

        public Builder<T> exclude(Predicate<T> toExclude) {
            this.toExclude = toExclude;
            return this;
        }

        public Builder<T> on(BiConsumer<T, Long> consumer) {
            this.onDelay = consumer;
            return this;
        }

        public Delay<T> build() {
            return new Delay<>(delay, toExclude, onDelay);
        }
    }
}
