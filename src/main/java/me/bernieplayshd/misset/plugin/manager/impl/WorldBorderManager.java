package me.bernieplayshd.misset.plugin.manager.impl;

import java.util.Arrays;
import java.util.List;

import me.bernieplayshd.misset.api.config.annotations.Config;
import me.bernieplayshd.misset.api.manager.simple.SimpleListManager;
import me.bernieplayshd.misset.api.module.annotations.ModuleData;
import me.bernieplayshd.misset.plugin.manager.impl.internal.RegisterManager;
import me.bernieplayshd.misset.plugin.worldborder.RainbowBorder;

@ModuleData(name = "WorldBorder", requires = RegisterManager.class)
public class WorldBorderManager extends SimpleListManager<RainbowBorder> {
	
	/*@Config
	private String borderWorld = "world";
	@Config
	private double borderSize = 1600.0D; //20_0000
	@Config
	private List<Double> borderCenter = Arrays.asList(32.5D, 172.5D); //-7224, -13318*/
	
	@Config
	private List<String> worldBorders = Arrays.asList(
				"world;79.5;169.5;750.0"
				//"Stadt;20000.0;-7224.0;-13318.0"
			);
	
	@Config
	private long period   =  10L,
				 moveTime = 500L;

	public WorldBorderManager() {
		super("WorldBorder Manager");
	}

	@Override
	public void onEnable() {
		super.onEnable();
		for (String serialized : worldBorders) {
			String[] splitted = serialized.split(";");
			getManaged().add(new RainbowBorder(splitted[0], Double.parseDouble(splitted[2]),
					Double.parseDouble(splitted[3]), Double.parseDouble(splitted[1])));
		}
		getManaged().forEach(border -> border.onEnable(moveTime, period));
	}
	
	@Override
	public void onDisable() {
		getManaged().forEach(RainbowBorder::onDisable);
		getManaged().clear();
		super.onDisable();
	}
}
