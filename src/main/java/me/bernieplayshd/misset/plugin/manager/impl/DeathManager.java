package me.bernieplayshd.misset.plugin.manager.impl;

import net.minecraft.server.v1_14_R1.ChatMessage;
import net.minecraft.server.v1_14_R1.EntityPlayer;
import net.minecraft.server.v1_14_R1.IChatBaseComponent;
import org.bukkit.craftbukkit.v1_14_R1.entity.CraftPlayer;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;

import com.google.common.collect.ImmutableMap;

import me.bernieplayshd.misset.api.manager.immutable.ImmutableMapManager;
import me.bernieplayshd.misset.api.module.annotations.ModuleData;
import me.bernieplayshd.misset.plugin.manager.impl.internal.RegisterManager;

import java.util.stream.Collectors;
import java.util.stream.Stream;

@ModuleData(name = "Deaths", requires = RegisterManager.class)
public class DeathManager extends ImmutableMapManager<String, String> {

	public DeathManager() {
		super("Death Manager",
				new ImmutableMap.Builder<String, String>()
					.put("death.attack.anvil", "%1$s wurde von einem fallenden Amboss zerquetscht")
					.put("death.attack.anvil.player", "%1$s wurde während des Kampfes mit %2$s von einem fallenden Amboss zerquetscht")
					.put("death.attack.arrow", "%1$s wurde von %2$s erschossen")
					.put("death.attack.arrow.item", "%1$s wurde von %2$s mit %3$s erschossen")
					.put("death.attack.cactus", "%1$s wurde zu Tode gestochen")
					.put("death.attack.cactus.player", "%1$s rannte beim Versuch, %2$s zu entkommen, in einen Kaktus")
					.put("death.attack.cramming", "%1$s wurde zerquetscht")
					.put("death.attack.cramming.player", "%1$s wurde von %2$s zerquetscht")
					.put("death.attack.dragonBreath", "%1$s wurde im Drachenatem geröstet")
					.put("death.attack.dragonBreath.player", "%1$s wurde durch %2$s in Drachenatem geröstet")
					.put("death.attack.drown", "%1$s ertrank")
					.put("death.attack.drown.player", "%1$s ertrank beim Versuch, %2$s zu entkommen")
					.put("death.attack.even_more_magic", "%1$s wurde durch verstärkte Magie getötet")
					.put("death.attack.explosion", "%1$s wurde in die Luft gesprengt")
					.put("death.attack.explosion.player", "%1$s wurde durch %2$s in die Luft gesprengt")
					.put("death.attack.explosion.player.item", "%1$s wurde von %2$s mit %3$s in die Luft gesprengt")
					.put("death.attack.fall", "%1$s fiel der Schwerkraft zum Opfer")
					.put("death.attack.fall.player", "%1$s fiel beim Versuch, %2$s zu entkommen, der Schwerkraft zum Opfer")
					.put("death.attack.fallingBlock", "%1$s wurde von einem fallenden Block zerquetscht")
					.put("death.attack.fallingBlock.player", "%1$s wurde während des Kampfes mit %2$s von einem fallenden Block zerquetscht")
					.put("death.attack.fireball", "%1$s wurde von %2$s flambiert")
					.put("death.attack.fireball.item", "%1$s wurde von %2$s mit %3$s flambiert")
					.put("death.attack.fireworks", "%1$s flog mit einem Knall in die Luft")
					.put("death.attack.fireworks.player", "%1$s flog während des Kampfes mit %2$s mit einem Knall in die Luft")
					.put("death.attack.flyIntoWall", "%1$s erfuhr kinetische Energie")
					.put("death.attack.flyIntoWall.player", "%1$s erfuhr beim Versuch, %2$s zu entkommen, kinetische Energie")
					.put("death.attack.generic", "%1$s starb")
					.put("death.attack.generic.player", "%1$s starb wegen %2$s")
					.put("death.attack.hotFloor", "%1$s wurde der Boden zu heiß")
					.put("death.attack.hotFloor.player", "%1$s geriet wegen %2$s in die Gefahrenzone")
					.put("death.attack.inFire", "%1$s ging in Flammen auf")
					.put("death.attack.inFire.player", "%1$s lief während des Kampfes mit %2$s ins Feuer")
					.put("death.attack.inWall", "%1$s wurde lebendig begraben")
					.put("death.attack.inWall.player", "%1$s wurde während des Kampfes mit %2$s lebendig begraben")
					.put("death.attack.indirectMagic", "%1$s wurde von %2$s mit Magie getötet")
					.put("death.attack.indirectMagic.item", "%1$s wurde von %2$s mit %3$s getötet")
					.put("death.attack.lava", "%1$s versuchte, in Lava zu schwimmen")
					.put("death.attack.lava.player", "%1$s fiel beim Versuch, %2$s zu entkommen, in Lava")
					.put("death.attack.lightningBolt", "%1$s wurde vom Blitz getroffen")
					.put("death.attack.lightningBolt.player", "%1$s wurde während des Kampfes mit %2$s vom Blitz getroffen")
					.put("death.attack.magic", "%1$s wurde durch Magie getötet")
					.put("death.attack.message_too_long", "Leider ist die Meldung zu lang, um angezeigt werden zu können. Hier ist eine verkürzte Version: %s")
					.put("death.attack.mob", "%1$s wurde von %2$s erschlagen")
					.put("death.attack.mob.item", "%1$s wurde von %2$s mit %3$s erschlagen")
					.put("death.attack.netherBed.link", "beabsichtigtem Spieldesign")
					.put("death.attack.netherBed.message", "%1$s wurde von %2$s getötet")
					.put("death.attack.onFire", "%1$s verbrannte")
					.put("death.attack.onFire.player", "%1$s wurde während des Kampfes mit %2$s geröstet")
					.put("death.attack.outOfWorld", "%1$s fiel aus der Welt")
					.put("death.attack.outOfWorld.player", "%1$s wollte nicht mehr in derselben Welt wie %2$s leben")
					.put("death.attack.player", "%1$s wurde von %2$s erschlagen")
					.put("death.attack.player.item", "%1$s wurde von %2$s mit %3$s erschlagen")
					.put("death.attack.starve", "%1$s verhungerte")
					.put("death.attack.starve.player", "%1$s verhungerte während des Kampfes mit %2$s")
					.put("death.attack.sweetBerryBush", "%1$s wurde von einem Süßbeerenstrauch zu Tode gestochen")
					.put("death.attack.sweetBerryBush.player", "%1$s wurde beim Versuch, %2$s zu entkommen, von einem Süßbeerenstrauch zu Tode gestochen")
					.put("death.attack.thorns", "%1$s wurde beim Versuch, %2$s zu verletzen, getötet")
					.put("death.attack.thorns.item", "%1$s wurde beim Versuch, %2$s zu verletzen, von %3$s getötet")
					.put("death.attack.thrown", "%1$s wurde von %2$s zu Tode geprügelt")
					.put("death.attack.thrown.item", "%1$s wurde von %2$s mit %3$s zu Tode geprügelt")
					.put("death.attack.trident", "%1$s wurde von %2$s aufgespießt")
					.put("death.attack.trident.item", "%1$s wurde von %2$s mit %3$s aufgespießt")
					.put("death.attack.wither", "%1$s verdorrte")
					.put("death.attack.wither.player", "%1$s verdorrte während des Kampfes mit %2$s")
					.put("death.fell.accident.generic", "%1$s fiel aus zu großer Höhe")
					.put("death.fell.accident.ladder", "%1$s fiel von einer Leiter")
					.put("death.fell.accident.vines", "%1$s fiel von Ranken")
					.put("death.fell.accident.water", "%1$s fiel aus dem Wasser")
					.put("death.fell.assist", "%1$s wurde von %2$s zum Fallen verdammt")
					.put("death.fell.assist.item", "%1$s wurde von %2$s mit %3$s zum Fallen verdammt")
					.put("death.fell.finish", "%1$s fiel zu tief und wurde von %2$s erledigt")
					.put("death.fell.finish.item", "%1$s fiel zu tief und wurde von %2$s mit %3$s erledigt")
					.put("death.fell.killer", "%1$s wurde zum Fallen verdammt"));

		registerListener(new Listener() {
			
			@EventHandler
			public void onDeath(PlayerDeathEvent event) {
				EntityPlayer player = ((CraftPlayer) event.getEntity()).getHandle();
				ChatMessage cmessage = (ChatMessage) player.getCombatTracker().getDeathMessage();
				
				String title = cmessage.getKey();
				Object[] params = cmessage.getArgs();

				for (int x = 0; x < params.length; x++) {
					Object obj = params[x];
					if (obj instanceof IChatBaseComponent) {
						IChatBaseComponent base = (IChatBaseComponent) obj;
						String components = getComponents(base)
								.map(s -> String.format("§e%s§7", s.getText()))
								.collect(Collectors.joining());
						obj = String.format("§e%s§7", components);
					}
					params[x] = obj;
				}
			
				event.setDeathMessage("§7".concat(String.format(getManaged().getOrDefault(title, "§e%1$s §7starb"), params)));
			}
		});
	}

	private Stream<IChatBaseComponent> getComponents(IChatBaseComponent base) {
		return Stream.concat(Stream.of(base), base.getSiblings().stream().flatMap(this::getComponents));
	}

	/*public static void main(String[] args) {
		String[] strings = {
				"\"death.attack.anvil\": \"%1$s wurde von einem fallenden Amboss zerquetscht\"",
				"\"death.attack.anvil.player\": \"%1$s wurde während des Kampfes mit %2$s von einem fallenden Amboss zerquetscht\"",
				"\"death.attack.arrow\": \"%1$s wurde von %2$s erschossen\"",
				"\"death.attack.arrow.item\": \"%1$s wurde von %2$s mit %3$s erschossen\"",
				"\"death.attack.cactus\": \"%1$s wurde zu Tode gestochen\"",
				"\"death.attack.cactus.player\": \"%1$s rannte beim Versuch, %2$s zu entkommen, in einen Kaktus\"",
				"\"death.attack.cramming\": \"%1$s wurde zerquetscht\"",
				"\"death.attack.cramming.player\": \"%1$s wurde von %2$s zerquetscht\"",
				"\"death.attack.dragonBreath\": \"%1$s wurde im Drachenatem geröstet\"",
				"\"death.attack.dragonBreath.player\": \"%1$s wurde durch %2$s in Drachenatem geröstet\"",
				"\"death.attack.drown\": \"%1$s ertrank\"",
				"\"death.attack.drown.player\": \"%1$s ertrank beim Versuch, %2$s zu entkommen\"",
				"\"death.attack.even_more_magic\": \"%1$s wurde durch verstärkte Magie getötet\"",
				"\"death.attack.explosion\": \"%1$s wurde in die Luft gesprengt\"",
				"\"death.attack.explosion.player\": \"%1$s wurde durch %2$s in die Luft gesprengt\"",
				"\"death.attack.explosion.player.item\": \"%1$s wurde von %2$s mit %3$s in die Luft gesprengt\"",
				"\"death.attack.fall\": \"%1$s fiel der Schwerkraft zum Opfer\"",
				"\"death.attack.fall.player\": \"%1$s fiel beim Versuch, %2$s zu entkommen, der Schwerkraft zum Opfer\"",
				"\"death.attack.fallingBlock\": \"%1$s wurde von einem fallenden Block zerquetscht\"",
				"\"death.attack.fallingBlock.player\": \"%1$s wurde während des Kampfes mit %2$s von einem fallenden Block zerquetscht\"",
				"\"death.attack.fireball\": \"%1$s wurde von %2$s flambiert\"",
				"\"death.attack.fireball.item\": \"%1$s wurde von %2$s mit %3$s flambiert\"",
				"\"death.attack.fireworks\": \"%1$s flog mit einem Knall in die Luft\"",
				"\"death.attack.fireworks.player\": \"%1$s flog während des Kampfes mit %2$s mit einem Knall in die Luft\"",
				"\"death.attack.flyIntoWall\": \"%1$s erfuhr kinetische Energie\"",
				"\"death.attack.flyIntoWall.player\": \"%1$s erfuhr beim Versuch, %2$s zu entkommen, kinetische Energie\"",
				"\"death.attack.generic\": \"%1$s starb\"",
				"\"death.attack.generic.player\": \"%1$s starb wegen %2$s\"",
				"\"death.attack.hotFloor\": \"%1$s wurde der Boden zu heiß\"",
				"\"death.attack.hotFloor.player\": \"%1$s geriet wegen %2$s in die Gefahrenzone\"",
				"\"death.attack.inFire\": \"%1$s ging in Flammen auf\"",
				"\"death.attack.inFire.player\": \"%1$s lief während des Kampfes mit %2$s ins Feuer\"",
				"\"death.attack.inWall\": \"%1$s wurde lebendig begraben\"",
				"\"death.attack.inWall.player\": \"%1$s wurde während des Kampfes mit %2$s lebendig begraben\"",
				"\"death.attack.indirectMagic\": \"%1$s wurde von %2$s mit Magie getötet\"",
				"\"death.attack.indirectMagic.item\": \"%1$s wurde von %2$s mit %3$s getötet\"",
				"\"death.attack.lava\": \"%1$s versuchte, in Lava zu schwimmen\"",
				"\"death.attack.lava.player\": \"%1$s fiel beim Versuch, %2$s zu entkommen, in Lava\"",
				"\"death.attack.lightningBolt\": \"%1$s wurde vom Blitz getroffen\"",
				"\"death.attack.lightningBolt.player\": \"%1$s wurde während des Kampfes mit %2$s vom Blitz getroffen\"",
				"\"death.attack.magic\": \"%1$s wurde durch Magie getötet\"",
				"\"death.attack.message_too_long\": \"Leider ist die Meldung zu lang, um angezeigt werden zu können. Hier ist eine verkürzte Version: %s\"",
				"\"death.attack.mob\": \"%1$s wurde von %2$s erschlagen\"",
				"\"death.attack.mob.item\": \"%1$s wurde von %2$s mit %3$s erschlagen\"",
				"\"death.attack.netherBed.link\": \"beabsichtigtem Spieldesign\"",
				"\"death.attack.netherBed.message\": \"%1$s wurde von %2$s getötet\"",
				"\"death.attack.onFire\": \"%1$s verbrannte\"",
				"\"death.attack.onFire.player\": \"%1$s wurde während des Kampfes mit %2$s geröstet\"",
				"\"death.attack.outOfWorld\": \"%1$s fiel aus der Welt\"",
				"\"death.attack.outOfWorld.player\": \"%1$s wollte nicht mehr in derselben Welt wie %2$s leben\"",
				"\"death.attack.player\": \"%1$s wurde von %2$s erschlagen\"",
				"\"death.attack.player.item\": \"%1$s wurde von %2$s mit %3$s erschlagen\"",
				"\"death.attack.starve\": \"%1$s verhungerte\"",
				"\"death.attack.starve.player\": \"%1$s verhungerte während des Kampfes mit %2$s\"",
				"\"death.attack.sweetBerryBush\": \"%1$s wurde von einem Süßbeerenstrauch zu Tode gestochen\"",
				"\"death.attack.sweetBerryBush.player\": \"%1$s wurde beim Versuch, %2$s zu entkommen, von einem Süßbeerenstrauch zu Tode gestochen\"",
				"\"death.attack.thorns\": \"%1$s wurde beim Versuch, %2$s zu verletzen, getötet\"",
				"\"death.attack.thorns.item\": \"%1$s wurde beim Versuch, %2$s zu verletzen, von %3$s getötet\"",
				"\"death.attack.thrown\": \"%1$s wurde von %2$s zu Tode geprügelt\"",
				"\"death.attack.thrown.item\": \"%1$s wurde von %2$s mit %3$s zu Tode geprügelt\"",
				"\"death.attack.trident\": \"%1$s wurde von %2$s aufgespießt\"",
				"\"death.attack.trident.item\": \"%1$s wurde von %2$s mit %3$s aufgespießt\"",
				"\"death.attack.wither\": \"%1$s verdorrte\"",
				"\"death.attack.wither.player\": \"%1$s verdorrte während des Kampfes mit %2$s\"",
				"\"death.fell.accident.generic\": \"%1$s fiel aus zu großer Höhe\"",
				"\"death.fell.accident.ladder\": \"%1$s fiel von einer Leiter\"",
				"\"death.fell.accident.vines\": \"%1$s fiel von Ranken\"",
				"\"death.fell.accident.water\": \"%1$s fiel aus dem Wasser\"",
				"\"death.fell.assist\": \"%1$s wurde von %2$s zum Fallen verdammt\"",
				"\"death.fell.assist.item\": \"%1$s wurde von %2$s mit %3$s zum Fallen verdammt\"",
				"\"death.fell.finish\": \"%1$s fiel zu tief und wurde von %2$s erledigt\"",
				"\"death.fell.finish.item\": \"%1$s fiel zu tief und wurde von %2$s mit %3$s erledigt\"",
				"\"death.fell.killer\": \"%1$s wurde zum Fallen verdammt\"",
		};

		for (String s : strings) {
			int end = s.indexOf('"', 2);
			System.out.printf(".put(%s\",%s)\n", s.substring(0, end), s.substring(end + 2)));
		}
	}*/
}
