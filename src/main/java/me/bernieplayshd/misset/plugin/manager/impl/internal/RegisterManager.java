package me.bernieplayshd.misset.plugin.manager.impl.internal;

import java.util.Optional;

import co.aikar.timings.Timing;
import com.google.common.collect.ImmutableClassToInstanceMap;

import me.bernieplayshd.misset.api.manager.AbstractManager;
import me.bernieplayshd.misset.api.manager.immutable.ImmutableClassToInstanceManager;
import me.bernieplayshd.misset.api.module.annotations.ModuleData;
import me.bernieplayshd.misset.plugin.Utils;
import me.bernieplayshd.misset.plugin.manager.impl.BackpackManager;
import me.bernieplayshd.misset.plugin.manager.impl.DeathManager;
import me.bernieplayshd.misset.plugin.manager.impl.SpawnerManager;
import me.bernieplayshd.misset.plugin.manager.impl.SpectateManager;
import me.bernieplayshd.misset.plugin.manager.impl.WorldBorderManager;

import javax.annotation.Nullable;

@ModuleData(name = "Register")
public class RegisterManager extends ImmutableClassToInstanceManager<AbstractManager<?>> {

	public RegisterManager() {
		super("Register Manager",
				new ImmutableClassToInstanceMap.Builder<AbstractManager<?>>()
					.put(ShellManager.class, new ShellManager())
					.put(BackpackManager.class, new BackpackManager())
					.put(DeathManager.class, new DeathManager())
					.put(SpawnerManager.class, new SpawnerManager())
					//.put(ReportManager.class, new ReportManager())
					.put(SpectateManager.class, new SpectateManager())
					//.put(PlayTimeManager.class, new PlayTimeManager())
					//.put(SubtitleManager.class, new SubtitleManager())
					.put(WorldBorderManager.class, new WorldBorderManager()));
	}

	private void updateState(boolean state) {
		getManaged().values().stream()
				.filter( manager -> manager.isRunning() != state)
				.forEach(manager -> changeState(manager, state));
	}

	private void changeState(AbstractManager<?> manager, boolean state) {
		try (Timing timing = Utils.timing(String.format("%s - %s",
				manager.getName(), state ? "Startup" : "Shutdown")))
		{
			manager.setRunning(state);
		}
	}

	@Override
	public void onEnable() {
		updateState(true);
	}
	
	@Override
	public void onDisable() {
		updateState(false);
	}

	@Nullable
	public <T extends AbstractManager<?>> T getManager(Class<T> clazz) {
		return getManaged().getInstance(clazz);
	}
	
	public Optional<AbstractManager<?>> findManager(String name) {
		return getManaged().values().stream()
				.filter(manager -> manager.getName().equalsIgnoreCase(name))
			    .findAny();
	}
}
