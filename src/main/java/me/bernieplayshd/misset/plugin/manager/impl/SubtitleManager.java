package me.bernieplayshd.misset.plugin.manager.impl;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.netty.buffer.ByteBuf;
import io.netty.buffer.Unpooled;
import me.bernieplayshd.misset.api.manager.simple.SimpleListManager;
import me.bernieplayshd.misset.api.module.annotations.ModuleData;
import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.Utils;
import me.bernieplayshd.misset.plugin.manager.impl.internal.RegisterManager;
import me.bernieplayshd.misset.plugin.subtitle.Subtitle;
import me.lucko.luckperms.api.LuckPermsApi;
import me.lucko.luckperms.api.User;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.RegisteredServiceProvider;
import us.dev.api.command.Command;
import us.dev.api.command.handler.annotations.Executes;

import java.nio.charset.StandardCharsets;
import java.util.Collection;
import java.util.Map;
import java.util.Optional;
import java.util.logging.Level;

@ModuleData(name = "Subtitles", requires = RegisterManager.class)
public class SubtitleManager extends SimpleListManager<Subtitle> {

    private static final Gson GSON = new GsonBuilder().create();

    private static final Map<String, String> GROUPS = new ImmutableMap.Builder<String, String>()
            // Admin
            .put("leitung", "§4Administrator")
            //.put("S-Admin", "§4Administrator")
            .put("admin", "§4Administrator")
            // Staff
            .put("moderator", "§cStaff Mitglied")
            .put("supporter", "§cStaff Mitglied")
            // Adel
            .put("adel", "§6Mitglied des Adels")
            .build();

    private LuckPermsApi luckyPerms;

    public SubtitleManager() {
        super("Subtitle Manager");

        registerListener(new Listener() {

            @EventHandler
            public void onConnect(PlayerJoinEvent event) {
                load(event.getPlayer(), true);
            }

            @EventHandler
            public void onDisconnect(PlayerQuitEvent event) {
                unload(event.getPlayer());
            }
        });

        registerCommand(new Command(MissetPlugin.getPlugin().getCommandManager(), "subtitle") {

            @Executes(value = "hide", permission = "system.support")
            public String hideSubtitle(Player player) {
                Optional<Subtitle> optional = getSubtitleOf(player);
                if (!optional.isPresent()) {
                    return MissetPlugin.PREFIX + "§cDu hast derzeit keinen eigenen Subtitle!";
                }

                Subtitle title = optional.get();
                title.toggleHidden();

                updateTitle(title);

                return MissetPlugin.PREFIX + "§aDu hast dein Subtitle erfolgreich "
                        + (title.isHidden() ? "versteckt!" : "sichtbar gemacht!");
            }
        });
    }

    @Override
    public void onEnable() {
        super.onEnable();
        // Get the LuckyPerms API
        RegisteredServiceProvider<LuckPermsApi> provider = Bukkit.getServicesManager()
                .getRegistration(LuckPermsApi.class);
        if (provider != null) {
            this.luckyPerms = provider.getProvider();
        } else {
            getLogger().log(Level.SEVERE, "Failed to load the LuckyPerms API, disabling module..");
            onDisable();
            return;
        }

        Utils.runTaskLater(() -> {
            Bukkit.getOnlinePlayers().forEach(player -> load(player, false));
            updateTitles(getManaged());
        }, 5L);
    }

    public void load(Player player, boolean update) {
        User user = luckyPerms.getUser(player.getUniqueId());
        String subtitle = GROUPS.get(user.getPrimaryGroup());
        if (subtitle != null) {
            Subtitle title = new Subtitle(player, subtitle);
            if (getManaged().add(title) && update) {
                updateTitle(title);
            }
        }
    }

    public Optional<Subtitle> getSubtitleOf(Player player) {
        return getManaged().stream().filter(title -> title.getPlayer().equals(player)).findAny();
    }

    public void unload(Player player) {
        getSubtitleOf(player).ifPresent(getManaged()::remove);
    }

    // TODO: maybe change the size of the subtitles since its possible now
    // http://docs.labymod.net/pages/server/subtitles/
    private JsonArray generateJson(Subtitle title) {
        JsonArray array = new JsonArray();
        {
            JsonObject object = new JsonObject();
            object.addProperty("uuid", title.getPlayer().getUniqueId().toString());
            // object.addProperty("size", 1.0D); // range: 0.8 - 1.6
            if (!title.isHidden()) {
                object.addProperty("value", title.getTitle());
            }
            array.add(object);
        }
        return array;
    }

    private byte[] generatePayload(JsonArray json) {
        ByteBuf buf = Unpooled.buffer();
        writeString(buf, "account_subtitle");
        writeString(buf, GSON.toJson(json));

        // Copying the buffer's bytes to the byte array
        byte[] bytes = new byte[buf.readableBytes()];
        buf.readBytes(bytes);
        return bytes;
    }

    public void updateTitle(Subtitle title) {
        byte[] payload = generatePayload(generateJson(title));
        Bukkit.getOnlinePlayers().forEach(player -> sendPayload(player, payload));
    }

    public void updateTitles(Collection<Subtitle> titles) {
        // Merge the subtitles to send them in a single packet,
        // instead of generating and sending every title alone
        JsonArray destination = new JsonArray();
        titles.stream().map(this::generateJson).forEach(destination::addAll);

        byte[] merged = generatePayload(destination);

        Bukkit.getOnlinePlayers().forEach(player -> sendPayload(player, merged));
    }

    private void sendPayload(Player player, byte[] bytes) {
        /*PacketPlayOutCustomPayload packet = new PacketPlayOutCustomPayload("LMC",
                new PacketDataSerializer(Unpooled.wrappedBuffer(bytes)));

        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(packet);*/
        player.sendPluginMessage(MissetPlugin.getPlugin(), "LMC", bytes);
    }

    @Override
    public void onDisable() {
        getManaged().clear();
        super.onDisable();
    }

    /**
     * Writes a string to the given byte buffer
     *
     * @param buf    the byte buffer the string should be written to
     * @param string the string that should be written to the buffer
     */
    private void writeString( ByteBuf buf, String string ) {
        byte[] abyte = string.getBytes( StandardCharsets.UTF_8 );

        if ( abyte.length > Short.MAX_VALUE ) {
            throw new RuntimeException( "String too big (was " + string.length()
                    + " bytes encoded, max " + Short.MAX_VALUE + ")" );
        } else {
            writeVarIntToBuffer( buf, abyte.length );
            buf.writeBytes( abyte );
        }
    }

    /**
     * Writes a varint to the given byte buffer
     *
     * @param buf   the byte buffer the int should be written to
     * @param input the int that should be written to the buffer
     */
    private void writeVarIntToBuffer( ByteBuf buf, int input ) {
        while ( (input & -128) != 0 ) {
            buf.writeByte( input & 127 | 128 );
            input >>>= 7;
        }

        buf.writeByte( input );
    }
}
