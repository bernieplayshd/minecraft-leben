package me.bernieplayshd.misset.plugin.manager.impl;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.stream.Collectors;

import me.bernieplayshd.misset.plugin.commands.ReportCommand;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.craftbukkit.libs.org.apache.commons.lang3.tuple.Pair;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.maxgamer.maxbans.MaxBans;

import me.bernieplayshd.misset.api.config.annotations.Config;
import me.bernieplayshd.misset.api.item.ItemBuilder;
import me.bernieplayshd.misset.api.manager.simple.DatabaseManager;
import me.bernieplayshd.misset.api.module.annotations.ModuleData;
import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.listeners.ReportListener;
import me.bernieplayshd.misset.plugin.manager.impl.internal.RegisterManager;
import me.bernieplayshd.misset.plugin.report.Report;
import me.bernieplayshd.misset.plugin.report.ReportTemplate;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.chat.ClickEvent.Action;

@ModuleData(name = "Reports", requires = RegisterManager.class)
public class ReportManager extends DatabaseManager<Report> {
	
	public ReportManager() {
		super("Report Manager", MissetPlugin.getPlugin().getDatabase());
		
		registerListener(new ReportListener(this));
		
		registerCommand(new ReportCommand(this));
	}
	
	public static final long HOUR  = 3600L,
							 DAY   = 86400L,
							 WEEK  = 604800L,
							 MONTH = 2620800L;
	
	public List<Report> getReportsOf(UUID uuid) {
		return getManaged().stream().filter(report -> report.getReported().equals(uuid)).collect(Collectors.toList());
	}
	
	public Optional<Report> getReportOf(int id) {
		return getManaged().stream().filter(report -> report.getID() == id).findFirst();
	}

	private void loadAll(Object[][] rows) {
		for (Object[] row : rows) {
			getManaged().add(parse((int) row[0], (String) row[1], (String) row[2],
					(int) row[3], ((Timestamp) row[4]).getTime()));
		}
	}

	@Override
	public void onEnable() {
		super.onEnable();

		getDatabase().queryRows("SELECT * FROM `reports`")
			.whenComplete((rows, ex) -> {
				if (ex != null) {
					getLogger().log(Level.SEVERE, "Failed to load reports", ex);
				} else {
					loadAll(rows);
				}
			});
	}

	@Override
	public void onDisable() {
		// TODO: shutdown
		super.onDisable();
	}

	public CompletableFuture<Report> create(UUID reported, UUID sender, ReportTemplate template) {
		return getDatabase().update("INSERT INTO `reports` (`reported`, `sender`, `template`) VALUES (?, ?, ?)",
				reported.toString(), sender.toString(), template.ordinal())
			.thenCompose(__ -> getDatabase()
					.querySet("SELECT `id`, `timestamp` FROM `reports` ORDER BY ID DESC LIMIT 1",
							rs -> {
								rs.next();
								return Pair.of(rs.getInt("id"), rs.getTimestamp("timestamp").getTime());
							}))
			.thenApply(pair -> {
				Report report = new Report(pair.getLeft(), reported, sender, template, pair.getRight());
				getManaged().add(report);
				return report;
			});
	}
	
	public void remove(Report report, boolean loaded) {
		getDatabase().update("DELETE FROM `reports` WHERE `id` = ?", report.getID());
		if (loaded) {
			getManaged().remove(report);
		}
	}
	
	public void accept(Report report) {
		OfflinePlayer player = Bukkit.getOfflinePlayer(report.getReported()),
					  sender = Bukkit.getOfflinePlayer(report.getSender());
		if (player != null && sender != null) {
			ReportTemplate template = report.getTemplate();
			MaxBans.instance.getBanManager().warn(player.getName(), template.getDisplay(), sender.getName()); // warn
			if (template.getTime() > 0L) {
				MaxBans.instance.getBanManager().tempban(player.getName(), template.getDisplay(), sender.getName(),
						new Date(new Date().getTime() + template.getTime()).getTime()); // ban
			}
			if (sender.isOnline()) {
				rewardPlayer(sender.getPlayer(), report);
				remove(report, true);
				return;
			}
			report.finish();
			getDatabase().update("UPDATE `reports` SET `finished` = ? WHERE `id` = ?",
					report.isFinished(), report.getID());
			getManaged().remove(report);
		}
	}
	
	private Report parse(int id, String reported, String sender, int template, long timestamp) {
		ReportTemplate newTemplate = ReportTemplate.values()[template];
		return new Report(id, UUID.fromString(reported), UUID.fromString(sender), newTemplate, timestamp);
	}

	public CompletableFuture<List<Report>> getFinished(UUID uuid) {
		return getDatabase().queryRows("SELECT `id`, `reported`, `template`, `timestamp`"
				+ " FROM `reports` WHERE `sender` = ? AND `finished` = ?", uuid.toString(), true)
			.thenApply(rows -> {
				List<Report> result = new ArrayList<>(rows.length);
				for (Object[] row : rows) {
					result.add(parse((int) row[0], (String) row[1], uuid.toString(),
							(int) row[2], ((Timestamp) row[3]).getTime()));
				}
				return result;
			});
	}
	
	public void notifyTeam(Report report) {
		OfflinePlayer sender   = Bukkit.getOfflinePlayer(report.getSender()),
				      reported = Bukkit.getOfflinePlayer(report.getReported());
		
		TextComponent text = new TextComponent();
		text.setText(String.format("%s§7Der Spieler §e%s §7hat den Spieler §c%s §7wegen %s §7gemeldet ",
				MissetPlugin.PREFIX, sender.getName(), reported.getName(), report.getTemplate().getDisplay()));
		
		TextComponent clickable = new TextComponent("§8[§9Bearbeiten§8]");
		clickable.setClickEvent(new ClickEvent(Action.RUN_COMMAND, "/report edit " + report.getID()));
		clickable.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponent[] {
				new TextComponent("§7Linksklick um zu bearbeiten") }));
		text.addExtra(clickable);
		
		for (Player player : Bukkit.getOnlinePlayers()) {
			if (player.hasPermission("system.support")) {
				player.spigot().sendMessage(text);
			}
		}
	}
	
	public void rewardPlayer(Player player, Report report) {
		OfflinePlayer offline = Bukkit.getOfflinePlayer(report.getReported());
		if (offline == null) {
			return;
		}
		player.sendMessage(String.format("%s§7Dein Report von dem Spieler §e%s §7wurde angenommen" +
				" und §7du erhälst somit §beinen Diamanten", MissetPlugin.PREFIX, offline.getName()));
		player.getInventory().addItem(reportReward);
	}

	//
	//
	
	public static final DateFormat DATE_FORMAT = new SimpleDateFormat();
	
	@Config
	public ItemStack reportReward = new ItemStack(Material.DIAMOND);
	
	@Config
	public static String GUI_TITLE  = "§8Report:§e ",
					     EDIT_TITLE = "§8Report[§e";
	
	public Inventory getReportGui(String reported) {
		Inventory inv = Bukkit.createInventory(null, 9, GUI_TITLE.concat(reported));
		for (ReportTemplate template : ReportTemplate.values()) {
			inv.addItem(ItemBuilder.build(template.getItem(), template.getDisplay(), template.getDescription()));
		}
		return inv;
	}
	
	public Inventory getMenuGui(Report report) {
		OfflinePlayer offline = Bukkit.getOfflinePlayer(report.getReported());
		Inventory inv = Bukkit.createInventory(null, 9,
				String.format("%s%d§8]", EDIT_TITLE, report.getID()));
		
		ItemStack skull = new ItemStack(Material.PLAYER_HEAD, 1, (short) 3);
		SkullMeta meta = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.PLAYER_HEAD);
		meta.setOwner(offline.getName());
		meta.setDisplayName("§3" + offline.getName());
		
		List<String> lore = new ArrayList<>();
		lore.add("§7Letztes Mal online: §e" + DATE_FORMAT.format(new Date(offline.getLastPlayed())));
		if (offline.isOnline()) {
			Player player = offline.getPlayer();
			lore.add("§7Leben: §e" + player.getHealth());
			lore.add("§7XP: §e" + player.getExpToLevel());
			lore.add("§7Position: §e" + player.getLocation());
		}
		meta.setLore(lore);
		
		skull.setItemMeta(meta);
		inv.setItem(0, skull);
		
		if (offline.isOnline()) {
			inv.setItem(1, ItemBuilder.build(Material.ENDER_PEARL, "§9Zum Spieler teleportieren",
					"§7Teleportiert dich zu der Position, an",
					"§7der sich der gemeldete Spieler befindet"));
			ItemStack stack = ItemBuilder.build(Material.ENDER_PEARL, "§9TP mit Spectator-Mode",
					"§7Teleportiert dich zu der Position, an",
					"§7der sich der gemeldete Spieler befindet",
					"§7und wechselt automatisch in den Spectator-Mode");
			ItemMeta meta2 = stack.getItemMeta();
			meta2.addEnchant(Enchantment.DURABILITY, 1, false);
			stack.setItemMeta(meta2);
			//.addEnchantment(Enchantment.DURABILITY, 1, true);#
			inv.setItem(2, stack);
			inv.setItem(3, ItemBuilder.build(Material.PAPER, "§rListe der letzten 50 Chat-Nachrichten",
					"§7Listet die letzten 50 Chat-Nachrichten des",
					"§7gemeldeten Spielers auf (Wenn Verfügbar)"));
			inv.setItem(4, ItemBuilder.build(Material.CHEST, "§6Inventar anschauen",
					"§7Öffnet das Inventar des gemeldeten Spielers"));
		}
		inv.setItem(7, ItemBuilder.build(Material.REDSTONE_BLOCK, "§cReport ablehnen",
				"§7Schließt den Report ab ohne dem",
				"§7gemeldeten Spieler eine Strafe",
				"§7zu erteilen"));
		inv.setItem(8, ItemBuilder.build(Material.EMERALD_BLOCK, "§aReport annehmen",
				"§7Schließt den Report ab und verteilt",
				"§7dem gemeldeten Spieler eine Strafe"));
		return inv;
	}
}
