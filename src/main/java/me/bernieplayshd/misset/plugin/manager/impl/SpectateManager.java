package me.bernieplayshd.misset.plugin.manager.impl;

import java.util.Iterator;
import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.google.common.collect.ImmutableMap;

import me.bernieplayshd.misset.api.config.annotations.Config;
import me.bernieplayshd.misset.api.item.ItemBuilder;
import me.bernieplayshd.misset.api.manager.simple.SimpleListManager;
import me.bernieplayshd.misset.api.module.annotations.ModuleData;
import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.listeners.SpectateListener;
import me.bernieplayshd.misset.plugin.manager.impl.internal.RegisterManager;
import me.bernieplayshd.misset.plugin.report.ReportTemplate;
import me.bernieplayshd.misset.plugin.spectate.PlayerInformation;
import us.dev.api.command.Command;
import us.dev.api.command.handler.annotations.Executes;

@ModuleData(name = "Spectate", requires = RegisterManager.class)
public class SpectateManager extends SimpleListManager<PlayerInformation> {
	
	private static final ItemStack[] EMPTY = new ItemStack[0];
	@Config
	private float flySpeed = 0.4F;
	
	private static final ImmutableMap<Integer, ItemStack> SPECTATE_INV = new ImmutableMap.Builder<Integer, ItemStack>()
			.put(0, ItemBuilder.build(Material.ENDER_PEARL, "§9Zu Spieler teleportieren"))
			.put(1, ItemBuilder.build(Material.ENDER_PEARL, "§bTp zu Random Spieler"))
			//.put(2, ItemBuilder.build(Material.REDSTONE_TORCH_ON, "§6ESP §7(§cAus§7)"))
			.put(2, ItemBuilder.build(Material.CHEST, "§6Ins Inventar sehen"))
			.put(3, ItemBuilder.build(Material.REDSTONE_BLOCK, "§cSpieler Bannen"))
			.put(4, ItemBuilder.build(Material.GOLDEN_AXE, "§eLogBlock Werkzeug"))
			.put(8, ItemBuilder.build(Material.RED_WOOL, 1, (short) 14, "§cVerlassen"))
			.build();

	@Config
	public static String BAN_TITLE = "§8Ban: ";

	public SpectateManager() {
		super("Spectate Manager");
		
		registerListener(new SpectateListener(this));
		
		registerCommand(new Command(MissetPlugin.getPlugin().getCommandManager(), "spectate", "system.support") {

			@Executes
			public String spectate(Player player) {
				if (isSpectator(player)) {
					normalizePlayer(player, true);
					return MissetPlugin.PREFIX + "§7Du hast den §eSpectator-Mode §7verlassen";
				}

				setSpectatorMode(player);
				return MissetPlugin.PREFIX + "§7Du hast den §eSpectator-Mode §7betreten";
			}

			@Executes
			public String teleport(Player player, String otherPlayer) {
				Player target = Bukkit.getPlayer(otherPlayer);
				if (target == null) {
					return MissetPlugin.PREFIX + "§cDieser Spieler ist derzeit nicht online!";
				}

				if (!isSpectator(player)) {
					setSpectatorMode(player);
					player.sendMessage(MissetPlugin.PREFIX + "§7Du hast den §eSpectator-Mode §7beigetreten");
				}

				player.teleport(target);
				return null;
			}
		});
	}

	public void setSpectatorMode(Player player) {
		PlayerInformation info = new PlayerInformation(player.getUniqueId());
		
		player.setHealth(20.0D);
		player.setExp(0.0F);
		player.setFoodLevel(20);
		
		player.getInventory().setContents(EMPTY);
		player.getInventory().setArmorContents(EMPTY);
		player.getInventory().setExtraContents(EMPTY);
		
		player.getActivePotionEffects().forEach(effect -> player.removePotionEffect(effect.getType()));

		for (Player p : Bukkit.getOnlinePlayers()) {
			if (p != player) {
				p.hidePlayer(player);
			}
		}
		
		player.setGameMode(GameMode.SURVIVAL);
		player.setAllowFlight(true);
		player.setFlying(true);
		player.setFlySpeed(flySpeed);
		player.addPotionEffect(new PotionEffect(PotionEffectType.INVISIBILITY, 99999, 999, true, true));

		SPECTATE_INV.forEach(player.getInventory()::setItem);
		
		getManaged().add(info);
	}
	
	public void normalizePlayer(Player player, boolean remove) {
		Optional<PlayerInformation> optional = getManaged().stream()
				.filter(i -> i.uuid.equals(player.getUniqueId()))
				.findFirst();
		optional.ifPresent(info -> {
			info.apply();		
			if (remove) {
				getManaged().remove(info);
			}
		});
	}
	
	public boolean isSpectator(Player player) {
		return getManaged().stream().anyMatch(info -> info.uuid.equals(player.getUniqueId()));
	}
	
	@Override
	public void onDisable() {
		Iterator<PlayerInformation> iterator = getManaged().iterator();
		while (iterator.hasNext()) {
			PlayerInformation info = iterator.next();
			Player player = Bukkit.getPlayer(info.uuid);
			normalizePlayer(player, false);
			player.sendMessage(MissetPlugin.PREFIX + "§7Du hast den §eSpectator-Mode §7verlassen");
			iterator.remove();
		}
		super.onDisable();
	}
	
	//
	//
	
	public Inventory getBanGui(String name) {
		Inventory inv = Bukkit.createInventory(null, 9, BAN_TITLE + name);
		for (ReportTemplate template : ReportTemplate.values()) {
			inv.addItem(ItemBuilder.build(template.getItem(), template.getDisplay(), template.getDescription()));
		}
		return inv;
	}
	 
	/*public List<Player> getESPSpectators() {
		return getManaged().stream()
				.map(info -> Bukkit.getPlayer(info.uuid))
				.filter(player -> {
					ItemStack item = player.getInventory().getContents()[2];
					return item.hasItemMeta() && "§6ESP §7(§aAn§7)".equals(item.getItemMeta().getDisplayName());
				})
				.collect(Collectors.toList());
	}*/
}
