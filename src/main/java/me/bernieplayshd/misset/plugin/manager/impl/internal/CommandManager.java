package me.bernieplayshd.misset.plugin.manager.impl.internal;

import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;

import me.bernieplayshd.misset.api.manager.simple.SimpleMapManager;
import me.bernieplayshd.misset.api.module.annotations.ModuleData;
import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.Utils;
import me.bernieplayshd.misset.plugin.commands.DupBookCommand;
import me.bernieplayshd.misset.plugin.commands.ModuleCommand;

import net.md_5.bungee.api.chat.BaseComponent;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.command.PluginCommand;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Player;

import us.dev.api.command.Command;
import us.dev.api.command.handler.CommandExecutor;
import us.dev.api.command.handler.arguments.ArgumentStack;
import us.dev.api.command.handler.handling.ArgumentHandler;
import us.dev.api.command.handler.handling.handlers.*;
import us.dev.api.command.handler.handling.parameter.CommandParameter;
import us.dev.api.command.handler.utils.PrimitiveArrays;
import us.dev.api.command.handler.utils.Primitives;

import javax.annotation.Nullable;
import java.util.*;
import java.util.logging.Level;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@ModuleData(name = "Commands")
public class CommandManager extends SimpleMapManager<String, Command> {
    private final Set<ArgumentHandler> argumentHandlers = new HashSet<>();
    private final MissetPlugin plugin;

    public CommandManager(MissetPlugin plugin) {
        super("Command Manager", new HashMap<>());
        this.plugin = plugin;

        this.registerHandler(new StringArgumentHandler());
        this.registerHandler(new BooleanArgumentHandler());
        this.registerHandler(new NumberArgumentHandler());
        this.registerHandler(new ArrayArgumentHandler());
        this.registerHandler(new EnumArgumentHandler());
        this.registerHandler(new PlayerArgumentHandler());
    }

    @Override
    public void onEnable() {
        /*
         * Register independent commands
         */
        ImmutableSet.of(
                new DupBookCommand(this),
                new  ModuleCommand(this, plugin)
        ).forEach(this::register);
    }

    private List<String> getOnlinePlayers(@Nullable String str) {
        Stream<? extends Player> stream = Bukkit.getOnlinePlayers().stream();
        if (str != null) {
            stream = stream.filter(p -> p.getName().contains(str));
        }
        return stream.map(Player::getName).collect(Collectors.toList());
    }

    private List<String> getHandlerNames(Command command, CommandSender sender) {
        return command.getExecutors().stream()
                .filter(exe -> exe.getHandler().value().length != 0 && (!exe.getPermission().isPresent()
                        || sender.hasPermission(exe.getPermission().get())))
                .map(exe -> exe.getHandler().value()[0].indexOf('|') != -1 ?
                        exe.getHandler().value()[0].split("\\|")[0] : exe.getHandler().value()[0])
                .collect(Collectors.toList());
    }

    @SuppressWarnings("unchecked")
    public <T> Optional<ArgumentHandler<T>> findArgumentHandler(Class<T> type) {
        if (type.isPrimitive()) {
            type = Primitives.wrap(type);
        } else if (type.isArray() && type.getComponentType().isPrimitive()) {
            type = PrimitiveArrays.wrap(type);
        }
        for (ArgumentHandler argumentHandler : this.argumentHandlers) {
            for (Class handledType : argumentHandler.getHandledTypes()) {
                if (handledType.isAssignableFrom(type)) {
                    return Optional.of(argumentHandler);
                }
            }
        }
        return Optional.empty();
    }

    private void registerHandler(ArgumentHandler argumentHandler) {
        this.argumentHandlers.add(argumentHandler);
    }

    private void registerBukkit(String name, Command cmd) {
        PluginCommand command = plugin.getCommand(name);
        if (command == null) {
            getLogger().log(Level.SEVERE, String.format("Command '%s' is not registered in the spigot.yml", name));
            return;
        }
        command.setAliases(Arrays.asList(cmd.getAliases()));
        command.setExecutor(new org.bukkit.command.CommandExecutor() {
            @Override
            public boolean onCommand(CommandSender sender, org.bukkit.command.Command spigotCmd, String label, String[] args) {
                if (cmd.getPermission() != null && !sender.hasPermission(cmd.getPermission())) {
                    sender.sendMessage(MissetPlugin.NO_PERMISSIONS);
                    return true;
                }

                String input = name + " " + Joiner.on(' ').join(args);
                ArgumentStack argumentStack = new ArgumentStack(parse(input.toCharArray()));
                argumentStack.next();
                argumentStack.remove();
                try {
                    Object output = cmd.execute(sender, argumentStack);
                    if (output != null) {
                        if (output instanceof String) {
                            for (String split : ((String) output).split(System.lineSeparator())) {
                                sender.sendMessage(split);
                            }
                        } else if (output instanceof String[]) {
                            for (String msg : (String[]) output) {
                                sender.sendMessage(msg);
                            }
                        } else if (output instanceof BaseComponent && sender instanceof Player) {
                            ((Player) sender).spigot().sendMessage((BaseComponent) output);
                        }
                    }
                } catch (Exception e) {
                    getLogger().log(Level.SEVERE, "An error occured while executing a command", e);
                }
                return true;
            }
        });
        // TODO: clean this mess up
        command.setTabCompleter(new TabCompleter() {
            @Override
            public List<String> onTabComplete(CommandSender sender,
                                              org.bukkit.command.Command spigotCommand,
                                              String label, String[] args)
            {
                args = Utils.notBlankOrLast(args);

                List<String> possibleHandlers = new ArrayList<>(args.length);
                // 0 = not found, 1 = found, but without params, 2 = found with params
                int handlerState = 0;

                int argsLenght;
                if ((argsLenght = args.length) != 0) {
                    for (CommandExecutor exe : cmd.getExecutors()) {
                        if (exe.getHandler().value().length == 0 || (exe.getPermission().isPresent()
                                && !sender.hasPermission(exe.getPermission().get())))
                        {
                            continue;
                        }
                        String handlerName = exe.getHandler().value()[0].split("\\|")[0];
                        if (handlerName.equalsIgnoreCase(args[0])) {
                            handlerState = 1;
                            if (argsLenght == 1) {
                                break;
                            }
                            int params = exe.getParameters().length;
                            if (params != 0 && (argsLenght - 1) <= params) {
                                CommandParameter param = exe.getParameters()[argsLenght - 2];
                                Class<?> type = param.getType();
                                if (type.isEnum()) {
                                    return Arrays.stream((Enum[]) type.getEnumConstants())
                                            .map(Enum::name)
                                            .collect(Collectors.toList());
                                } else if (param.isBoolean()) {
                                    return Arrays.asList("true", "false");
                                } else if (type == Player.class) {
                                    return getOnlinePlayers(null);
                                }
                                handlerState = 2;
                                break;
                            }
                        } else if (handlerState == 0 && handlerName.contains(args[0].trim())) {
                            possibleHandlers.add(handlerName);
                        }
                    }

                    if (handlerState == 1) {
                        return Collections.emptyList();
                    }

                    if (possibleHandlers.size() != 0) {
                        return possibleHandlers;
                    }

                    if (argsLenght == 1) {
                        return getHandlerNames(cmd, sender);
                    }

                    return getOnlinePlayers(args[argsLenght - 1]);
                } else {
                    return getHandlerNames(cmd, sender);
                }
            }
        });
        command.setUsage(cmd.getFormattedUsage(null, true));
    }

    public void unregisterBukkit(String commandName) {
        PluginCommand command = plugin.getCommand(commandName);
        command.setExecutor(null);
        command.setTabCompleter(null);
        command.setAliases(Collections.emptyList());
        command.setUsage("");
    }

    public boolean register(Command command) {
        /*if (this.registry.containsKey(command.getLabel()) || this.registry.containsValue(command)) {
            return false;
        }*/
        String name;
        Command previous = this.getManaged().put(name = command.getLabel().toLowerCase(), command);
        if (previous != null) {
            this.getManaged().values().removeIf(c -> c.equals(previous));
        }
        registerBukkit(name, command);
        /*for (String alias : command.getAliases()) {
            this.getManaged().put(alias.toLowerCase(), command);
        }*/
        return true;
    }

    public Optional<Command> find(String identifier) {
        return Optional.ofNullable(this.getManaged().get(identifier.toLowerCase()));
    }

    /*public Optional<String> execute(String input) {
        if (input == null) {
            return Optional.of("Invalid input. (input cannot be null)");
        } else if (input.length() == 0) {
            return Optional.of("Invalid input. (input cannot be empty)");
        }

        final ArgumentStack argumentStack = new ArgumentStack(parse(input.toCharArray()));
        return Optional.ofNullable(this.find(argumentStack.next())
                .<Supplier<String>>map(c -> () -> {
                    argumentStack.remove();
                    return c.execute(argumentStack);
                }).orElseGet(() -> () -> "Command not found.").get());
    }*/

    private static String[] parse(char[] input) {
        boolean quoted = false, inArray = false;
        int arrayElementCount = -1;
        ArrayList<String> arguments = new ArrayList<>();
        StringBuilder argument = new StringBuilder();
        parserBlock : for (int index = 0; index < input.length; index++) {
            switch (input[index]) {
                case '{': {
                    if (!quoted && !inArray) {
                        inArray = true;
                        arrayElementCount = 1;
                    }
                    argument.append(input[index]);
                    continue parserBlock;
                }
                case '\"': {
                    if (index > 0 && input[index - 1] == '\\') {
                        argument.setCharAt(argument.length() - 1, '\"');
                    } else {
                        quoted = !quoted;
                    }
                    continue parserBlock;
                }
                case ',': {
                    argument.append(input[index]);
                    if (!quoted && inArray) {
                        while (input[index+1] == ' ') {
                            index++;
                        }
                        arrayElementCount++;
                    }
                    continue parserBlock;
                }
                case '}': {
                    if (!quoted && inArray) {
                        inArray = false;
                        argument.append(input[index]).append(arrayElementCount);
                        arrayElementCount = 0;
                        arguments.add(argument.toString());
                        argument.setLength(0);
                    } else {
                        argument.append(input[index]);
                    }
                    continue parserBlock;
                }
                case ' ': {
                    if (!quoted && !inArray) {
                        if (argument.length() <= 0) continue parserBlock;
                        arguments.add(argument.toString());
                        argument.setLength(0);
                        continue parserBlock;
                    }
                }
                default: {
                    argument.append(input[index]);
                }
            }
        }
        if (argument.length() > 0) arguments.add(argument.toString());
        return arguments.toArray(new String[arguments.size()]);
    }
}
