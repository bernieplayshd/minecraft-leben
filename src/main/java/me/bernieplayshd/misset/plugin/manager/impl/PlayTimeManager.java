package me.bernieplayshd.misset.plugin.manager.impl;

import me.bernieplayshd.misset.api.config.annotations.Config;
import me.bernieplayshd.misset.api.manager.simple.SimpleListManager;
import me.bernieplayshd.misset.api.module.annotations.ModuleData;
import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.manager.impl.internal.RegisterManager;
import org.bukkit.Bukkit;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitTask;

@ModuleData(name = "PlayTime", requires = RegisterManager.class)
public class PlayTimeManager extends SimpleListManager<Player> {

    @Config
    private static long REQUIRED = 7200L;

    @Config
    private static long TASK_INTERVAL = 100L;

    @Config
    private static String WORLD = "world";

    private BukkitTask task;

    public PlayTimeManager() {
        super("Play Time Manager");

        registerListener(new Listener() {

            @EventHandler
            public void onConnect(PlayerJoinEvent event) {
                Player player = event.getPlayer();
                if (getTimePlayed(player) >= REQUIRED) {
                    getManaged().add(player);
                }
            }

            @EventHandler
            public void onDisconnect(PlayerQuitEvent event) {
                getManaged().remove(event.getPlayer());
            }

            @EventHandler
            public void onDamage(EntityDamageByEntityEvent event) {
                if (event.getEntity() instanceof Player && WORLD.equals(event.getEntity().getWorld().getName())) {
                    boolean direct;
                    if ((direct = event.getDamager() instanceof Player) || (event.getDamager() instanceof Projectile
                            &&  ((Projectile) event.getDamager()).getShooter() instanceof Player))
                    {
                        Player damager = direct ? (Player) event.getDamager()
                               : (Player) ((Projectile) event.getDamager()).getShooter();
                        if (!getManaged().contains(damager)) {
                            event.setCancelled(true);
                            damager.sendMessage(MissetPlugin.PREFIX + "§cDu darfst in der Hauptwelt erst einen Spieler angreifen," +
                                    " sofern du §e2 Stunden §cSpielzeit hast!");
                        } else if (!getManaged().contains((Player) event.getEntity())) {
                            damager.sendMessage(MissetPlugin.PREFIX + "§7Du greifst einen Neuling an," +
                                    " §7du musst dazu aber auch einen angemessenen §7Grund haben");
                        }
                    }
                }
            }
        });
    }

    @Override
    public void onEnable() {
        super.onEnable();
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (getTimePlayed(player) >= REQUIRED) {
                getManaged().add(player);
            }
        }

        MissetPlugin plugin;
        this.task = (plugin = MissetPlugin.getPlugin()).getServer().getScheduler()
                .runTaskTimer(plugin, () -> {
                    for (Player player : Bukkit.getOnlinePlayers()) {
                        if (!getManaged().contains(player) && getTimePlayed(player) >= REQUIRED) {
                            getManaged().add(player);
                            player.sendMessage(MissetPlugin.PREFIX + "§aDu hast nun 2 Stunden Spielzeit" +
                                    " und somit die Rechte §aerworben, andere Spieler in der §aHauptwelt anzugreifen");
                        }
                    }
                }, 20L, TASK_INTERVAL);
    }

    /**
     * Returns the play time of the player in seconds
     */
    private long getTimePlayed(Player player) {
        int time = player.getStatistic(Statistic.PLAY_ONE_MINUTE); // player.getStatistic(Statistic.PLAY_ONE_TICK);
        return time * 60; // time == 0 ? 0 : time / 20;
    }

    @Override
    public void onDisable() {
        if (task != null) {
            this.task.cancel();
            this.task = null;
        }
        getManaged().clear();
        super.onDisable();
    }
}
