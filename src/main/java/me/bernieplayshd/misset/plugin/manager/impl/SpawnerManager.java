package me.bernieplayshd.misset.plugin.manager.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import co.aikar.timings.Timing;
import me.bernieplayshd.misset.plugin.Utils;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

import me.bernieplayshd.misset.api.config.annotations.Config;
import me.bernieplayshd.misset.api.item.ItemBuilder;
import me.bernieplayshd.misset.api.manager.simple.DatabaseManager;
import me.bernieplayshd.misset.api.module.annotations.ModuleData;
import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.listeners.SpawnerListener;
import me.bernieplayshd.misset.plugin.manager.impl.internal.RegisterManager;
import me.bernieplayshd.misset.plugin.spawner.Spawner;
import me.bernieplayshd.misset.plugin.spawner.SpawnerMob;
import us.dev.api.command.Command;
import us.dev.api.command.handler.annotations.Executes;

@ModuleData(name = "Spawners", requires = RegisterManager.class)
public class SpawnerManager extends DatabaseManager<Spawner> {

	private final SpawnerListener listener;
	
	public SpawnerManager() {
		super("SpawnerManager", MissetPlugin.getPlugin().getDatabase());
		
		registerListener(this.listener = new SpawnerListener(this));
		
		registerCommand(new Command(MissetPlugin.getPlugin().getCommandManager(), "spawners") {

			@Executes
			public void listSpawners(Player player) {
				List<Spawner> spawners = getSpawnersOf(player.getUniqueId());
				if (!spawners.isEmpty()) {
					player.sendMessage(MissetPlugin.PREFIX + "§7Deine Spawner:");
					for (Spawner spawner : spawners) {
						Block block = spawner.getBlock();
						player.sendMessage(String.format("%s§7- %s §7@ §8%d§7, §8%d§7, §8%d", MissetPlugin.PREFIX,
								spawner.getType().map(SpawnerMob::getPrefix).orElse(SpawnerMob.PIG.getPrefix()),
								block.getX(), block.getY(), block.getZ()));
					}
				} else {
					player.sendMessage(MissetPlugin.PREFIX + "§cDu besitzt derzeit keine Spawner");
				}
			}
		});
	}

	private void loadAll(Object[][] rows) {
		// TODO: test timings
		try (Timing timing = Utils.timing("Load all Spawners")) {
			for (Object[] row : rows) {
				String uuid  = (String) row[1],
					   name  = (String) row[2],
					   world_name = (String) row[6];
				int id = (int) row[0],
					 x = (int) row[3], y = (int) row[4], z = (int) row[5];
				World world = Bukkit.getWorld(world_name);
				if (world == null) {
					continue;
				}
				Block block = world.getBlockAt(x, y, z);
				if (block.getType() != Material.SPAWNER) {
					remove(id);
					continue;
				}
				getManaged().add(new Spawner(id, getType(block).orElse(null), name, UUID.fromString(uuid), block));
			}
		}
	}
	
	@Override
	public void onEnable() {
		super.onEnable();
		if (!getManaged().isEmpty()) {
			return;
		}
		getDatabase().queryRows("SELECT * FROM `spawners`").thenAcceptAsync(this::loadAll, Utils.sync());
	}
	
	@Override
	public void onDisable() {
		listener.onDisable();
		super.onDisable();
	}
	
	public CompletableFuture<Boolean> claim(Player player, Block block) {
		if (block.getType() != Material.SPAWNER) {
			return CompletableFuture.completedFuture(false);
		}

		return getDatabase().update("INSERT INTO `spawners` " +
						"(`player_uuid`, `name`, `blockx`, `blocky`, `blockz`, `block_world`)" +
						" VALUES (?, ?, ?, ?, ?, ?)",
				player.getUniqueId().toString(), player.getName(),
				block.getX(), block.getY(), block.getZ(), block.getWorld().getName())
		.thenCompose(__ -> getDatabase()
			.querySet("SELECT `id` FROM `spawners` ORDER BY ID DESC LIMIT 1",
				rs -> {
					rs.next();
					return rs.getInt("id");
				})
			)
		.thenApplyAsync(id -> getManaged().add(new Spawner(id, getType(block).orElse(null),
				player.getName(), player.getUniqueId(), block)), Utils.sync());
	}
	
	public boolean isClaimable(Block block) {
		return getManaged().stream().noneMatch(spawner -> spawner.getBlock().equals(block));
	}
	
	public boolean isOwner(UUID uuid, Block block) {
		return getManaged().stream().anyMatch(spawner -> spawner.getBlock().equals(block) && spawner.getOwnerUUID().equals(uuid));
	}

	public Optional<String> getName(Block block) {
		return getManaged().stream()
				.filter(spawner -> spawner.getBlock().equals(block))
				.findFirst()
				.map(Spawner::getOwner);
	}

	public CompletableFuture<Optional<String>> getName(UUID uuid) {
		return getDatabase().query("SELECT `name` FROM `spawners` WHERE `player_uuid` = ? LIMIT 1", uuid.toString())
				.thenApply(objects -> objects.length == 1 ? Optional.of((String) objects[0]) : Optional.empty());
	}
	
	public void setName(UUID uuid, String name) {
		getDatabase().update("UPDATE `spawners` SET `name` = ? WHERE `player_uuid` = ?", name, uuid.toString());
	}
	
	public void remove(int id) {
		getDatabase().update("DELETE FROM `spawners` WHERE `id` = ?", id);
		getSpawnerOf(id).ifPresent(getManaged()::remove);
	}
	
	public Optional<SpawnerMob> getType(Block block) {
		BlockState state = block.getState();
		if (!(state instanceof CreatureSpawner)) {
			return Optional.empty();
		}
		return SpawnerMob.of(((CreatureSpawner) state).getSpawnedType());
	}
	
	public Optional<Spawner> getSpawnerOf(Block block) {
		return getManaged().stream().filter(spawner -> spawner.getBlock().equals(block)).findFirst();
	}

	public Optional<Spawner> getSpawnerOf(int id) {
		return getManaged().stream().filter(spawner -> spawner.getID() == id).findFirst();
	}
	
	public List<Spawner> getSpawnersOf(UUID uuid) {
		return getManaged().stream().filter(spawner -> spawner.getOwnerUUID().equals(uuid)).collect(Collectors.toList());
	}

	/*public boolean hasHostileSpawners(UUID uuid) {
		return getSpawnersOf(uuid).stream().anyMatch(spawner -> spawner.getType().map(SpawnerMob::isHostile).orElse(false));
	}*/
	
	//
	//

	@Config
	public static String BUY_TITLE  = "§7Spawner kaufen",
			   			 MENU_TITLE = "§7Wähle eine Aktion aus";
 
	@Config(desc = "Price in diamonds")
	public static int SPAWNER_PRICE = 20;
	@Config(desc = "Price in diamonds")
	public static int GIVE_PRICE = 1;

	public Inventory getBuyInv() {
		Inventory inv = Bukkit.createInventory(null, 9, BUY_TITLE);
		
		ItemStack buy = ItemBuilder.build(Material.EMERALD_BLOCK, "§aKaufen",
				"§7Kaufe diesen Spawner damit kein anderer ihn dir wegnehmen kann",
				String.format("§7Preis: §b%d Diamanten", SPAWNER_PRICE));
		inv.setItem(0, buy);
		
		ItemStack cancel = ItemBuilder.build(Material.REDSTONE_BLOCK, "§cAbbrechen", "§7Bricht den Kauf ab");
		inv.setItem(8, cancel);
		
		return inv;
	}
	
	public Inventory getSpawnerInv(Player player) {
		Inventory inv = Bukkit.createInventory(null, 27, MENU_TITLE);
		
		ItemStack stack = ItemBuilder.build(Material.SPAWNER, "§8Spawner mitnehmen",
				"§7Gibt dir deinen Spawners ins Inventar",
				"§c§nAchtung§r§c: §7Bei erneutem Platzieren muss",
				"§7der Spawner neu gekauft werden!",
				String.format("§7Preis: §b%d Diamanten", GIVE_PRICE));
		inv.setItem(0, stack);

		for (int x = 0; x < SpawnerMob.values().length; x++) {
			SpawnerMob mob = SpawnerMob.values()[x];
			/*if (mob.isHostile() && hasHostileSpawners(player.getUniqueId())) {
				continue;
			}*/
			ItemStack skull = new ItemStack(Material.PLAYER_HEAD, 1, (short) 3);
			
			SkullMeta meta = (SkullMeta) Bukkit.getItemFactory().getItemMeta(Material.PLAYER_HEAD);
			// meta.setOwner(mob.getHead());
			meta.setOwningPlayer(Bukkit.getOfflinePlayer(mob.getHead()));
			meta.setDisplayName(mob.getPrefix());
			meta.setLore(Arrays.asList(String.format("§7Ändere das Spawnmob in %s §7um", mob.getPrefix()),
				mob.getDescription(),  String.format("§7Preis: §b%d Diamanten", mob.getPrice())));
			
			skull.setItemMeta(meta);
			inv.setItem(x + 1, skull);
		}
		return inv;
	}
}
