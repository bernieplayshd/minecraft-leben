package me.bernieplayshd.misset.plugin.manager.impl;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;
import java.util.stream.Collectors;

import me.bernieplayshd.misset.plugin.commands.BackpackCommand;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.ImmutableMap;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.JsonPrimitive;

import me.bernieplayshd.misset.api.config.annotations.Config;
import me.bernieplayshd.misset.api.item.ItemBuilder;
import me.bernieplayshd.misset.api.manager.simple.DatabaseManager;
import me.bernieplayshd.misset.api.module.annotations.ModuleData;
import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.backpack.Backpack;
import me.bernieplayshd.misset.plugin.backpack.BackpackType;
import me.bernieplayshd.misset.plugin.listeners.BackpackListener;
import me.bernieplayshd.misset.plugin.manager.impl.internal.RegisterManager;

@ModuleData(name = "Backpacks", requires = RegisterManager.class)
public class BackpackManager extends DatabaseManager<Backpack> {
	
	// Reflections
	private static Method SERIALIZE_METHOD, DESERIALIZE_METHOD;
	private static Field  SERIALIZE_CLASS_MAP;
	
	static {
		try {
			Class<?> metaItem = Class.forName("org.bukkit.craftbukkit.v1_14_R1.inventory.CraftMetaItem");
			
			SERIALIZE_METHOD = metaItem.getDeclaredMethod("serialize", ImmutableMap.Builder.class);
			SERIALIZE_METHOD.setAccessible(true);
			
			DESERIALIZE_METHOD = metaItem.getClasses()[0].getMethod("deserialize", Map.class);
			DESERIALIZE_METHOD.setAccessible(true);
			
			SERIALIZE_CLASS_MAP = metaItem.getClasses()[0].getDeclaredField("classMap");
			SERIALIZE_CLASS_MAP.setAccessible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public BackpackManager() {
		super("Backpack Manager", MissetPlugin.getPlugin().getDatabase());
		
		registerCommand(new BackpackCommand(this));
		
		registerListener(new BackpackListener(this));
	}
	
	public static final Map<UUID, Backpack> RENAMING = new HashMap<>();
	
	private static final Gson GSON = new GsonBuilder()
			 //.serializeNulls()
			 .registerTypeAdapter(Map.class, new StringObjectMapDeserializer())
			 .create();

    public List<Backpack> getBackpacksOf(UUID owner) {
		return getManaged().stream().filter(pack -> pack.getOwner().equals(owner)).collect(Collectors.toList());
	}
	
	public Optional<Backpack> getBackpackOf(Collection<Backpack> packs, int id) {
	    return packs.stream().filter(pack -> pack.getID() == id).findAny();
	}

	public Optional<Backpack> getBackpackOf(int id) {
		return getBackpackOf(getManaged(), id);
	}

    public CompletableFuture<Integer> create(UUID owner, BackpackType type) {
        return getDatabase().update("INSERT INTO `backpacks` (`player_uuid`, `type`, `content`) VALUES (?, ?, ?)",
                owner.toString(), type.ordinal() + 1, "[]")
				// TODO: improve, very bad practice
				.thenCompose(__ -> getDatabase()
						.querySet("SELECT `id` FROM `backpacks` ORDER BY ID DESC LIMIT 1",
								rs -> {
									rs.next();
									return rs.getInt("id");
								})
				);
    }

    @SuppressWarnings("unchecked")
    public CompletableFuture<Integer> save(Backpack pack) {
        String serialized;

        serialization: {
            Inventory inventory = pack.getInventory();
            final int size = inventory.getContents().length;

            if (size == 0) {
                serialized = "[]";
                break serialization; // break label
            }

            Map<String, Object>[] items = new Map[size];

            int slot = 0;
            for (ItemStack stack : inventory.getContents()) {
                if (stack != null) {
                    items[slot] = stack.serialize();

                    if (items[slot].containsKey("meta")) {
                        try {
                            Object instance = items[slot].get("meta");

                            ImmutableMap<Class<?>, String> classMap =
                                    (ImmutableMap<Class<?>, String>) SERIALIZE_CLASS_MAP.get(null);

                            ImmutableMap.Builder<String, Object> withType = new ImmutableMap.Builder<String, Object>()
                                    .put("meta-type", classMap.get(instance.getClass()));

                            ImmutableMap.Builder<String, Object> newMeta =
                                    (ImmutableMap.Builder<String, Object>) SERIALIZE_METHOD.invoke(instance, withType);

                            items[slot].put("meta", newMeta.build());

                        } catch (Exception e) {
                            getLogger().log(Level.SEVERE, "Failed to correct item meta serialization", e);
                        }
                    }
                    slot++;
                    continue;
                }
                items[slot++] = null;
            }

            serialized = GSON.toJson(items, Map[].class);
        } // end label

        return getDatabase().update("UPDATE `backpacks` SET `content` = ? WHERE `id` = ?", serialized, pack.getID());
    }
	
	/**
	 * Updates the name of the backpack. The <b>&</b> character gets replaced by the <b>§</b>
	 */
    public CompletableFuture<Integer> updateName(Backpack pack, String name) {
        name = name.replace('&', '§');

        pack.setName(name);
        return getDatabase().update("UPDATE `backpacks` SET `name` = ? WHERE `id` = ?", name, pack.getID());
    }

	@SuppressWarnings("unchecked")
	private Backpack parse(int id, String uuid, int type, String content, String name) {
		BackpackType newType = BackpackType.values()[type - 1];
		Map<String, Object>[] items = GSON.fromJson(content, Map[].class);

		Inventory inv = Bukkit.createInventory(null, newType.getSize(),
				String.format("§8Backpack-%d [%s§8]", id, newType.getDisplay()));
		
		int slot = 0;
		for (Map<String, Object> item : items) {
			if (item != null) {
				ItemStack stack = ItemStack.deserialize(item);
				if (stack != null && item.get("meta") != null) {
					try {
						ItemMeta itemMeta = (ItemMeta) DESERIALIZE_METHOD
								.invoke(null, (Map<String, Object>) item.get("meta"));
						stack.setItemMeta(itemMeta);
					} catch (IllegalAccessException | InvocationTargetException e) {
						getLogger().log(Level.SEVERE, "Failed to deserialize item meta", e);
					}
				}
				inv.setItem(slot, stack);
			}
			slot++;
		}
		
		return new Backpack(id, newType, UUID.fromString(uuid), inv, name);
	}

	/**
	 * Loads all backpacks of the {@code uuid} in memory
	 */
    public CompletableFuture<?> load(UUID uuid) {
        return getDatabase().queryRows("SELECT `id`, `type`, `content`, `name`"
				+ " FROM `backpacks` WHERE `player_uuid` = ?", uuid.toString())
			.thenAccept(rows -> {
				for (Object[] row : rows) {
					Backpack pack = parse((int) row[0], uuid.toString(), (int) row[1], (String) row[2], (String) row[3]);
					getManaged().add(pack);
				}
			});
    }

	/**
	 * Unloads all backpacks of the {@code uuid}
	 */
	public void unload(UUID uuid) {
		for (Backpack pack : getBackpacksOf(uuid)) {
			save(pack).whenComplete((__, ex) -> {
				if (ex != null) {
					getLogger().log(Level.SEVERE, "Failed to unload backpack", ex);
				}
				getManaged().remove(pack);
			});
		}
	}
	
	public void delete(Backpack pack) {
		getDatabase().update("DELETE FROM `backpacks` WHERE `id` = ?", pack.getID())
				.whenComplete((__, ex) -> {
					if (ex != null) {
						getLogger().log(Level.SEVERE, "Failed to delete backpack", ex);
					}
					getManaged().remove(pack);
				});
	}
	
	public boolean contains(UUID uuid) {
		return getManaged().stream().anyMatch(pack -> pack.getOwner().equals(uuid));
	}
	
	@Override
	public void onEnable() {
		super.onEnable();
		Bukkit.getOnlinePlayers().stream().map(Player::getUniqueId).forEach(this::load);
	}
	
	@Override
	public void onDisable() {
    	getManaged().forEach(this::save);
		
		Bukkit.getOnlinePlayers().stream()
			.filter(player -> player.getOpenInventory().getTitle().startsWith("§8Backpack")
				           || player.getOpenInventory().getTitle().startsWith("§8Backpacks von"))
			.forEach(Player::closeInventory);
		
		BackpackCommand.INVSEE.forEach((player, invsee) -> getManaged().removeAll(getBackpacksOf(invsee)));
		BackpackCommand.INVSEE.clear();
		
		RENAMING.keySet().stream()
			.map(Bukkit::getPlayer)
			.forEach(player -> player.sendMessage(MissetPlugin.PREFIX + "§cDie Namensumnennung wurde abgebrochen"));
		RENAMING.clear();
		super.onDisable();
	}
	
	//
	//
	
	@Config
	public static String SELECT_TITLE = "§8Wähle ein Backpack aus";
	
	public ItemStack getCertificate(BackpackType type) {
		return ItemBuilder.build(Material.PAPER, String.format("§7Zertifikat für Backpack (%s§7)", type.getDisplay()));
	}
	
	public Inventory getPackSelectGui(UUID uuid, boolean invsee) {
		OfflinePlayer player = Bukkit.getOfflinePlayer(uuid);
		Inventory inv = Bukkit.createInventory(null, 9, invsee ? "§8Backpacks von §b" + player.getName() : SELECT_TITLE);
		
		List<Backpack> backpacks = getBackpacksOf(uuid);
		backpacks.sort(Comparator.comparingInt(Backpack::getID));
		backpacks.forEach(pack -> {
			String display = String.format("§7Backpack-%d [%s§7]", pack.getID(), pack.getType().getDisplay());
			inv.addItem(ItemBuilder.build(Material.CHEST, pack.hasName() ?
							String.format("§r%s §r%s", pack.getName().get(), display) : display,
					"§8[Linksklick] §7- Backpack öffnen",
					"§8[Rechtsklick] §7- Backpack umbennen",
					"§8[Wegwerfen] §7- Backpack löschen"));
		});
		
		return inv;
	}

	// Gson sucks at differentiating between a float and an integer
	private static final class StringObjectMapDeserializer implements JsonDeserializer<Map<String, Object>> {
		
		@Override
		public Map<String, Object> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context)
				throws JsonParseException {
			Map<String, Object> m = new LinkedHashMap<>();
			JsonObject jo = json.getAsJsonObject();
			for (Map.Entry<String, JsonElement> mx : jo.entrySet()) {
				final String key = mx.getKey();
				final JsonElement v = mx.getValue();
	
				if (v.isJsonArray()) {
					m.put(key, GSON.fromJson(v, List.class));
				} else if (v.isJsonPrimitive()) {
					Number num = null;
					ParsePosition position = new ParsePosition(0);
				    String vString = v.getAsString();
	
					try {
						num = NumberFormat.getInstance(Locale.ROOT).parse(vString, position);
					} catch (Exception e) {
						e.printStackTrace();
					}
	
					if (num != null && (vString.contains(".") || vString.contains(","))) {
						num = num.doubleValue();
					}
	
					if (num instanceof Long) {
						num = num.intValue();
					}
	
					// Check if the position corresponds to the length of the string
					if (position.getErrorIndex() < 0 && vString.length() == position.getIndex()) {
						if (num != null) {
							m.put(key, num);
							continue;
						}
					}
	
					JsonPrimitive prim = v.getAsJsonPrimitive();
					if (prim.isBoolean()) {
						m.put(key, prim.getAsBoolean());
					} else if (prim.isString()) {
						m.put(key, prim.getAsString());
					} else {
						m.put(key, null);
					}
	
				} else if (v.isJsonObject()) {
					m.put(key, GSON.fromJson(v, Map.class));
				} 
	
			}
			return m;
		}
	}
}
