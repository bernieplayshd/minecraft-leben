package me.bernieplayshd.misset.plugin.manager.impl.internal;

import java.io.File;
import java.lang.reflect.Field;
import java.util.*;
import java.util.logging.Level;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.serialization.ConfigurationSerializable;
import org.bukkit.configuration.serialization.ConfigurationSerialization;
import com.google.common.base.Joiner;
import com.google.common.collect.ImmutableSet;
import com.google.common.primitives.Primitives;

import me.bernieplayshd.misset.api.module.Module;
import me.bernieplayshd.misset.api.config.ConfigItem;
import me.bernieplayshd.misset.api.config.annotations.Config;
import me.bernieplayshd.misset.api.manager.simple.SimpleMapManager;
import me.bernieplayshd.misset.api.module.annotations.ModuleData;
import me.bernieplayshd.misset.plugin.MissetPlugin;

@ModuleData(name = "Config")
public class ConfigManager extends SimpleMapManager<Module, Collection<ConfigItem>> {

	private static ImmutableSet<Class<?>> SERIALIZABLE;
	
	static {
		try {
			Field registry = ConfigurationSerialization.class.getDeclaredField("aliases");
			registry.setAccessible(true);
			
			@SuppressWarnings("unchecked")
			Map<String, Class<? extends ConfigurationSerializable>> aliases =
					(Map<String, Class<? extends ConfigurationSerializable>>) registry.get(null);
			
			ImmutableSet.Builder<Class<?>> builder = ImmutableSet.builder();
			builder.addAll(aliases.values());
			builder.addAll(Primitives.allPrimitiveTypes());
			builder.add(String.class);
			builder.add(List.class);
			
			SERIALIZABLE = builder.build();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	private FileConfiguration config;
	
	public ConfigManager() {
		super("Config Manager");
	}
	
	@Override
	public void onEnable() {
		MissetPlugin plugin = MissetPlugin.getPlugin();
		
	    if (!plugin.getDataFolder().exists() && !plugin.getDataFolder().mkdirs()) {
	    	getLogger().log(Level.SEVERE, "Failed to create plugin data folder");
        }

	    if (!new File(plugin.getDataFolder(), "config.yml").exists()) {
	    	plugin.saveDefaultConfig(); // creates file
	    }
	    
		this.config = plugin.getConfig();
	}
	
	@Override
	public void onDisable() {
		getManaged().clear();
		MissetPlugin.getPlugin().saveConfig();
	}

	public void load(Module module) {
		for (Field field : module.getClass().getDeclaredFields()) {
			Config config = field.getDeclaredAnnotation(Config.class);

			if (config == null || !SERIALIZABLE.contains(field.getType())) {
				continue;
			}
			// TODO: check for recursion -> List<List<...>>
			if (field.getType() == List.class && !SERIALIZABLE.contains(field.getGenericType())) {
				continue;
			}
			
			field.setAccessible(true);
			
			try {
				Object value = field.get(module);
				
				Collection<ConfigItem> items = getManaged().getOrDefault(module, new LinkedList<>());
				String name = config.name();
				ConfigItem item = new ConfigItem(module, field, name.isEmpty() ? field.getName().toLowerCase()
						: name, config.desc());
				items.add(item);
				getManaged().put(module, items);
			
				String path = combine(module.getName().toLowerCase(), item.getName());
				if (this.config.contains(path)) {
					Object obj = this.config.get(path);
					
					// Spigot's config system sucks at differentiating between a float and a double...
					if (field.getType() == Float.TYPE
							&& !(obj instanceof Float) && obj instanceof Number)
					{
						obj = ((Number) obj).floatValue();
					}
					// ... and doesn't support arrays, just lists
					if (field.getType().isArray()) {
						obj = ((List<?>) obj).toArray();
					}

					field.set(module, obj);
				} else {
					this.config.set(path, value);
				}
				 
			} catch (Exception e) {
				getLogger().log(Level.SEVERE, "Couldn't access/change value of field", e);
			}
		}
	}
	
	public void unload(Module module) {
		if (getManaged().containsKey(module)) {
			for (ConfigItem item : getManaged().get(module)) {
				String path = combine(module.getName().toLowerCase(), item.getName());
				try {
					this.config.set(path, item.getField().getType().isArray()
							? Arrays.asList((Object[]) item.getValue()) : item.getValue());
				} catch (Exception e) {
					getLogger().log(Level.SEVERE, "Couldn't access value of field", e);
				}
			}
		}
	}
	
	private static final Joiner COMBINE_JOINER = Joiner.on('.');
	
	private String combine(String ... elements) {
		return COMBINE_JOINER.join(elements).replace(' ', '_');
	}
	
	public FileConfiguration getConfig() {
		return config;
	}
}
