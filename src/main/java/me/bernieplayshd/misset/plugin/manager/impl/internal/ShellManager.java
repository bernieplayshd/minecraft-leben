package me.bernieplayshd.misset.plugin.manager.impl.internal;

import me.bernieplayshd.misset.api.manager.simple.SimpleMapManager;
import me.bernieplayshd.misset.plugin.manager.impl.internal.ShellManager.*;
import me.bernieplayshd.misset.api.module.annotations.ModuleData;
import me.bernieplayshd.misset.plugin.MissetPlugin;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import us.dev.api.command.Command;
import us.dev.api.command.handler.annotations.Executes;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.nio.file.NoSuchFileException;
import java.util.concurrent.TimeUnit;

@ModuleData(name = "Shell")
public class ShellManager extends SimpleMapManager<Player, ShellContext> {

    private static String EXEC_PREFIX, DEFAULT_WORKING_DIR, HOME_PATH;

    static {
        try {
            DEFAULT_WORKING_DIR = MissetPlugin.getPlugin().getDataFolder().getParent();

            HOME_PATH = System.getProperty("user.home");

            EXEC_PREFIX = "§a" + System.getProperty("user.name") + "@server:§9%s§r$ ";
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final MissetPlugin plugin;

    public ShellManager() {
        super("Shell Manager");

        this.plugin = MissetPlugin.getPlugin();

        registerListener(new Listener() {

            @EventHandler
            public void onDisconnect(PlayerQuitEvent event) {
                Player player = event.getPlayer();

                if (inShell(player)) leaveShell(player);
            }

            @EventHandler(priority = EventPriority.MONITOR)
            public void onChatMessage(AsyncPlayerChatEvent event) {
                Player  player = event.getPlayer();
                String message = event.getMessage();

                if (inShell(player)) {
                    event.setCancelled(true);
                    if ("exit".equalsIgnoreCase(message)) {
                        leaveShell(player);
                    } else if (message.startsWith("cd")) {
                        /*
                         * Simple implementation of the 'change directory' (cd) command
                         */
                        try {
                            ShellContext ctx = getContextOf(player);
                            File newWorkingDir = ctx.getWorkingDirectory().toPath()
                                    .resolve(message.substring(3))
                                    .normalize()
                                    .toFile();

                            if (!newWorkingDir.isDirectory() || !newWorkingDir.exists()) {
                                player.sendMessage(MissetPlugin.PREFIX + "§cDieses Verzeichnis existiert nicht!");
                                return;
                            }

                            ctx.setWorkingDirectory(newWorkingDir);

                            player.sendMessage(String.format("%s§7New Working Directory: '%s'",
                                    MissetPlugin.PREFIX, newWorkingDir.getAbsolutePath()));
                        } catch (Exception e) {
                            player.sendMessage(MissetPlugin.ERROR);
                        }
                    } else {
                        executeCommand(player, message);
                    }
                }
            }
        });

        registerCommand(new Command(plugin.getCommandManager(), "shell", "system.admin") {

            @Executes
            public void shell(Player player) {
                if (inShell(player)) leaveShell(player);
                else joinShell(player);
            }
        });
    }

    public boolean inShell(Player player) {
        return getManaged().containsKey(player);
    }

    public ShellContext getContextOf(Player player) {
        return getManaged().get(player);
    }

    public void joinShell(Player player) {
        getManaged().put(player, new ShellContext(new File(DEFAULT_WORKING_DIR)));
        player.sendMessage(MissetPlugin.PREFIX + "§7Du bist in den Shell-Mode übergegangen");
        player.sendMessage(MissetPlugin.PREFIX + "§7Um ihn zu verlassen schreibe '§eexit§7' in den Chat");
    }

    public void leaveShell(Player player) {
        getManaged().remove(player);
        player.sendMessage(MissetPlugin.PREFIX + "§7Du hast den Shell-Mode verlassen");
    }

    private void executeCommand(Player sender, String command) {
        ShellContext ctx = getContextOf(sender);
        sender.sendMessage(getExecutionPrefix(ctx).concat(command));
        plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> processCommand(sender, ctx, command));
    }

    private String getExecutionPrefix(ShellContext ctx) {
        String wDir = ctx.getWorkingDirectory().getAbsolutePath();
        return String.format(EXEC_PREFIX, wDir.startsWith(HOME_PATH) ? ("~".concat(wDir.substring(HOME_PATH.length()))) : wDir);
    }

    private void processCommand(Player sender, ShellContext ctx, String input) {
        try {
            Process process = new ProcessBuilder(input.split(" "))
                    .directory(ctx.getWorkingDirectory())
                    .start();
            // process.onExit().thenAcceptAsync(proc -> {}, Utils.sync());

            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(process.getInputStream() /* ironically, thats the processes output stream */ )
            );

            String line;
            while ((line = reader.readLine()) != null) {
                sender.sendMessage(line);
            }

            process.waitFor(5L, TimeUnit.MINUTES);

            reader.close();

            sender.sendMessage(String.format("%s§7Der Prozess ist mit der Exit Value %d beendet",
                    MissetPlugin.PREFIX, process.exitValue()));

        } catch (Exception e) {
            if (e instanceof NoSuchFileException) {
                sender.sendMessage(MissetPlugin.PREFIX + "§cDiese Datei oder Befehl existiert nicht!");
                return;
            }
            e.printStackTrace();
            sender.sendMessage(MissetPlugin.ERROR);
        }
    }

    public static class ShellContext {

        private File workingDir;

        public ShellContext(File workingDir) {
            this.workingDir = workingDir;
        }

        public File getWorkingDirectory() {
            return workingDir;
        }

        public void setWorkingDirectory(File workingDir) {
            this.workingDir = workingDir;
        }
    }
}
