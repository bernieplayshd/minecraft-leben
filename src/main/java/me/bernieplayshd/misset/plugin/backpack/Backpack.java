package me.bernieplayshd.misset.plugin.backpack;

import java.util.Optional;
import java.util.UUID;

import org.bukkit.inventory.Inventory;

public class Backpack {
	
	private final int id;
	private final BackpackType type;
	private final UUID owner;
	private String name;
	
	private final Inventory inventory;
	
	public Backpack(int id, BackpackType type, UUID owner, Inventory inv, String name) {
		this.id = id;
		this.type = type;
		this.owner = owner;
		this.inventory = inv;
		this.name = name == null || name.isEmpty() ? null : name;
	}

	public int getID() {
		return id;
	}

	public BackpackType getType() {
		return type;
	}

	public UUID getOwner() {
		return owner;
	}

	public Inventory getInventory() {
		return inventory;
	}

	public Optional<String> getName() {
		return Optional.ofNullable(name);
	}
	
	public boolean hasName() {
		return name != null;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public static class Builder {
		private int id;
		private BackpackType type;
		private UUID owner;
		private Inventory inventory;
		private String name = null;

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder from(UUID owner) {
			this.owner = owner;
			return this;
		}

		public Builder of(Inventory inventory) {
			this.inventory = inventory;
			return this;
		}

		public Builder ofType(BackpackType type) {
			this.type = type;
			return this;
		}

		public Builder withName(String name) {
			this.name = name;
			return this;
		}

		public Backpack build() {
			return new Backpack(id, type, owner, inventory, name);
		}
	}
}
