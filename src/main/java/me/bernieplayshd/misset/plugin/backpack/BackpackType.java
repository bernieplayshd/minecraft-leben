package me.bernieplayshd.misset.plugin.backpack;

import java.util.Arrays;
import java.util.Optional;

public enum BackpackType {
	
	SMALL(9, "§7Klein"),
	MEDIUM(18, "§bMedium"),
	// Adel
	LARGE(27, "§cGroß");
	
	private final int size;
	private final String display;
	
	private BackpackType(int size, String display) {
		this.size = size;
		this.display = display;
	}
	
	public int getSize() {
		return size;
	}
	
	public String getDisplay() {
		return display;
	}

	public static Optional<BackpackType> of(String display) {
		return Arrays.stream(values()).filter(type -> type.getDisplay().equals(display)).findAny();
	}
}
