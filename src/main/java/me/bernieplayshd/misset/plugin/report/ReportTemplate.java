package me.bernieplayshd.misset.plugin.report;

import java.util.Arrays;
import java.util.Optional;

import org.bukkit.Material;

import me.bernieplayshd.misset.plugin.manager.impl.ReportManager;

public enum ReportTemplate {
	
	GRIEFING(ReportManager.MONTH, "§cGriefing", Material.TNT,
			"§7Nicht erlaubtes Abbauen oder Platzieren von Blöcken"),
	KILLING(0L, "§6Grundloses Töten", Material.DIAMOND_SWORD,
			"§7Töten von einem anderen Spieler ohne erlaubten Grund"),
	HACKING(ReportManager.MONTH, "§4Hacking", Material.COMMAND_BLOCK,
			"§7Benutzen von verbotenen Client-Modifikationen",
			"§7wie XRAY, Speed, Killaura, ESP, etc."),
	CHAT(0L, "§9Chat-Verstöße", Material.PAPER,
			"§7Spammen, Werbung, Beleidigungen,",
			"§7Respektlosigkeit vor den Mitspielern, etc."),
	OTHER(0L, "§3Anderer Grund", Material.NETHER_STAR,
			"§7Ein anderer Grund, den du berechtigt findest,",
			"§7Ausnutzen wird bestraft");

	private final long time;
	private final String display, description[];
	private final Material item;
	
	private ReportTemplate(long time, String display, Material item, String ... desc) {
		this.time = time;
		this.display = display;
		this.item = item;
		this.description = desc;
	}

	public long getTime() {
		return time;
	}

	public String getDisplay() {
		return display;
	}
	
	public Material getItem() {
		return item;
	}
	
	public static Optional<ReportTemplate> of(Material item) {
		return Arrays.stream(values()).filter(temp -> temp.getItem() == item).findAny();
	}

	public String[] getDescription() {
		return description;
	}
}
