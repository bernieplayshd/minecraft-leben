package me.bernieplayshd.misset.plugin.report;

import java.util.UUID;

public class Report {
	
	private final int id;
	
	private final UUID reported, sender;
	private final ReportTemplate template;
	private final long timestamp;
	
	private boolean finished;

	public Report(int id, UUID reported, UUID sender, ReportTemplate template, long timestamp) {
		this.id = id;
		this.reported = reported;
		this.sender = sender;
		this.template = template;
		this.timestamp = timestamp;
		this.finished = false;
	}
	
	public int getID() {
		return id;
	}
	
	public UUID getReported() {
		return reported;
	}

	public UUID getSender() {
		return sender;
	}

	public ReportTemplate getTemplate() {
		return template;
	}

	public long getTimeStamp() {
		return timestamp;
	}
	
	public boolean isFinished() {
		return finished;
	}
	
	public void finish() {
		this.finished = true;
	}

	public static class Builder {
		private int id;
		private UUID reported, sender;
		private ReportTemplate template;
		private long time;

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder of(UUID reported) {
			this.reported = reported;
			return this;
		}

		public Builder from(UUID sender) {
			this.sender = sender;
			return this;
		}

		public Builder with(ReportTemplate template) {
			this.template = template;
			return this;
		}

		public Builder at(long time) {
			this.time = time;
			return this;
		}

		public Report build() {
			return new Report(id, reported, sender, template, time);
		}
	}
}
