package me.bernieplayshd.misset.plugin.commands;

import me.bernieplayshd.misset.api.config.ConfigItem;
import me.bernieplayshd.misset.api.item.ItemBuilder;
import me.bernieplayshd.misset.api.manager.AbstractManager;
import me.bernieplayshd.misset.api.module.AbstractModule;
import me.bernieplayshd.misset.api.module.ToggleableModule;
import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.Utils;
import me.bernieplayshd.misset.plugin.manager.impl.internal.CommandManager;
import me.bernieplayshd.misset.plugin.manager.impl.internal.RegisterManager;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import us.dev.api.command.Command;
import us.dev.api.command.handler.annotations.Executes;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;

public class ModuleCommand extends Command {

    private final MissetPlugin plugin;

    public ModuleCommand(CommandManager manager, MissetPlugin plugin) {
        super(manager, "modules", "system.admin");
        this.plugin = plugin;
    }

    @Executes("list|ls")
    public void listModules(Player player) {
        RegisterManager registerManager = plugin.getRegisterManager();
        Collection<AbstractManager<?>> abstractManagers = registerManager.getManaged().values();
        AbstractModule[] modules  = new AbstractModule[2 + abstractManagers.size()];

        modules[0] = plugin.getConfigManager();
        modules[1] = registerManager;
        System.arraycopy(abstractManagers.toArray(new AbstractManager[0]),
                0, modules, 2, abstractManagers.size());

        try {
            player.openInventory(getModuleInv(modules));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static final String MODULES_TITLE = "§8Modules";

    private Inventory getModuleInv(AbstractModule[] modules) throws Exception {
        Inventory inv = Bukkit.createInventory(null, Utils.inventorySize(modules.length), MODULES_TITLE);
        for (AbstractModule module : modules) {
            List<String> lore = new LinkedList<>();
            if (module.getDependencies().length > 0) {
                lore.add("");
                lore.add("§9Dependencies:");
                for (Class<? extends AbstractModule> req : module.getDependencies()) {
                    lore.add(" - §8" + req.getSimpleName());
                }
            }
            if (plugin.getConfigManager().getManaged().containsKey(module)) {
                lore.add("");
                lore.add("§9Config Values:");
                for (ConfigItem item : plugin.getConfigManager().getManaged().get(module)) {
                    lore.add(String.format("§7 - §8%s §7(§b%s§r§7) = §c%s", item.getName(),
                            item.getField().getType().getSimpleName(), item.getValue().toString()));
                }
            }
            if (!module.getDescription().isEmpty()) {
                lore.add("");
                lore.add("§7Description:");

                String[] splitted = module.getDescription().split("\n");
                for (String line : splitted) {
                    lore.add("§7" + line);
                }
            }

            ItemStack item = ItemBuilder.build(Material.STONE, "§9" + module.getName(), lore.toArray(new String[0]));
            if (module instanceof ToggleableModule && ((ToggleableModule) module).isRunning()) {
                ItemMeta meta = item.getItemMeta();
                meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
                meta.addEnchant(Enchantment.DURABILITY, 1, true);
                item.setItemMeta(meta);
            }
            inv.addItem(item);
        }
        return inv;
    }
}
