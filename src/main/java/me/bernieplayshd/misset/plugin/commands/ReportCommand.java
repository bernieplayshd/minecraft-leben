package me.bernieplayshd.misset.plugin.commands;

import me.bernieplayshd.misset.api.timing.DelayCommand;
import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.manager.impl.ReportManager;
import me.bernieplayshd.misset.plugin.report.Report;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import us.dev.api.command.handler.annotations.Executes;

import java.util.Collection;
import java.util.Date;
import java.util.Optional;

public class ReportCommand extends DelayCommand {

    private final ReportManager manager;

    public ReportCommand(ReportManager manager) {
        super(MissetPlugin.getPlugin().getCommandManager(), "report", 30000L);
        this.manager = manager;
    }

    @Executes(value = "edit", permission = "system.support")
    public String editReport(Player player, int id) {
        Optional<Report> optional = manager.getReportOf(id);
        if (!optional.isPresent()) {
            return MissetPlugin.PREFIX + "§cKonnte den Report unter der ID nicht finden";
        }
        player.openInventory(manager.getMenuGui(optional.get()));
        return null;
    }

    @Executes(value = "list|ls", permission = "system.support")
    public void listReports(Player player) {
        Collection<Report> reports = manager.getManaged();
        player.sendMessage(String.format("%s§7Liste aller offenen Reports (§e%d§7):", MissetPlugin.PREFIX, reports.size()));
        for (Report report : reports) {
            OfflinePlayer reported = Bukkit.getOfflinePlayer(report.getReported()),
                          sender2  = Bukkit.getOfflinePlayer(report.getSender());

            if (reported == null || sender2 == null) {
                continue;
            }

            TextComponent text = new TextComponent();
            text.setText(String.format("§7- §e%s §7-> §c%s §7wegen %s §8[§7%s§8] ",
                    sender2.getName(), reported.getName(), report.getTemplate().getDisplay(),
                    ReportManager.DATE_FORMAT.format(new Date(report.getTimeStamp())))
            );

            TextComponent clickable = new TextComponent("§8[§9Bearbeiten§8]");
            clickable.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/report edit " + report.getID()));
            clickable.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponent[] {
                    new TextComponent("§7Linksklick um zu bearbeiten") }));
            text.addExtra(clickable);

            player.spigot().sendMessage(text);
        }
    }

    @Executes
    public String report(Player player, String otherPlayer) {
        if (player.getName().equalsIgnoreCase(otherPlayer)) {
            return MissetPlugin.PREFIX + "§cDu kannst dich nicht selber melden!";
        }

        @SuppressWarnings("deprecation")
        OfflinePlayer offline = Bukkit.getOfflinePlayer(otherPlayer);
        if (offline == null || (!offline.hasPlayedBefore() && !offline.isOnline())) {
            return MissetPlugin.PREFIX + "§cDieser Spieler ist dem Server nicht bekannt!";
        }

        if (!checkCooldown(player)) {
            player.openInventory(manager.getReportGui(otherPlayer));
        }

        return null;
    }
}
