package me.bernieplayshd.misset.plugin.commands;

import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.Utils;
import me.bernieplayshd.misset.plugin.backpack.Backpack;
import me.bernieplayshd.misset.plugin.backpack.BackpackType;
import me.bernieplayshd.misset.plugin.manager.impl.BackpackManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import us.dev.api.command.Command;
import us.dev.api.command.handler.annotations.Executes;

import java.util.*;

public class BackpackCommand extends Command {

    public static final Map<Player, UUID> INVSEE = new HashMap<>();

    private final BackpackManager manager;

    public BackpackCommand(BackpackManager manager) {
        super(MissetPlugin.getPlugin().getCommandManager(), "backpack");
        this.manager = manager;
    }

    @Executes("menu|m")
    public String openMenu(Player player) {
        if (manager.getBackpacksOf(player.getUniqueId()).isEmpty()) {
            return MissetPlugin.PREFIX + "§cDu besitzt keine Backpacks!";
        }
        player.openInventory(manager.getPackSelectGui(player.getUniqueId(), false));
        return null;
    }

    @Executes("delete|del|remove|rem")
    public String deleteBackpack(Player player, int id) {
        Optional<Backpack> optional = manager.getBackpackOf(id);
        if (!optional.isPresent()) {
            return MissetPlugin.PREFIX + "§cEin Backpack mit dieser ID existiert nicht!";
        }
        Backpack backpack = optional.get();

        if (!backpack.getOwner().equals(player.getUniqueId()) && !player.hasPermission("system.admin")) {
            return MissetPlugin.NO_PERMISSIONS;
        }

        ItemStack[] items = backpack.getInventory().getContents();
                    items = Arrays.stream(items).filter(Objects::nonNull).toArray(ItemStack[]::new);
        for (ItemStack item : items) {
            if (!Utils.canStore(item, player.getInventory())) {
                return MissetPlugin.PREFIX + "§cIn deinem Inventar ist kein §cPlatz, um die Items von deinem §cBackpack aufzunehmen";
            }
        }
        player.getInventory().addItem(items);

        manager.delete(backpack);

        return MissetPlugin.PREFIX + "§7Du hast dein Backpack erfolgreich gelöscht" + System.lineSeparator() +
               MissetPlugin.PREFIX + "§7In deinem Inventar befinden sich die §7Items, die sich zuletzt im §7Backpack befunden haben";
    }

    @Executes(value = "give|g", permission = "system.admin")
    public void giveBackpack(Player player, BackpackType type) {
        player.getInventory().addItem(manager.getCertificate(type));
    }

    @Executes(value = "see", permission = "system.support")
    public String seeBackpacks(Player p, String otherPlayer) {
        @SuppressWarnings("deprecation")
        OfflinePlayer player = Bukkit.getOfflinePlayer(otherPlayer);
        if (player == null || (!player.hasPlayedBefore() && !player.isOnline())) {
            return MissetPlugin.PREFIX + "§cDieser Spieler existiert nicht!";
        }

        if (player.getUniqueId().equals(p.getUniqueId())) {
           return MissetPlugin.PREFIX + "§cDu kannst deine eigenen Backpacks nicht spectaten!";
        }

        if (!manager.contains(player.getUniqueId())) {
            manager.load(player.getUniqueId()).whenComplete((__, ex) -> {
                if (ex != null) {
                    p.sendMessage(MissetPlugin.ERROR);
                    return;
                }
                if (!p.isOnline()) {
                    return;
                }
                // Backpacks are now loaded, just get them from the global list
                if (manager.getBackpacksOf(player.getUniqueId()).isEmpty()) {
                    p.sendMessage(MissetPlugin.PREFIX + "§cDieser Spieler besitzt keine Backpacks!");
                    return;
                }
                INVSEE.put(p, player.getUniqueId());
                p.openInventory(manager.getPackSelectGui(player.getUniqueId(), true));
            });
        }
        return null;
    }
}
