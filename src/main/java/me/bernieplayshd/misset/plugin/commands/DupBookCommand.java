package me.bernieplayshd.misset.plugin.commands;

import me.bernieplayshd.misset.api.timing.DelayCommand;
import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.Utils;
import me.bernieplayshd.misset.plugin.manager.impl.internal.CommandManager;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import us.dev.api.command.handler.annotations.Executes;

public class DupBookCommand extends DelayCommand {

    public DupBookCommand(CommandManager manager) {
        super(manager, "dupbook", "system.presse", 2000L);
    }

    @Executes
    public String duplicateBook(Player player) {
        if (checkCooldown(player)) {
            return null;
        }
        ItemStack inHand = player.getInventory().getItemInHand();
        if (inHand == null || inHand.getType() != Material.WRITTEN_BOOK) {
           return MissetPlugin.PREFIX + "§cDu hälst kein beschriebenes Buch in der Hand!";
        }

        if (!Utils.canStore(inHand, player.getInventory())) {
            return MissetPlugin.PREFIX + "§cDein Inventar ist voll!";
        }

        // cloning not necessary
        player.getInventory().addItem(inHand);
        return MissetPlugin.PREFIX + "§7Du hast eine Kopie von deinem Buch in deiner Hand erhalten!";
    }
}
