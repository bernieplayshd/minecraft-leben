package me.bernieplayshd.misset.plugin.job.ability;

import me.bernieplayshd.misset.api.event.EventFunction;
import me.bernieplayshd.misset.plugin.job.ability.stage.JobAbilityStage;
import org.bukkit.event.Event;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public abstract class AbstractJobAbility implements JobAbility {

    private final JobAbilityStage[] stages;

    private final Set<EventFunction<?>> listeners;

    public AbstractJobAbility(JobAbilityStage ... stages) {
        this(new EventFunction[0], stages);
    }

    public AbstractJobAbility(EventFunction<?>[] listeners, JobAbilityStage ... stages) {
        this.listeners = new HashSet<>();
        this.stages = stages;
    }

    protected <T extends Event> void on(Class<T> clazz /* for easier readability */,
                                        EventFunction.Builder<T> builder)
    {
        listeners.add(builder.build());
    }

    @Override
    public JobAbilityStage[] getStages() {
        return stages;
    }

    @Override
    public Set<EventFunction<?>> getListeners() {
        return Collections.unmodifiableSet(listeners);
    }
}
