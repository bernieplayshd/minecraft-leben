package me.bernieplayshd.misset.plugin.job.ability.action.item;

import me.bernieplayshd.misset.api.event.EventFunction;
import me.bernieplayshd.misset.api.lang.RestrictionFilter;
import me.bernieplayshd.misset.plugin.job.ability.action.ActionJobAbility;
import me.bernieplayshd.misset.plugin.job.ability.stage.JobAbilityStage;
import me.bernieplayshd.misset.plugin.job.user.JobUser;
import org.bukkit.entity.Player;

import java.util.HashSet;
import java.util.Set;

public class ItemJobAction extends ActionJobAbility {

    public ItemJobAction(Set<RestrictionFilter<Player, JobUser>> filters, JobAbilityStage... stages) {
        super(filters, new EventFunction[0], stages);
    }

    public static class Builder {
        private Set<RestrictionFilter<Player, JobUser>> filters = new HashSet<>();
        private Set<JobAbilityStage> stages = new HashSet<>();
    }
}
