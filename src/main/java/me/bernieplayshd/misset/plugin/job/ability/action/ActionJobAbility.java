package me.bernieplayshd.misset.plugin.job.ability.action;

import com.google.common.collect.ImmutableSet;
import me.bernieplayshd.misset.api.event.EventFunction;
import me.bernieplayshd.misset.api.lang.RestrictionFilter;
import me.bernieplayshd.misset.plugin.job.ability.AbstractJobAbility;
import me.bernieplayshd.misset.plugin.job.ability.stage.JobAbilityStage;
import me.bernieplayshd.misset.plugin.job.user.JobUser;
import org.bukkit.entity.Player;

import java.util.Set;

public abstract class ActionJobAbility extends AbstractJobAbility {

    private final Set<RestrictionFilter<Player, JobUser>> filters;

    public ActionJobAbility(Set<RestrictionFilter<Player, JobUser>> filters, EventFunction<?>[] listeners,
                            JobAbilityStage... stages)
    {
        super(listeners, stages);
        this.filters = ImmutableSet.copyOf(filters);
    }

    public Set<RestrictionFilter<Player, JobUser>> getFilters() {
        return filters;
    }
}
