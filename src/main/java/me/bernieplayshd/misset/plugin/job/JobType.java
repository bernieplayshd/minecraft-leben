package me.bernieplayshd.misset.plugin.job;

public enum JobType {

    MINER("§8Miner"),
    BUILDER("§3Builder"),
    COOK("§2Bauer"),
    HUNTER("§cJäger"),
    WIZARD("§dBrauer");

    private final String display;

    private JobType(String display) {
        this.display = display;
    }

    public String getDisplay() {
        return display;
    }
}
