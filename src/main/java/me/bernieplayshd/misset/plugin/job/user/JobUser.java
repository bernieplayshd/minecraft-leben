package me.bernieplayshd.misset.plugin.job.user;

import me.bernieplayshd.misset.plugin.job.Job;
import org.bukkit.entity.Player;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

public class JobUser {

    private final Player player;

    private final Set<Job> jobs = new LinkedHashSet<>(2);

    public JobUser(Player player, Job ... jobs) {
        this.player = player;

        Collections.addAll(this.jobs, jobs);
    }

    public Player getPlayer() {
        return player;
    }

    public Set<Job> getJobs() {
        return Collections.unmodifiableSet(jobs);
    }
}
