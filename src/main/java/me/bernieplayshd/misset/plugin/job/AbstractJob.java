package me.bernieplayshd.misset.plugin.job;

public abstract class AbstractJob implements Job {

    private final JobType type;

    public AbstractJob(JobType type) {
        this.type = type;
    }

    @Override
    public JobType getType() {
        return type;
    }
}
