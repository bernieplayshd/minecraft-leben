package me.bernieplayshd.misset.plugin.job;

import me.bernieplayshd.misset.api.interfaces.Loadable;
import me.bernieplayshd.misset.plugin.job.ability.JobAbility;
import me.bernieplayshd.misset.plugin.job.user.JobUser;

import java.util.List;

public interface Job extends Loadable {

    JobType getType();

    JobAbility[] getAbilities();

    default String getDisplay() {
        return getType().getDisplay();
    }

    List<JobUser> getCurrentUsers();
}
