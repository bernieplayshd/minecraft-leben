package me.bernieplayshd.misset.plugin.job.ability.stage;

import me.bernieplayshd.misset.plugin.job.user.JobUser;

import java.util.Optional;
import java.util.function.Predicate;

public interface JobAbilityStage {

    Predicate<JobUser>[] getRequirements();

    Optional<JobAbilityStage> upgrade();

    boolean isUpgradeable();

    Optional<JobAbilityStage> getNext();

    JobAbilityStage[] getSiblings();
}
