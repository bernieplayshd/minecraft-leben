package me.bernieplayshd.misset.plugin.job.ability.action.delayed;

import me.bernieplayshd.misset.api.event.EventFunction;
import me.bernieplayshd.misset.api.lang.RestrictionFilter;
import me.bernieplayshd.misset.api.timing.Delay;
import me.bernieplayshd.misset.plugin.job.ability.action.ActionJobAbility;
import me.bernieplayshd.misset.plugin.job.ability.stage.JobAbilityStage;
import me.bernieplayshd.misset.plugin.job.user.JobUser;
import org.bukkit.entity.Player;

import java.util.Set;

public class DelayedJobAction<T> extends ActionJobAbility {

    private final Delay<T> delay;

    public DelayedJobAction(Set<RestrictionFilter<Player, JobUser>> filters, EventFunction<?>[] listeners,
                             Delay<T> delay, JobAbilityStage... stages)
    {
        super(filters, listeners, stages);
        this.delay = delay;
    }

    public Delay<T> getDelay() {
        return delay;
    }

    protected boolean checkCooldown(T entity) {
        return getDelay().checkCooldown(entity);
    }
}
