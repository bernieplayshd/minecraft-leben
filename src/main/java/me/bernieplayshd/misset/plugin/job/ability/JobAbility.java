package me.bernieplayshd.misset.plugin.job.ability;

import me.bernieplayshd.misset.api.event.EventFunction;
import me.bernieplayshd.misset.plugin.job.ability.stage.JobAbilityStage;

import java.util.Set;

public interface JobAbility {

    JobAbilityStage[] getStages();

    Set<EventFunction<?>> getListeners();
}
