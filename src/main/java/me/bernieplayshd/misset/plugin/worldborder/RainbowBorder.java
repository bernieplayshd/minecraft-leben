package me.bernieplayshd.misset.plugin.worldborder;

import org.bukkit.Bukkit;
import org.bukkit.WorldBorder;
import org.bukkit.craftbukkit.v1_14_R1.CraftWorld;
import org.bukkit.scheduler.BukkitTask;

import me.bernieplayshd.misset.plugin.MissetPlugin;

public class RainbowBorder {

	private final String world;
	private final double centerX, centerZ, size;
	
	private BukkitTask task;
	
	public RainbowBorder(String world, double centerX, double centerZ, double size) {
		this.world = world;
		this.centerX = centerX;
		this.centerZ = centerZ;
		this.size = size;
	}
	
	public void onEnable(long moveTime, long period) {
		if (task != null) {
			onDisable();
		}
		WorldBorder worldBorder = Bukkit.getWorld(world).getWorldBorder();
		worldBorder.setSize(size);
		worldBorder.setCenter(centerX, centerZ);
		
		this.task = Bukkit.getScheduler().runTaskTimer(MissetPlugin.getPlugin(), 
				new RainbowBorderRunnable(((CraftWorld) Bukkit.getWorld(world)).getHandle().getWorldBorder(), moveTime),
				0L, period);
	}

	public void onDisable() {
		if (task != null) {
			this.task.cancel();
			this.task = null;
		}
	}
	
	private static final class RainbowBorderRunnable implements Runnable {
		
		private final net.minecraft.server.v1_14_R1.WorldBorder worldBorder;
		private final double realSize;
		private final long moveTime;
		
		private int state = 0;
		
		public RainbowBorderRunnable(net.minecraft.server.v1_14_R1.WorldBorder worldBorder, long moveTime) {
			this.worldBorder = worldBorder;
			this.realSize = worldBorder.getSize();
			this.moveTime = moveTime;
		}
		
		@Override
		public void run() {
			if (state == 1) {
				worldBorder.setSize(realSize);
			} else {
				double size = state == 0 ? realSize - 0.1D : realSize + 0.1D;
				worldBorder.transitionSizeBetween(worldBorder.getSize(), size, moveTime);
			}
			if (state == 2) {
				this.state = 0;
				return;
			}
			state++;
		}
	}
}
