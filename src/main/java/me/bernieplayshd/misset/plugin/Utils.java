package me.bernieplayshd.misset.plugin;

import java.util.*;
import java.util.concurrent.Executor;
import java.util.function.Predicate;

import co.aikar.timings.Timing;
import co.aikar.timings.Timings;
import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;

public class Utils {

	public static final Random RANDOM = new Random();

	private Utils() { }

	/**
	 * Returns the amount of all items with the specified type combined
	 */
	public static int countItem(Inventory inv, Material type) {
		return Arrays.stream(inv.getContents())
				.filter(stack -> stack != null && stack.getType() == type)
				.mapToInt(ItemStack::getAmount)
				.sum();
	}

	/**
	 * Removes all items with the type for the amount.
	 *
	 * @return the amount that is left over or <code>-1</code> if the amount is negative
	 */
	public static int removeItems(Inventory inv, Material type, int amount) {
		return removeItems(inv, stack -> stack.getType() == type, amount);
	}

	/**
	 * Removes the item for the given amount.
	 *
	 * @return the amount that is left over or <code>-1</code> if the amount is negative
	 */
	public static int removeItems(Inventory inv, ItemStack item, int amount) {
		return removeItems(inv, stack -> stack.isSimilar(item), amount);
	}

	private static int removeItems(Inventory inv, Predicate<ItemStack> filter, int amount) {
		if (amount < 0) {
			return -1;
		}
		if (amount == 0) {
			return 0;
		}
		for (ItemStack stack : inv.getContents()) {
			if (stack != null && filter.test(stack)) {
				if (stack.getAmount() > amount) {
					stack.setAmount(stack.getAmount() - amount);
					return 0;
				} else if (stack.getAmount() < amount) {
					amount -= stack.getAmount();
					inv.remove(stack);
				} else if (stack.getAmount() == amount) {
					inv.remove(stack);
					return 0;
				}
			}
		}
		return amount;
	}

	/**
	 * Returns whether the inventory is full
	 */
	public static boolean isFull(Inventory inv) {
		// The amount of slots a player can store items in
		// ignoring the slots for the armor and second hand
		// Inventory#getSize does not return the actual amount of storage slots
		int size = inv.getType() == InventoryType.PLAYER ? 36 : inv.getStorageContents().length;
		return Arrays.stream(inv.getStorageContents()).filter(Objects::nonNull).count() >= size;
	}

	/**
	 * Returns whether the inventory can take the {@code stack}
	 */
	public static boolean canStore(ItemStack stack, Inventory inv) {
		if (!isFull(inv)) {
			return true;
		}
		Material type = stack.getType();
		int maxStack = Math.min(type.getMaxStackSize(), inv.getMaxStackSize());

		for (ItemStack item : inv.getStorageContents()) {
			if (item != null && item.getType() == type) {
				if (item.getAmount() + stack.getAmount() <= maxStack && item.isSimilar(stack)) {
					return true;
				}
			}
		}
		return false;
	}

	// https://bukkit.org/threads/get-entity-player-is-looking.300661/#post-2727103
	public static Optional<Player> getPlayerTarget(Player player) {
        Collection<? extends Player> players = Bukkit.getOnlinePlayers();
        Player target = null;
        final double threshold = 1;
        for (final Player other : players) {
            final Vector n = other.getLocation().toVector()
                    .subtract(player.getLocation().toVector());
            if (player.getLocation().getDirection().normalize().crossProduct(n)
                    .lengthSquared() < threshold
                    && n.normalize().dot(
                            player.getLocation().getDirection().normalize()) >= 0) {
                if ((target == null
                        || target.getLocation().distanceSquared(
                                player.getLocation()) > other.getLocation()
                                .distanceSquared(player.getLocation()))
                        && !player.equals(target))
                    target = other;
            }
        }
        return Optional.ofNullable(target);
    }

	/**
	 * Writes x-{@code times} a space ({@code \n}) to the player
	 *
	 * @param player The player which receives the spaces
	 * @param times How many times it should spaces
	 */
	public static void space(Player player, int times) {
		for (int x = 0; x < times; x++) {
			player.sendMessage("");
		}
	}

	/**
	 * Excludes the Minecraft format and color codes (for example <b>§a</b>)
	 *
	 * @param str The formatted input text
	 *
	 * @return The text without the formatting and coloring codes
	 */
	public static String getUnformatted(String str) {
		return str.replaceAll("§[a-fk-or0-9]", "");
	}

	private static final char[] NUMBERS = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

	/**
	 * Returns the last numeric sequence in a {@link String}
	 *
	 * @param string The string input
	 *
	 * @return last number or <code>-1</code> if not found
	 */
	public static int getLastNumber(String string) {
		/*
		 * made it reverse, no negative or decimal number
		 * support because not needed (and im too lazy)
		 */
		int startIdx = -1, stopIdx = -1;
		boolean inNum = false;
		for (int x = string.length() - 1; x != -1; x--) {
			if (isNumber(string.charAt(x))) {
				if (!inNum) {
					inNum = true;
					stopIdx = x;
				}
			} else if (inNum) {
				startIdx = x;
				break;
			}
		}
		return inNum ? Integer.parseInt(string.substring(startIdx == -1 ? 0 : startIdx + 1, stopIdx + 1)) : -1;
	}

	private static boolean isNumber(char c) {
        for (char num : NUMBERS) {
            if (c == num) {
                return true;
            }
        }
        return false;
    }

    public static BukkitTask runTaskLater(Runnable task, long delay) {
		MissetPlugin plugin = MissetPlugin.getPlugin();
		return plugin.getServer().getScheduler().runTaskLater(plugin, task, delay);
	}

	public static Location getHighest(Location from) {
		World world = from.getWorld();
		int highestY = world.getHighestBlockYAt(from);
		return new Location(world, from.getBlockX(),
				highestY++ >= world.getMaxHeight() ? world.getMaxHeight() : highestY,
				from.getBlockZ());
	}

	private static Object parseString(Class<?> clazz, String value) {
		if (Boolean.class == clazz) return Boolean.parseBoolean(value);
		if (Byte.class == clazz) return Byte.parseByte(value);
		if (Short.class == clazz) return Short.parseShort(value);
		if (Integer.class == clazz) return Integer.parseInt(value);
		if (Long.class == clazz) return Long.parseLong(value);
		if (Float.class == clazz) return Float.parseFloat(value);
		if (Double.class == clazz) return Double.parseDouble(value);
		return value;
	}

	@SuppressWarnings("unchecked")
	public static <T> T[] parse(Class<T> clazz, String[] strArray, int from, int to) {
		strArray = Arrays.copyOfRange(strArray, from, to);
		return (T[]) Arrays.stream(strArray).map(str -> (T) parseString(clazz, str)).toArray();
	}

	@SuppressWarnings("unchecked")
	public static <T> T parse(Class<T> clazz, String[] strArr, int index) {
		// return parse(clazz, strArr, index, index)[0];
		return (T) parseString(clazz, strArr[index]);
	}

	/**
	 * Returns all strings which are not blank or the last element of the array
	 */
	public static String[] notBlankOrLast(String ... strings) {
		final int lenght;
		if ((lenght = strings.length) == 0) {
			return new String[0];
		}
	    Set<String> temp = new LinkedHashSet<>(lenght);
        for (int x = 0; x < lenght - 1; x++) {
            String str = strings[x];
            if (StringUtils.isNotBlank(str)) {
                temp.add(str);
            }
        }
		temp.add(strings[lenght - 1]);
        return temp.toArray(new String[0]);
    }

	/**
	 * Returns an {@link Executor} which executes on the main thread
	 */
	public static Executor sync() {
		return runnable -> Bukkit.getScheduler().runTask(MissetPlugin.getPlugin(), runnable);
	}

	/**
	 * Starts a timing with the specified identifier
	 *
	 * <p>Usage example:
	 *
	 * <pre>{@code
	 * 	try (Timing timing = Utils.timing("Example Identifier")) {
	 * 	    ...
	 * 	}
	 * }</pre>
	 */
	public static Timing timing(String identifier) {
		return Timings.ofStart(MissetPlugin.getPlugin(), identifier);
	}

	public static int inventorySize(int slots) {
		return Math.min(54, Math.max(9, slots % 9 == 0 ? slots : (9 - (slots % 9)) + slots));
	}

	public static boolean isSpawnEgg(Material item) {
		// Not the optimal way, but i am too lazy
		// to type out every single spawn egg
		return item.name().endsWith("SPAWN_EGG");
	}
}