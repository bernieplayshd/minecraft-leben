package me.bernieplayshd.misset.plugin.spawner;

import java.util.Optional;
import java.util.UUID;

import javax.annotation.Nullable;

import org.bukkit.block.Block;

public class Spawner {

	private final int id;
	private SpawnerMob type;
	private final String owner;
	private final UUID ownerUUID;
	private final Block block;
	
	public Spawner(int id, @Nullable SpawnerMob type, String owner, UUID ownerUUID, Block block) {
		this.id = id;
		this.type = type;
		this.owner = owner;
		this.ownerUUID = ownerUUID;
		this.block = block;
	}

	public int getID() {
		return id;
	}

	public Optional<SpawnerMob> getType() {
		return Optional.ofNullable(type);
	}
	
	public void setType(SpawnerMob type) {
		this.type = type;
	}
	
	public String getOwner() {
		return owner;
	}
	
	public UUID getOwnerUUID() {
		return ownerUUID;
	}
	
	public Block getBlock() {
		return block;
	}

	public static class Builder {
		private int id;
		private SpawnerMob type;
		private String owner;
		private UUID ownerUUID;
		private Block block;

		public Builder id(int id) {
			this.id = id;
			return this;
		}

		public Builder of(SpawnerMob type) {
			this.type = type;
			return this;
		}

		public Builder from(UUID ownerUUID) {
			this.ownerUUID = ownerUUID;
			return this;
		}

		public Builder ownerName(String name) {
			this.owner = name;
			return this;
		}

		public Builder at(Block block) {
			this.block = block;
			return this;
		}

		public Spawner build() {
			return new Spawner(id, type, owner, ownerUUID, block);
		}
	}
}
