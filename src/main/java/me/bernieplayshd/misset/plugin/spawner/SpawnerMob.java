package me.bernieplayshd.misset.plugin.spawner;

import java.util.Arrays;
import java.util.Optional;

import org.bukkit.entity.EntityType;

public enum SpawnerMob {

	CHICKEN(EntityType.CHICKEN, "§rHuhn", "MHF_Chicken", 4, "§7§nHinweis§r§7: Braucht Gras um zu spawnen"),
	PIG(EntityType.PIG, "§cSchwein", "MHF_Pig", 4, "§7§nHinweis§r§7: Braucht Gras um zu spawnen"),
	SHEEP(EntityType.SHEEP, "§rSchaf", "MHF_Sheep", 8, "§7§nHinweis§r§7: Braucht Gras um zu spawnen"),
	COW(EntityType.COW, "§cKuh", "MHF_Cow", 6, "§7§nHinweis§r§7: Braucht Gras um zu spawnen"),
	MUSHROOM_COW(EntityType.MUSHROOM_COW, "§cPilzkuh", "MHF_MushroomCow", 20, "§7§nHinweis§r§7: Braucht Myzel um zu spawnen"),
	RABBIT(EntityType.RABBIT, "§6Hase", "MHF_Rabbit", 7, "§7§nHinweis§r§7: Braucht Gras um zu spawnen"),
	// VILLAGER(EntityType.VILLAGER, "Dorfbewohner", "§6", "MHF_Villager", 32, "§7§nHinweis§r§7: Braucht Gras um zu spawnen"),
	ZOMBIE_PIGMAN(EntityType.PIG_ZOMBIE, "§cZombie Pigman", "MHF_PigZombie", 16, ""),
	LLAMA(EntityType.LLAMA, "§eLama", "EntityLlamaSpit", 12, "§7§nHinweis§r§7: Braucht Gras um zu spawnen"),
	TRADER_LLAMA(EntityType.TRADER_LLAMA, "§cHändler Lama", "EntityLlamaSpit", 14, "§7§nHinweis§r§7: Braucht Gras um zu spawnen"),
	POLAR_BEAR(EntityType.POLAR_BEAR, "§9Eisbär", "Polar_bear", 24, "§7§nHinweis§r§7: Braucht Gras um zu spawnen"),
	// TODO: check spawning conditions and add them in the description
	PARROT(EntityType.PARROT, "§4Papagei", "MHF_Parrot", 28, ""),
	TURTLE(EntityType.TURTLE, "§2Schildkröte", "MHF_Turtle", 32, ""),
	// TODO: check spawning conditions and add them in the description
	PANDA(EntityType.PANDA, "§0Panda", "1stPanda", 64, ""),
	FOX(EntityType.FOX, "§6Fuchs", "MHF_Fox", 15, ""),
	// Hostile
	SKELETON(EntityType.SKELETON, "§7Skelett", "MHF_Skeleton",
			8, ""/*"§7§nHinweis§r§7: Du kannst nur einen Spawner mit einem bösartigen Mob haben", true*/),
	ZOMBIE(EntityType.ZOMBIE, "§2Zombie", "MHF_Zombie",
			4, ""/*"§7§nHinweis§r§7: Du kannst nur einen Spawner mit einem bösartigen Mob haben", true*/),
	SPIDER(EntityType.SPIDER, "§8Spinne", "MHF_Spider",
			8, ""/*"§7§nHinweis§r§7: Du kannst nur einen Spawner mit einem bösartigen Mob haben", true*/),
	CREEPER(EntityType.CREEPER, "§2Creeper", "MHF_Creeper",
			20, ""/*"§7§nHinweis§r§7: Du kannst nur einen Spawner mit einem bösartigen Mob haben", true*/);

	// VINDICATOR(EntityType.VINDICATOR, "§8Diener", "Vindicator", 32, "§7§nHinweis§r§7: Für genauere Infos /warp vindicator");
	
	// Muss in einem geschlossen Raum(am Besten 4x3),von dem die Wände aus normalem Stein bestehen,
	// mit 2 Türen nebeneinander am Eingang und 6 Cobbelstone über dem Spawner an der Decke platziert werden"
			
	private final EntityType type;
	private final String prefix, head, description;
	private final int price;
	// private final boolean hostile;

	/*private SpawnerMob(EntityType type, String prefix, String head, int price, String desc) {
		this(type, prefix, head, price, desc, false);
	}*/
	
	private SpawnerMob(EntityType type, String prefix, String head, int price, String desc/*, boolean hostile*/) {
		this.type = type;
		this.prefix = prefix;
		this.head = head;
		this.price = price;
		this.description = desc;
		//this.hostile = hostile;
	}

	public EntityType getType() {
		return type;
	}
	
	public static Optional<SpawnerMob> of(EntityType type) {
		return Arrays.stream(values()).filter(mob -> mob.getType() == type).findAny();
	}

	public String getPrefix() {
		return prefix;
	}

	public String getHead() {
		return head;
	}

	public String getDescription() {
		return description;
	}

	public int getPrice() {
		return price;
	}

	/*public boolean isHostile() {
		return hostile;
	}*/
}
