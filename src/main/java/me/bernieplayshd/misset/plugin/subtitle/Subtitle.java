package me.bernieplayshd.misset.plugin.subtitle;

import org.bukkit.entity.Player;

public class Subtitle {

    private final Player player;
    private String title;
    private boolean hidden;

    public Subtitle(Player player, String title) {
        this(player, title, false);
    }

    public Subtitle(Player player, String title, boolean hidden) {
        this.player = player;
        this.title  = title.replace('§', '&');
        this.hidden = hidden;
    }

    public Player getPlayer() {
        return player;
    }

    public String getTitle() {
        return title;
    }

    /**
     * Remember to also update the title (manually) via its manager
     *
     * @see me.bernieplayshd.misset.plugin.manager.impl.SubtitleManager#updateTitle(Subtitle)
     */
    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isHidden() {
        return hidden;
    }

    public void setHidden(boolean hidden) {
        this.hidden = hidden;
    }

    public void toggleHidden() {
        this.hidden = !hidden;
    }
}
