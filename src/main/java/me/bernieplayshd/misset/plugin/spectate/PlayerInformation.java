package me.bernieplayshd.misset.plugin.spectate;

import java.util.Collection;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

public class PlayerInformation {
	
	public final UUID uuid;
	
	private final ItemStack[] contents, armor, extra;
	
	private final GameMode gameMode;
	
	private final Location location;
	private final float exp, flySpeed;
	private final double health;
	private final int food, fireTicks;
	
	private final boolean allowFly, flying;
	
	private final Collection<PotionEffect> activeEffects;

	/**
	 * Saves state informations about the player with the uuid
	 */
	public PlayerInformation(UUID uuid) {
		Player player = Bukkit.getPlayer(uuid);
		
		this.uuid = uuid;
		
		this.gameMode = player.getGameMode();
		
		this.contents = player.getInventory().getContents();
		this.armor = player.getInventory().getArmorContents();
		this.extra = player.getInventory().getExtraContents();
		
		this.location = player.getLocation();
		
		this.exp = player.getExp();
		this.flySpeed = player.getFlySpeed();
		this.health = player.getHealth();
		this.food = player.getFoodLevel();
		this.fireTicks = player.getFireTicks();
		
		this.allowFly = player.getAllowFlight();
		this.flying = player.isFlying();
		
		this.activeEffects = player.getActivePotionEffects();
	}

	/**
	 * Applies the states in the information on the player
	 */
	public void apply() {
		Player player = Bukkit.getPlayer(uuid);
		player.setGameMode(gameMode);
		
		player.getInventory().setContents(contents);
		player.getInventory().setExtraContents(extra);
		player.getInventory().setArmorContents(armor);
		
		player.setHealth(health);
		player.setExp(exp);
		player.setFlySpeed(flySpeed);
		player.setFoodLevel(food);
		player.setFireTicks(fireTicks);
		
		player.setAllowFlight(allowFly);
		player.setFlying(flying);
		
		player.getActivePotionEffects().forEach(effect -> player.removePotionEffect(effect.getType()));
		player.addPotionEffects(activeEffects);
		
		for (Player p : Bukkit.getOnlinePlayers()) {
			/*if (GlowAPI.isGlowing(p, player)) {
				Bukkit.getScheduler().runTaskLaterAsynchronously(MissetPlugin.getPlugin(),
						() -> GlowAPI.setGlowing(p, false, player), 10L);
			}*/
			if (player != p) {
				p.showPlayer(player);
			}
		}
		
		player.teleport(location);
	}
}