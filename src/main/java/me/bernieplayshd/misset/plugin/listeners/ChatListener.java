package me.bernieplayshd.misset.plugin.listeners;

import me.bernieplayshd.misset.api.timing.Delay;
import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.manager.impl.BackpackManager;
import me.lucko.luckperms.LuckPerms;
import me.lucko.luckperms.api.Contexts;
import me.lucko.luckperms.api.User;
import me.lucko.luckperms.api.caching.MetaData;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.maxgamer.maxbans.MaxBans;

import java.util.Collection;

public class ChatListener implements Listener {

    private static final int CHAT_RANGE = 40;

    private final Delay<Player> delay = new Delay.Builder<Player>().of(5000L).build();

    @EventHandler
    public void onChat(AsyncPlayerChatEvent event) {
        Player player = event.getPlayer();
        String msg = event.getMessage();

        if (BackpackManager.RENAMING.containsKey(player.getUniqueId())) {
            return;
        }

        if (MaxBans.instance.getBanManager().getMute(player.getName()) != null) {
            event.setCancelled(true);
            return;
        }

        if ('#' == msg.charAt(0) || (msg.length() > 1 && "//".equals(msg.substring(0, 2)))) {
            event.setCancelled(true);
        } else {
            event.setCancelled(true);
            String message = getFormatted(player, msg, false);
            Collection<Player> inRange = inRange(player.getLocation(), CHAT_RANGE);
            inRange.forEach(p -> p.sendMessage(message));
            if (inRange.size() < 1) {
                player.sendMessage(MissetPlugin.PREFIX + "§7Deine Nachricht hat niemand außer §7dir erhalten, da " +
                        "sich niemand außer dir in §7Reichweite von " + CHAT_RANGE + " Blöcken befindet!");
                player.sendMessage(MissetPlugin.PREFIX + "§7Benutze doch den globalen Chat mit einem §e# " +
                        "§7vor deiner Nachricht");
            } else if (!delay.checkCooldown(player)) {
                int size = inRange.size() - 1;
                player.sendMessage(MissetPlugin.PREFIX + "§6" + size + " §7Spieler, " + (size == 1 ? "der" : "die")
                    + " in Reichweite von " + CHAT_RANGE + " Blöcken "
                    + (size == 1 ? "ist hat" : "sind haben") + " deine Nachricht erhalten");
            }
        }
    }

    private String getFormatted(Player player, String message, boolean global) {
        User user = LuckPerms.getApi().getUser(player.getUniqueId());
        MetaData metaData = user.getCachedData().getMetaData(Contexts.global());
        String prefix, suffix;
        return (global ? "§3§oGLOBAL §r" : "")
                + ChatColor.translateAlternateColorCodes('&',
                    (prefix = metaData.getPrefix()) == null ? "" : prefix)
                + player.getName() + " §8» §r"
                + ChatColor.translateAlternateColorCodes('&',
                    (suffix = metaData.getSuffix()) == null ? "" : suffix) + message;
    }

    private Collection<Player> inRange(Location loc, int range) {
        return loc.getWorld().getNearbyPlayers(loc, range, range, range);
    }
}
