package me.bernieplayshd.misset.plugin.listeners;

import java.util.*;
import java.util.logging.Level;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.block.CreatureSpawner;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.Utils;
import me.bernieplayshd.misset.plugin.manager.impl.SpawnerManager;
import me.bernieplayshd.misset.plugin.spawner.SpawnerMob;

// TODO: clean up old code
public class SpawnerListener implements Listener {

	private final Map<Player, Block> claiming = new HashMap<>(),
									 buying   = new HashMap<>();
	
	private final SpawnerManager manager;

	public SpawnerListener(SpawnerManager manager) {
		this.manager = manager;
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		Player player   = event.getPlayer();
		Block block     = event.getClickedBlock();
		ItemStack stack = event.getItem();
		
		if (event.getAction() == Action.RIGHT_CLICK_BLOCK && block != null
				&& block.getType() == Material.SPAWNER)
		{
			if (stack != null && (Utils.isSpawnEgg(stack.getType()) || stack.getType() == Material.LEGACY_MONSTER_EGGS)) {
				event.setCancelled(true);
				player.sendMessage(MissetPlugin.PREFIX + "§cDu darfst den Mob des Spawners nicht per SpawnEgg verändern!");
				return;
			}
			
			//fix: event called twice for each hand
			if (event.getHand() != null && event.getHand() == EquipmentSlot.OFF_HAND) {
				return;
			}

			event.setCancelled(true);
			
			if (manager.isClaimable(block)) {
				if (claiming.containsValue(block)) {
					player.sendMessage(MissetPlugin.PREFIX + "§cDieser Spawner ist bereits besetzt!");
				} else {
					claiming.put(player, block);
					player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 2.0F, 1.5F);
					player.sendTitle("§aDu hast einen freien Spawner entdeckt!", null);
					player.openInventory(manager.getBuyInv());
				}
			} else if (!manager.isClaimable(block) && !manager.isOwner(player.getUniqueId(), block)) {
				try {
					manager.getName(block).ifPresent(name -> player.sendMessage(MissetPlugin.PREFIX +
							"§cDieser Spawner gehört nicht dir sondern §4" + name + "§c!"));
				} catch (Exception e) {
					manager.getLogger().log(Level.SEVERE, "Failed to lookup player name for spawner", e);
					player.sendMessage(MissetPlugin.ERROR);
				}
			} else if (manager.isOwner(player.getUniqueId(), block) && !buying.containsKey(player)) {
				buying.put(player, block);
				player.openInventory(manager.getSpawnerInv(player));
			}
		}
	}
	 
	@EventHandler
	public void onInvClick(InventoryClickEvent e) {
		Player p       = (Player) e.getWhoClicked();
		ItemStack item = e.getCurrentItem();
		String   title = e.getView().getTitle();

		if (item == null)
			return;
		if (title.equals(SpawnerManager.BUY_TITLE)) {
			e.setCancelled(true);
			if (item.getType() == Material.EMERALD_BLOCK) {
				int diamonds = Utils.countItem(p.getInventory(), Material.DIAMOND);
				if (diamonds >= SpawnerManager.SPAWNER_PRICE) {
					Block b = claiming.get(p);
					Utils.removeItems(p.getInventory(), Material.DIAMOND, SpawnerManager.SPAWNER_PRICE);
					manager.claim(p, b).whenComplete((state, ex) -> {
						if (ex != null || !state) {
							manager.getLogger().log(Level.SEVERE, "Failed to claim spawner");
							p.sendMessage(MissetPlugin.ERROR);
						} else {
							p.sendMessage(MissetPlugin.PREFIX + "§aDu hast einen Spawner gekauft!");
							p.sendMessage(String.format("%s§7Koordinaten: X: §e%d §7Y: §e%d §7Z: §e%d",
									MissetPlugin.PREFIX, b.getX(), b.getY(), b.getZ()));
							claiming.remove(p);
							p.closeInventory();
						}
					});
				} else {
					int required = SpawnerManager.SPAWNER_PRICE - diamonds;
					p.sendMessage(MissetPlugin.PREFIX + "§cDir fehlen noch §b" + required
							+ " Diamanten §cdamit du dir dies kaufen kannst!");
					claiming.remove(p);
					p.closeInventory();
				}
			} else if (item.getType() == Material.REDSTONE_BLOCK) {
				if (claiming.containsKey(p)) {
					p.sendMessage(MissetPlugin.PREFIX + "§cDu hast den Kauf abgebrochen!");
					claiming.remove(p);
					p.closeInventory();
				}
			}
		}
		else if (title.equals(SpawnerManager.MENU_TITLE) && buying.containsKey(p)) {
			e.setCancelled(true);

			if(item.getType() == Material.SPAWNER) {
				int diamonds = Utils.countItem(p.getInventory(), Material.DIAMOND);
				if (diamonds >= SpawnerManager.GIVE_PRICE) {
					if (!Utils.canStore(new ItemStack(Material.SPAWNER), p.getInventory())) {
						buying.remove(p);
						p.closeInventory();
						p.sendMessage(MissetPlugin.PREFIX + "§cDu hast kein Platz in deinem Inventar um die Diamanten zu erhalten!");
						p.sendMessage(MissetPlugin.PREFIX + "§7Mache in deinem Inventar Platz, damit du den Spawner erhalten kannst");
						return;
					}
					Block b = buying.get(p);
					manager.getSpawnerOf(b).ifPresent(spawner -> manager.remove(spawner.getID()));
					b.setType(Material.AIR);
					p.getInventory().addItem(new ItemStack(Material.SPAWNER));
					Utils.removeItems(p.getInventory(), Material.DIAMOND, SpawnerManager.GIVE_PRICE);
					p.sendMessage(MissetPlugin.PREFIX + "§aDu hast deinen Spawner erhalten!");
					buying.remove(p);
					p.closeInventory();
				} else {
					int required = SpawnerManager.GIVE_PRICE - diamonds;
					p.sendMessage(MissetPlugin.PREFIX + "§cDir fehlen noch §b" + required +
							" Diamanten §cdamit du dir dies kaufen kannst!");
					buying.remove(p);
					p.closeInventory();
					return;
				}
			}
			for (SpawnerMob mob : SpawnerMob.values()) {
				if (item.hasItemMeta() && mob.getPrefix().equals(item.getItemMeta().getDisplayName())) {
					int diamonds = Utils.countItem(p.getInventory(), Material.DIAMOND);
					if (diamonds >= mob.getPrice()) {
						Utils.removeItems(p.getInventory(), Material.DIAMOND, mob.getPrice());
						Block b1 = buying.get(p);

						CreatureSpawner to = (CreatureSpawner) b1.getWorld().getBlockAt(b1.getLocation()).getState();
						to.setSpawnedType(mob.getType());
						to.update(true);

						manager.getSpawnerOf(b1).ifPresent(spawner -> spawner.setType(mob));
						buying.remove(p);
						p.playSound(p.getLocation(), Sound.ENTITY_PLAYER_LEVELUP, 2.0F, 1.5F);
						p.sendMessage(MissetPlugin.PREFIX + "§aDu hast das Spawnmob in " + mob.getPrefix() + " §ageändert!");
						p.closeInventory();
					} else {
						int required = mob.getPrice() - diamonds;
						p.sendMessage(MissetPlugin.PREFIX + "§cDir fehlen noch §b" + required
								+ " Diamanten §cdamit du dir dies kaufen kannst!");
						buying.remove(p);
						p.closeInventory();
					}
				}
			}
		}
	}

	@EventHandler
	public void onExplosion(BlockExplodeEvent e) {
		if (e.getBlock().getType() == Material.SPAWNER) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onPiston(BlockPistonExtendEvent e) {
		if (e.getBlock().getType() == Material.SPAWNER) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onPiston2(BlockPistonRetractEvent e) {
		if (e.getBlock().getType() == Material.SPAWNER) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onEntityExplosion(EntityExplodeEvent e) {
		e.blockList().removeIf(block -> block.getType() == Material.SPAWNER);
	}

	@EventHandler
	public void onChangeBlock(EntityChangeBlockEvent e) {
		if (e.getBlock().getType() == Material.SPAWNER) {
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onInvClose(InventoryCloseEvent e) {
		Player p = (Player) e.getPlayer();
		String title = e.getView().getTitle();
		if (title.equals(SpawnerManager.BUY_TITLE) && claiming.containsKey(p)) {
			claiming.remove(p);
			p.sendMessage(MissetPlugin.PREFIX +
					"§cDu hast den Kauf abgebrochen, indem du das Kauf-Inventar geschlossen hast!");
		}
		else if (title.equals(SpawnerManager.MENU_TITLE) && buying.containsKey(p)) {
			buying.remove(p);
			p.sendMessage(MissetPlugin.PREFIX + "§7Du hast das Kaufmenü verlassen!");
		}
	}

	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		Player p = e.getPlayer();
		Block b = e.getBlock();
		if (b.getType() != Material.SPAWNER) {
			return;
		}
		if (claiming.containsValue(b)) {
			e.setCancelled(true);
			getPlayerFromBlock(b).ifPresent(fromBlock -> p.sendMessage(MissetPlugin.PREFIX
					+ "§cDu kannst diesen Spawner nicht abbauen, da §4"
					+ fromBlock.getName() + " §cgerade den Spawner kaufen möchte!"));
			return;
		}
		if (!manager.isClaimable(b)) {
			if (!manager.isOwner(p.getUniqueId(), b)) {
				e.setCancelled(true);
				try {
					Optional<String> optional = manager.getName(b);
					if (!optional.isPresent()) {
						return;
					}
					p.sendMessage(MissetPlugin.PREFIX + "§cDu kannst §4" + optional.get() + "'s §cSpawner nicht zerstören!");
				} catch (Exception e1) {
					manager.getLogger().log(Level.SEVERE, "Failed to lookup name of spawner @ " + b, e1);
					p.sendMessage(MissetPlugin.ERROR);
				}
			} else {
				if (!Utils.canStore(new ItemStack(Material.DIAMOND, SpawnerManager.SPAWNER_PRICE), p.getInventory())) {
					e.setCancelled(true);
					p.sendMessage(MissetPlugin.PREFIX + "§cDu hast kein Platz in deinem Inventar um die Diamanten zu erhalten!");
					p.sendMessage(MissetPlugin.PREFIX + "§7Machen in deinem Inventar Platz, damit du die §e"
							+ SpawnerManager.SPAWNER_PRICE + " §7Diamanten erhalten kannst");
					return;
				}
				manager.getSpawnerOf(b).ifPresent(spawner -> manager.remove(spawner.getID()));

				p.getInventory().addItem(new ItemStack(Material.DIAMOND, SpawnerManager.SPAWNER_PRICE));
				p.sendMessage(MissetPlugin.PREFIX + "§7Du hast deinen eigenen Spawner zerstört!");
				p.sendMessage(MissetPlugin.PREFIX + "§7Du hast §b" + SpawnerManager.SPAWNER_PRICE
						+ " Diamanten §7erhalten, da du deinen Spawner abgebaut hast!");
			}
		}
	}

	private Optional<Player> getPlayerFromBlock(Block block) {
		return claiming.entrySet().stream()
				.filter(entry -> entry.getValue().equals(block))
				.findAny()
				.map(Map.Entry::getKey);
	}

	@EventHandler
	public void onDisconnect(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		claiming.remove(p);
		buying.remove(p);
	}
	
	@EventHandler
	public void onConnect(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		manager.getName(player.getUniqueId()).whenComplete((opt, ex) -> {
			if (ex != null) {
				manager.getLogger().log(Level.SEVERE, "Failed to lookup name", ex);
			} else if (opt.isPresent() && !player.getName().equals(opt.get())) {
				manager.setName(player.getUniqueId(), player.getName());
			}
		});
	}
	
	public void onDisable() {
		claiming.keySet().forEach(Player::closeInventory);
		claiming.clear();

		buying.keySet().forEach(Player::closeInventory);
		buying.clear();
	}
}
