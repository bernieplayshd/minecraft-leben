package me.bernieplayshd.misset.plugin.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.event.server.TabCompleteEvent;

// TODO: lookup
public class TabListener implements Listener {

	@EventHandler
	public void onTabCompleteOld(PlayerChatTabCompleteEvent event) {
		if (event.getPlayer().hasPermission("system.admin")) {
			return;
		}
		// spigot permission system for commands is mostly not used, so just remove all command completions
		event.getTabCompletions().removeIf(comp -> comp.charAt(0) == '/');
	}

	@EventHandler
	public void onTabComplete(TabCompleteEvent event) {
		if (event.getSender().hasPermission("system.admin")) {
			return;
		}
		// spigot permission system for commands is mostly not used, so just remove all command completions
		event.getCompletions().removeIf(comp -> comp.charAt(0) == '/');
	}
}
