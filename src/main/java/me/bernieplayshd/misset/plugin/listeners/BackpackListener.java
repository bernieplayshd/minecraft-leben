package me.bernieplayshd.misset.plugin.listeners;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.Map.Entry;
import java.util.logging.Level;

import me.bernieplayshd.misset.plugin.commands.BackpackCommand;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryMoveItemEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.Utils;
import me.bernieplayshd.misset.plugin.backpack.Backpack;
import me.bernieplayshd.misset.plugin.backpack.BackpackType;
import me.bernieplayshd.misset.plugin.manager.impl.BackpackManager;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class BackpackListener implements Listener {

	private final BackpackManager manager;
	
	public BackpackListener(BackpackManager manager) {
		this.manager = manager;
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		if (BackpackManager.RENAMING.containsKey(player.getUniqueId())) {
			event.setCancelled(true);
			Backpack backpack = BackpackManager.RENAMING.get(player.getUniqueId());
			
			String name = event.getMessage();
			if (name.length() > 16) {
				player.sendMessage(MissetPlugin.PREFIX + "§cDer Name darf höchstens §e16 §cZeichen lang sein!");
				return;
			}
			
			BackpackManager.RENAMING.remove(player.getUniqueId());

			manager.updateName(backpack, event.getMessage()).whenComplete((__, ex) -> {
				if (ex != null) {
					manager.getLogger().log(Level.SEVERE, "Failed to rename backpack", ex);
					player.sendMessage(MissetPlugin.ERROR);
				} else {
					player.sendMessage(MissetPlugin.PREFIX + "§7Du hast dein Backpack erfolgreich in '§r"
							+ name.replace('&', '§') + "§r§7' umbenannt!");
				}
			});
		}
	}
	
	@EventHandler
	public void onConnect(PlayerJoinEvent event) {
		UUID uuid = event.getPlayer().getUniqueId();
		if (!manager.contains(uuid)) {
			Utils.runTaskLater(() -> manager.load(uuid), 5L);
		}
	}
	
	@EventHandler
	public void onDisconnect(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		if (BackpackCommand.INVSEE.containsKey(player)) {
			if (BackpackCommand.INVSEE.entrySet().stream().noneMatch(entry -> entry.getValue().equals(BackpackCommand.INVSEE.get(player))
						&& !entry.getKey().getUniqueId().equals(player.getUniqueId()) && Bukkit.getPlayer(entry.getValue()) == null))
			{
				manager.unload(BackpackCommand.INVSEE.get(player));
				BackpackCommand.INVSEE.remove(player);
			}
		}
		// removed if present
		BackpackManager.RENAMING.remove(player.getUniqueId());
		manager.unload(player.getUniqueId());
	}
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		if (e.getItem() != null && e.getHand() != null && e.getHand() == EquipmentSlot.HAND) {
			
			Player p = e.getPlayer();
			
			if (e.getMaterial() == Material.PAPER && e.getItem().hasItemMeta() && !e.getItem().getItemMeta().getDisplayName().isEmpty()) {
				Optional<BackpackType> optional = BackpackType.of(e.getItem().getItemMeta().
						getDisplayName().split("\\(")[1].replace("§7)", ""));
				if (!optional.isPresent()) {
					return;
				}
				BackpackType type = optional.get();

				e.setCancelled(true);

				if (type == BackpackType.LARGE && !p.hasPermission("system.adel")) {
					p.sendMessage(MissetPlugin.PREFIX + "§cDieses Backpack steht nur dem §eAdel §cRang zur Verfügung!");
					return;
				}

				int backpacks = manager.getBackpacksOf(p.getUniqueId()).size();

				if (backpacks >= 8) {
					p.sendMessage(MissetPlugin.PREFIX + "§cDu kannst nicht mehr als 9 Backpacks besitzen!");
					return;
				}

				Utils.removeItems(p.getInventory(), e.getItem(), 1);

				manager.create(p.getUniqueId(), type).whenComplete((id, ex) -> {
					if (ex != null) {
						manager.getLogger().log(Level.SEVERE, "Failed to create backpack", ex);
						p.sendMessage(MissetPlugin.ERROR);
						return;
					}
					Backpack pack = new Backpack(id, type, p.getUniqueId(), Bukkit.createInventory(null, type.getSize(),
							String.format("Backpack-%d §7[%s§7]", id, type.getDisplay())), null);

					manager.getManaged().add(pack);

					p.sendTitle("", "§aZertifikat eingelöst!");
					p.sendMessage(MissetPlugin.PREFIX + "§aDu hast ein Zertifikat für ein Backpack ("
							+ pack.getType().getDisplay() + "§a) eingelöst!");
					p.sendMessage(MissetPlugin.PREFIX + "§7Deine Backpacks kannst du mit §e/backpack menu §7verwalten");
				});
			} 
		}
	}

	@EventHandler
	public void onItemMove(InventoryMoveItemEvent event) {
		getTitle(event)
			.filter(title -> title.equals(BackpackManager.SELECT_TITLE) || title.startsWith("§8Backpacks von"))
			.ifPresent(__ -> event.setCancelled(true));
	}

	/*
	 * A little hack to get the InventoryView's title
	 * because the view isn't included in the event itself
	 */
	private Optional<String> getTitle(InventoryMoveItemEvent event) {
		List<HumanEntity> viewers = event.getSource().getViewers();
		if (viewers.isEmpty()) {
			return Optional.empty();
		}
		return Optional.of(viewers.get(0).getOpenInventory().getTitle());
	}
	
	@EventHandler
	public void onInvClick(InventoryClickEvent e) {
		Inventory inv = e.getClickedInventory();
		String title = e.getView().getTitle();
		ItemStack item = e.getCurrentItem();
		Player p = (Player) e.getWhoClicked();
		
		if (inv != null && item != null && item.getType() != Material.AIR) {
			
			if ((title.equals(BackpackManager.SELECT_TITLE) || title.startsWith("§8Backpacks von"))
					&& item.hasItemMeta() && item.getItemMeta().getDisplayName().contains("§7Backpack-")) {
				e.setCancelled(true);
				
				int id = Utils.getLastNumber(Utils.getUnformatted(item.getItemMeta().getDisplayName())/*.split("\\[")[0]*/);
				boolean invsee = BackpackCommand.INVSEE.containsKey(p);
				
				Optional<Backpack> optional = manager.getBackpackOf(manager.getBackpacksOf(
						invsee ? BackpackCommand.INVSEE.get(p) : p.getUniqueId()), id);
				if (!optional.isPresent()) {
					return;
				}
				Backpack backpack = optional.get();
				
				if (e.getClick() == ClickType.RIGHT || e.getClick() == ClickType.SHIFT_RIGHT) {
					BackpackManager.RENAMING.put(p.getUniqueId(), backpack);
					p.closeInventory();
					Utils.space(p, 5);
					p.sendMessage(MissetPlugin.PREFIX + "§7Schreibe in den Chat, wie du §7dein Backpack nennen möchtest");
					p.sendMessage(MissetPlugin.PREFIX + "§7Du kannnst Farbcodes benutzen, da '§e&§7' als Paragraph-Zeichen ersetzt wird");
					return;
				}
				
				if (e.getClick() == ClickType.DROP || e.getClick() == ClickType.CONTROL_DROP) {
					p.closeInventory();
					p.sendMessage(MissetPlugin.PREFIX + "§7Du bist gerade dabei eines deiner Backpacks zu löschen!");
					
					TextComponent text = new TextComponent();
					text.setText(MissetPlugin.PREFIX + "§7Sind sie sicher, dass sie fortfahren wollen? ");
					
					TextComponent yes = new TextComponent();
					yes.setText("§aJa");
					yes.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/backpack delete " + backpack.getID()));
					yes.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponent[] {
							new TextComponent("§7Linksklick um Vorgang fortzusetzten") }));
					
					TextComponent extra = new TextComponent(" §7| ");
					
					TextComponent no = new TextComponent();
					no.setText("§cNein");
					no.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/backpack menu"));
					no.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponent[] {
							new TextComponent("§7Linksklick um Vorgang abzubrechen") }));
					
					extra.addExtra(no);
					yes.addExtra(extra);
					text.addExtra(yes);
					
					p.spigot().sendMessage(text);
					return;
				}
				
				Inventory inventory = backpack.getInventory();
				if (inventory == null) {
					p.closeInventory();
					p.sendMessage(MissetPlugin.PREFIX + "§cDu besitzt dieses Backpack nicht!");
					return;
				}
				
				if (invsee) {
					inv.setMaxStackSize(4);
				}
				p.openInventory(inventory);

			} else if (title.startsWith("§8Backpack-") && BackpackCommand.INVSEE.containsKey(p)
					&& !p.hasPermission("leben.admin")) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onInvClose(InventoryCloseEvent e) {
		Inventory inv = e.getInventory();
		Player p = (Player) e.getPlayer();
		String title = e.getView().getTitle();
		
		if (inv != null && title.startsWith("§8Backpack-")) {
			if (BackpackCommand.INVSEE.containsKey(p)) {
				
				if (BackpackCommand.INVSEE.entrySet().stream()
						.anyMatch(entry -> entry.getValue().equals(BackpackCommand.INVSEE.get(p))
						&& !entry.getKey().getUniqueId().equals(p.getUniqueId())))
				{
					return;
				}

				Utils.runTaskLater(() -> p.openInventory(manager.getPackSelectGui(
						BackpackCommand.INVSEE.get(p), true)), 1L
				);
			}
		} else if (inv != null && title.startsWith("§8Backpacks von")
				&& BackpackCommand.INVSEE.containsKey(p) && inv.getMaxStackSize() != 4)
		{
			boolean online = false;
			for (Entry<Player, UUID> entry : BackpackCommand.INVSEE.entrySet()) {
				if (entry.getValue().equals(BackpackCommand.INVSEE.get(p))
					&& !entry.getKey().getUniqueId().equals(p.getUniqueId())) {
					return;
				} else {
					online = Bukkit.getOfflinePlayer(BackpackCommand.INVSEE.get(p)).isOnline();
				}
			}
		
			if (!online) {
				manager.unload(BackpackCommand.INVSEE.get(p));
			}
			
			BackpackCommand.INVSEE.remove(p);
		}
	}
}
