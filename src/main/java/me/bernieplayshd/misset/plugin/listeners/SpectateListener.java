package me.bernieplayshd.misset.plugin.listeners;

import java.util.Date;
import java.util.Optional;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.EquipmentSlot;
import org.bukkit.inventory.ItemStack;
import org.maxgamer.maxbans.MaxBans;

import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.Utils;
import me.bernieplayshd.misset.plugin.manager.impl.SpectateManager;
import me.bernieplayshd.misset.plugin.report.ReportTemplate;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class SpectateListener implements Listener {
	
	private final SpectateManager manager;
	
	public SpectateListener(SpectateManager manager) {
		this.manager = manager;
	}

	@EventHandler
	public void onDisconnect(PlayerQuitEvent event) {
		if (manager.isSpectator(event.getPlayer())) {
			manager.normalizePlayer(event.getPlayer(), true);
		} else {
			manager.getManaged().stream()
				.map(info -> Bukkit.getPlayer(info.uuid))
				.forEach(player -> event.getPlayer().showPlayer(player));
			//manager.getESPSpectators().forEach(spec -> Bukkit.getScheduler()
			//		.runTask(MissetPlugin.getPlugin(), () -> GlowAPI.setGlowing(event.getPlayer(), false, spec)));
		}
	}

	@EventHandler
	public void onDamage(EntityDamageEvent event) {
		if (event.getEntityType() == EntityType.PLAYER && manager.isSpectator((Player) event.getEntity())) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onDamageByEntity(EntityDamageByEntityEvent event) {
		if (!(event.getDamager() instanceof Player)) {
			return;
		}
		Player damager = (Player) event.getDamager();
		if (manager.isSpectator(damager)) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onInteract(PlayerInteractEvent event) {
		Player player = event.getPlayer();
		if (manager.isSpectator(player)) {
			event.setCancelled(true);
			
			if (event.hasItem()) {
				ItemStack item = event.getItem();
				Material type = item.getType();

				if (type == Material.GOLDEN_AXE) {
				    event.setCancelled(false);
				    return;
                }
				
				// fix: event called twice for each hand
				if (event.getHand() != null && event.getHand() == EquipmentSlot.OFF_HAND) {
					return;
				}
				
				if (type == Material.ENDER_PEARL && item.hasItemMeta()) {
					String display = item.getItemMeta().getDisplayName();
					if (display == null)
						return;
					boolean random = "§bTp zu Random Spieler".equals(display);
					if (random) {
						if (Bukkit.getOnlinePlayers().size() == 1) {
							return;
						}
						int rand = Utils.RANDOM.nextInt(Bukkit.getOnlinePlayers().size() - 1);
						Player target = (Player) Bukkit.getOnlinePlayers().toArray()[rand];
						player.teleport(target);
					} else {
						Utils.space(player, 5);
						for (Player online : Bukkit.getOnlinePlayers()) {
							TextComponent text = new TextComponent(),
									 clickable = new TextComponent();
							text.setText("§7- §r" + online.getDisplayName() + " ");
							
							clickable.setText("§7[§9TP§7]");
							clickable.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/spec " + online.getName()));
							clickable.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new TextComponent[] {
									new TextComponent("§7Linksklick um zu teleportieren") }));
							
							text.addExtra(clickable);
							player.spigot().sendMessage(text);
						}
					}
				/*} else if (type == Material.REDSTONE_TORCH_ON && item.hasItemMeta()) {
					String display = item.getItemMeta().getDisplayName();
					if (display == null)
						return;
					boolean glowing = "§6ESP §7(§cAus§7)".equals(display);
					
					//List<Player> online = Lists.newArrayList(Bukkit.getOnlinePlayers());
					online.remove(player);
					
					// Delay
					//online.forEach(on -> Bukkit.getScheduler().runTaskLaterAsynchronously(MissetPlugin.getPlugin(),
					//		() -> GlowAPI.setGlowing(on, glowing, player), 10L));
					
					ItemMeta meta = item.getItemMeta();
					meta.setDisplayName(glowing ? "§6ESP §7(§aAn§7)" : "§6ESP §7(§cAus§7)");
					item.setItemMeta(meta);
				*/} else if (type == Material.CHEST) {
					Optional<Player> target = Utils.getPlayerTarget(player);
					if (!target.isPresent()) {
						player.sendMessage(MissetPlugin.PREFIX + "§cDu schaust gerade keinen Spieler an!");
						return;
					}
					MissetPlugin.getPlugin().getServer().dispatchCommand(player, "invsee " + target.get().getName());
				} else if (type == Material.REDSTONE_BLOCK) {
					Optional<Player> target = Utils.getPlayerTarget(player);
					if (!target.isPresent()) {
						player.sendMessage(MissetPlugin.PREFIX + "§cDu schaust gerade keinen Spieler an!");
						return;
					}
					player.openInventory(manager.getBanGui(target.get().getName()));
				} else if (type == Material.RED_WOOL) {
					manager.normalizePlayer(player, true);
					player.sendMessage(MissetPlugin.PREFIX + "§7Du hast den §eSpectator-Mode §7verlassen");
				}
			}
		}
	}
	
	@EventHandler
	public void onFoodLevel(FoodLevelChangeEvent event) {
		if (event.getEntity() instanceof Player && manager.isSpectator((Player) event.getEntity())) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onPickUp(PlayerPickupItemEvent event) {
		if (manager.isSpectator(event.getPlayer())) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onConnect(PlayerJoinEvent event) {
		manager.getManaged().stream()
			.map(info -> Bukkit.getPlayer(info.uuid))
			.forEach(player -> event.getPlayer().hidePlayer(player));
		//manager.getESPSpectators().forEach(spec -> Bukkit.getScheduler()
		//		.runTaskLaterAsynchronously(MissetPlugin.getPlugin(), () -> GlowAPI.setGlowing(event.getPlayer(),
		//				true, spec), 10L));
	}
	
	@EventHandler
	public void onInvClick(InventoryClickEvent event) {
		if (!(event.getWhoClicked() instanceof Player)) {
			return;
		}
		Player player = (Player) event.getWhoClicked();

		if (manager.isSpectator(player)) {
			String title = event.getView().getTitle();
			if (title.startsWith(SpectateManager.BAN_TITLE)) {
				
				ItemStack clicked = event.getCurrentItem();
				
				if (clicked != null && clicked.getType() != Material.AIR) {
					String reported = title.split(SpectateManager.BAN_TITLE)[1];
					Optional<ReportTemplate> optional = ReportTemplate.of(clicked.getType());
					if (!optional.isPresent()) {
						player.closeInventory();
						return;
					}
					ReportTemplate template = optional.get();
					
					MaxBans.instance.getBanManager().warn(reported, template.getDisplay(), player.getName()); // warn
					if (template.getTime() > 0L) {
						MaxBans.instance.getBanManager().tempban(reported, template.getDisplay(), player.getName(),
								new Date(new Date().getTime() + template.getTime()).getTime()); // ban
					}
					player.closeInventory();
					player.sendMessage(MissetPlugin.PREFIX + "§7Du hast den Spieler §e" + reported + " §7bestraft!");
				}
			}
		}
	}
	
	@EventHandler
	public void onBlockPlace(BlockPlaceEvent event) {
		if (manager.isSpectator(event.getPlayer())) {
			event.setCancelled(true);
		}
	}
	
	@EventHandler
	public void onBlockBreak(BlockBreakEvent event) {
		if (manager.isSpectator(event.getPlayer())) {
			event.setCancelled(true);
		}
	}

	@EventHandler
	public void onItemDrop(PlayerDropItemEvent event) {
		if (manager.isSpectator(event.getPlayer())) {
			event.setCancelled(true);
		}
	}
}
