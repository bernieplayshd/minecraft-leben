package me.bernieplayshd.misset.plugin.listeners;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import com.google.common.primitives.Primitives;
import me.bernieplayshd.misset.api.module.AbstractModule;
import me.bernieplayshd.misset.plugin.commands.ModuleCommand;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import me.bernieplayshd.misset.api.config.ConfigItem;
import me.bernieplayshd.misset.api.item.ItemBuilder;
import me.bernieplayshd.misset.api.manager.AbstractManager;
import me.bernieplayshd.misset.api.module.ToggleableModule;
import me.bernieplayshd.misset.plugin.MissetPlugin;

public class ModuleListener implements Listener {
	
	// TODO: note "final" for setting
	
	private final MissetPlugin plugin;
	
	private final Map<Player, ConfigItem> changing = new HashMap<>();

	public ModuleListener(MissetPlugin plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onInvClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		Inventory inv = event.getClickedInventory();
		String title = event.getView().getTitle();
		ItemStack clicked = event.getCurrentItem();
		
		if (inv != null && clicked != null && clicked.getType() != Material.AIR) {
			if (title.equals(ModuleCommand.MODULES_TITLE)) {
				event.setCancelled(true);
				player.openInventory(getModuleGui(getModule(clicked.getItemMeta().getDisplayName().substring(2))));
			} else if (title.startsWith("§9")) {
				String splitted = title.split("§9")[1];
				Optional<AbstractManager<?>> optionalManager = plugin.getRegisterManager().findManager(splitted);
				if (optionalManager.isPresent() || splitted.equals("Register") || splitted.equals("Config")) {
					event.setCancelled(true);
					
					AbstractModule module = getModule(splitted);
					boolean enabled;
					if ((enabled = clicked.getType() == Material.EMERALD_BLOCK) || clicked.getType() == Material.REDSTONE_BLOCK
							&& module instanceof ToggleableModule) {
						player.sendMessage(String.format("%s§7%s '§9%s§7'...", MissetPlugin.PREFIX,
								enabled ? "Deaktiviere" : "Aktiviere", module.getName()));
						plugin.getServer().getScheduler().runTaskAsynchronously(plugin, () -> {
							((ToggleableModule) module).setRunning(!enabled);
							clicked.setType(enabled ? Material.REDSTONE_BLOCK : Material.EMERALD_BLOCK);
							player.sendMessage(String.format("%s§7Erfolgreich '§9%s§7' %s", MissetPlugin.PREFIX,
									module.getName(), enabled ? "deaktiviert" : "aktiviert"));
						});
					} else {
						player.openInventory(getConfig(module, plugin.getConfigManager().getManaged().get(module)));
					}
				} 
			} else if (title.startsWith("§8Config: §9")) {
				String splitted = title.split("§8Config: §9")[1],
					   splittedConf = clicked.getItemMeta().getDisplayName().split("§9")[1].split("§8")[0];
				
				AbstractModule module = getModule(splitted);
				Collection<ConfigItem> items = plugin.getConfigManager().getManaged().get(module);
				Optional<ConfigItem> optional = items.stream().filter(item -> item.getName().equals(splittedConf)).findFirst();
				
				if (optional.isPresent()) {
					try {
						ConfigItem item = optional.get();
						Object value = item.getValue();
						
						if (value instanceof Number || value instanceof String) {
							changing.put(player, item);
							
							player.closeInventory();
							player.sendMessage(MissetPlugin.PREFIX + "§7Schreibe in den Chat den Wert, den du dem ConfigItem zuteilen möchtest");
							player.sendMessage(MissetPlugin.PREFIX + "§7Hierbeit wird '§e&§7' zu '§e§§7' umgeändert");
						} else {
							player.closeInventory();
							player.sendMessage(MissetPlugin.PREFIX + "§7Den Typ '§9" + value.getClass().getSimpleName()
									+ "§7' kann man nicht im Chat bearbeiten!");
						}
												
					} catch (Exception e) {
						e.printStackTrace();
						player.sendMessage(MissetPlugin.ERROR);
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onDisconnect(PlayerQuitEvent event) {
		changing.remove(event.getPlayer());
	}
	
	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		if (changing.containsKey(player)) {
			try {
				ConfigItem item = changing.get(player);
				Object value = item.getValue();
				String message = event.getMessage();
				
				if (value instanceof String) {
					item.setValue(message.replace('&', '§'));
				} else {
					try {
						Number newValue = 0;
						Class<?> type = Primitives.wrap(value.getClass());
						if (type == Integer.class) {
							newValue = Integer.parseInt(message);
						} else if (type == Double.class) {
							newValue = Double.parseDouble(message);
						} else if (type == Float.class) {
							newValue = Float.parseFloat(message);
						} else if (type == Long.class) {
							newValue = Long.parseLong(message);
						} else if (type == Short.class) {
							newValue = Short.parseShort(message);
						} else if (type == Byte.class) {
							newValue = Byte.parseByte(message);
						}
						item.setValue(newValue);	
					} catch (NumberFormatException ex) {
						player.sendMessage(String.format("%s§cDas, was du geschrieben hast, kann nicht zum Typ" +
								" '§e%s§c' konvertiert werden!", MissetPlugin.PREFIX, value.getClass().getSimpleName()));
					}
				}
				changing.remove(player);
				player.sendMessage(String.format("%s§7Du hast den Wert erfolgreich in §e%s §7umgeändert!",
						MissetPlugin.PREFIX, message));
			} catch (Exception e) {
				e.printStackTrace();
				player.sendMessage(MissetPlugin.ERROR);
			}
		}
	}
	
	@EventHandler
	public void onInvClose(InventoryCloseEvent event) {
		String title = event.getView().getTitle();
		if (title.startsWith("§8Config: §9")) {
			String splitted = title.split("§8Config: §9")[1];
			AbstractModule module = getModule(splitted);
			
			plugin.getServer().getScheduler().runTaskLater(plugin, () -> event.getPlayer()
					.openInventory(getModuleGui(module)), 1L);
		}
	}

	private AbstractModule getModule(String name) {
		AbstractModule module;
		switch (name) {
			case "Register":
				module = plugin.getRegisterManager();
				break;
			case "Config":
				module = plugin.getConfigManager();
				break;
			default: {
				module = plugin.getRegisterManager().findManager(name).orElse(plugin.getRegisterManager());
			}
		}
		return module;
	}
	
	private Inventory getModuleGui(AbstractModule module) {
		Inventory inv = Bukkit.createInventory(null, 9, "§9" + module.getName());
		if (module instanceof ToggleableModule) {
			boolean enabled = ((ToggleableModule) module).isRunning();
			inv.addItem(ItemBuilder.build(enabled ? Material.EMERALD_BLOCK : Material.REDSTONE_BLOCK,
					enabled ? "§aDeaktivieren" : "§aAktivieren",
					"§8[Linksklick] §7- Schaltet das Module an/aus"), new ItemStack(Material.AIR));
		}
		if (plugin.getConfigManager().getManaged().containsKey(module)) {
			inv.addItem(ItemBuilder.build(Material.LEGACY_COMMAND, "§cConfig",
					"§8[Linksklick] §7- Zum bearbeiten der Values des Modules",
					"§8[Rechtsklick] §7- Speichert die Änderungen ab"));
		}
		return inv;
	}
	
	private Inventory getConfig(AbstractModule module, Collection<ConfigItem> items) {
		Inventory inv = Bukkit.createInventory(null, items.size() % 9 == 0 ? items.size()
						: items.size() % 9 + (9 - items.size()),
				"§8Config: §9" + module.getName());
		for (ConfigItem item : items) {
			try {
				inv.addItem(ItemBuilder.build(Material.PAPER, String.format("§9%s §7(§b%s§7) = §c%s",
						item.getName(), item.getField().getType().getSimpleName(), item.getValue().toString()),
						"§8[Linksklick] - §7Wert des Config Items bearbeiten"));
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return inv;
	}
	
	public void onDisable() {
		changing.clear();
	}
}
