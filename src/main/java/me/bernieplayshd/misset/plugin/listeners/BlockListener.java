package me.bernieplayshd.misset.plugin.listeners;

import org.bukkit.Material;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;

public class BlockListener implements Listener {
    
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent event) {
        if (event.getItemInHand().getType() == Material.BEDROCK
                && event.getPlayer().hasPermission("system.admin"))
        {
            event.setCancelled(true);
        }
    }
}
