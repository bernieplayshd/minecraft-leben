package me.bernieplayshd.misset.plugin.listeners;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;

import com.google.common.io.Files;

import me.bernieplayshd.misset.plugin.MissetPlugin;
import me.bernieplayshd.misset.plugin.manager.impl.ReportManager;
import me.bernieplayshd.misset.plugin.manager.impl.SpectateManager;
import me.bernieplayshd.misset.plugin.report.Report;
import me.bernieplayshd.misset.plugin.report.ReportTemplate;

public class ReportListener implements Listener {
	
	private final ReportManager manager;
	
	public ReportListener(ReportManager manager) {
		this.manager = manager;
	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		String title = event.getView().getTitle();
		ItemStack clicked = event.getCurrentItem();
		
		if (clicked != null && clicked.getType() != Material.AIR) {
			if (title.startsWith(ReportManager.GUI_TITLE)) {
				event.setCancelled(true);
				String reported = title.split(ReportManager.GUI_TITLE)[1];
				Optional<ReportTemplate> optional = ReportTemplate.of(clicked.getType());
				if (!optional.isPresent()) {
					return;
				}
				ReportTemplate template = optional.get();
				
				OfflinePlayer offline = Bukkit.getOfflinePlayer(reported);
				if (offline == null) {
					player.closeInventory();
					player.sendMessage(MissetPlugin.PREFIX + "§cDieser Spieler ist dem Server nicht bekannt!");
					return;
				}
				
				if (manager.getReportsOf(offline.getUniqueId()).stream().
						anyMatch(report -> report.getSender() == player.getUniqueId()
								&& report.getTemplate() == template))
				{
					player.closeInventory();
					player.sendMessage(MissetPlugin.PREFIX + "§cDu hast diesen Spieler bereits mit diesem Grund reportet");
					return;
				}

				manager.create(offline.getUniqueId(), player.getUniqueId(), template).whenComplete((report, ex) -> {
						if (ex != null) {
							manager.getLogger().log(Level.SEVERE, "Failed to create report", ex);
							player.sendMessage(MissetPlugin.ERROR);
						} else {
							manager.notifyTeam(report);
						}
				});
				player.closeInventory();
				player.sendMessage(MissetPlugin.PREFIX + "§7Du hast den Spieler §e" + offline.getName()
						+ " §7erfolgreich gemeldet");
				if (offline.isOnline()) {
					offline.getPlayer().sendMessage(MissetPlugin.PREFIX + "§7Du wurdest von dem Spieler §e"
							+ player.getName() + " §7wegen §r" + template.getDisplay() + " §7gemeldet!");
				}
			} else if (title.startsWith(ReportManager.EDIT_TITLE)) {
				if (!player.hasPermission("system.support")) {
					return;
				}
				event.setCancelled(true);
				int id = Integer.parseInt(title.split("\\[§e")[1].replace("§8]", ""));
				Optional<Report> optional = manager.getReportOf(id);
				if (!optional.isPresent()) {
					return;
				}
				
				Report report = optional.get();
				Material type = clicked.getType();
				OfflinePlayer offline = Bukkit.getOfflinePlayer(report.getReported());
				Player reported = Bukkit.getPlayer(report.getReported());

				switch (type) {
                    case ENDER_PEARL:
                        if (reported == null)
                            return;
                        if (clicked.getEnchantments().size() == 1) {
                            MissetPlugin.getPlugin().getManager(SpectateManager.class).setSpectatorMode(player);
                        }
                        player.teleport(reported.getLocation());
                        break;
                    case PAPER:
                        player.closeInventory();
                        player.sendMessage(MissetPlugin.PREFIX + "§7Die letzten 50 Nachrichten/Commands von §e"
                                + offline.getName());
                        // TODO: async & fix
                        // getLastMessages(offline.getName(), 50).forEach(msg -> player.sendMessage(MissetPlugin.PREFIX + "§e"
                        //		 + offline.getName() + " §7-> " + msg));
                        break;
                    case CHEST:
                        if (reported == null)
                            return;
                        player.closeInventory();
                        Bukkit.dispatchCommand(player, "invsee " + reported.getName());
                        break;
                    case REDSTONE_BLOCK:
						manager.remove(report, true);
                        player.closeInventory();
                        player.sendMessage(MissetPlugin.PREFIX + "§7Du hast den Report von §e" + offline.getName()
                                + " §7abgeschlossen");
                        break;
                    case EMERALD_BLOCK:
						manager.accept(report);
                        for (Player p : Bukkit.getOnlinePlayers()) {
                            if (p != player && player.hasPermission("system.support")) {
                                p.sendMessage(MissetPlugin.PREFIX + "§7Der Spieler §e" + player.getName()
                                        + " §7hat den Report von §e"
                                        + Bukkit.getOfflinePlayer(report.getSender()).getName() + " §7angenommen und §c"
                                        + offline.getName() + " §7bestraft");
                            }
                        }
                        player.closeInventory();
                        player.sendMessage(MissetPlugin.PREFIX + "§aDu hast den Report angenommen");
                        break;
                }
			}
		}
	}
	
	@EventHandler
	public void onConnect(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		manager.getFinished(player.getUniqueId()).whenComplete((finished, ex) -> {
			if (ex != null) {
				manager.getLogger().log(Level.SEVERE, "Failed to load finished reports", ex);
				player.sendMessage(MissetPlugin.ERROR);
			} else {
				for (Report report : finished) {
					OfflinePlayer offline = Bukkit.getOfflinePlayer(report.getReported());
					if (offline == null) {
						return;
					}
					manager.rewardPlayer(player, report);
					manager.remove(report, false);
				}
			}
		});
		
        if (player.hasPermission("system.support") && manager.getManaged().size() > 0) {
        	 player.sendMessage(MissetPlugin.PREFIX + "§7Es sind §e" + manager.getManaged().size() + " §7Reports offen");
             player.sendMessage(MissetPlugin.PREFIX + "§7Benutze /report list um die Reports zu verwalten");
        }
	}
	
	private static final File LATEST_LOG;
	
	static {
		String path = MissetPlugin.getPlugin().getDataFolder().getAbsolutePath(),
			   logs = path.replace("/plugins", "/logs/latest.log");
		LATEST_LOG = new File(logs);
	}
	
	// TODO: fix with regex pattern & tasks to update
	private List<String> getLastMessages(String name, int amount) {
		try {
			Files.readLines(LATEST_LOG, StandardCharsets.UTF_8).stream()
				.filter(line -> {
					if (line.contains("[Async Chat Thread") && line.contains(name)) {
						
					}
					return false;
				});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return new ArrayList<>();
	}
}
