package me.bernieplayshd.misset.plugin;

import me.bernieplayshd.misset.api.manager.AbstractManager;
import me.bernieplayshd.misset.api.database.sql.impl.MySQLDatabase;
import me.bernieplayshd.misset.plugin.listeners.*;
import me.bernieplayshd.misset.plugin.manager.impl.internal.CommandManager;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

import me.bernieplayshd.misset.plugin.manager.impl.internal.ConfigManager;
import me.bernieplayshd.misset.plugin.manager.impl.internal.RegisterManager;

public class MissetPlugin extends JavaPlugin {
	
	// TODO: finish module ui
	// TODO: tutorial
	// TODO: add Jobs
	// TODO: add Bot-NPCs, for Jobs like Miner, etc. (sub. money every work hour ...)
	// TODO: middle-click ring own entries

	// (Deprecated since we use gradle for building)
	//
	// Make sure to compile with the '-parameters' argument for the command system
	// Intellij: Settings -> Build, Execution, Deployment -> Compiler -> Java Compiler -> Javac Options
	// Eclipse:  Preferences -> Java -> Compiler -> Store information about method parameters

	public static final String PREFIX = "§eMisset §8|§r ",
							   NO_PERMISSIONS = PREFIX + "§cDafür hast du keine Rechte!",
							   DELAY = PREFIX + "§cDu kannst diesen Befehl erst wieder in §e%s §cSekunden benutzen!",
							   ERROR = PREFIX + "§cEin Fehler ist aufgetreten!";
	
	private static MissetPlugin instance;
	
	public MissetPlugin() {
		instance = this;
	}
	
	//
	//

	private final MySQLDatabase database = MySQLDatabase.LOCALHOST;
	
	/* Core Managers */
	private RegisterManager registerManager;
	private CommandManager commandManager;
	private ConfigManager configManager;
	/* ------------- */

	private ModuleListener moduleListener;
	
	@Override
	public void onLoad() {
		this.commandManager = new CommandManager(this);
		this.registerManager = new RegisterManager();
		this.configManager = new ConfigManager();
	}
	
	@Override
	public void onEnable() {
		database.connect();
		
		configManager.onEnable();
		registerManager.onEnable();
		commandManager.onEnable();
	
		PluginManager pManager = getServer().getPluginManager();
		pManager.registerEvents(new TabListener(), this);
		// pManager.registerEvents(new EatListener(), this);
		pManager.registerEvents(this.moduleListener = new ModuleListener(this), this);
		// pManager.registerEvents(new ChatListener(), this);
		pManager.registerEvents(new BlockListener(), this);
	}
	
	@Override
	public void onDisable() {
		registerManager.onDisable();
		commandManager.onDisable();
		configManager.onDisable();
		
		moduleListener.onDisable();

		database.disconnect();
	}
	
	public static MissetPlugin getPlugin() {
		return instance;
	}
	
	public MySQLDatabase getDatabase() {
		return database;
	}
	
	public RegisterManager getRegisterManager() {
		return registerManager;
	}
	
	public ConfigManager getConfigManager() {
		return configManager;
	}

	public CommandManager getCommandManager() {
		return commandManager;
	}

	@SuppressWarnings("unchecked")
	public <T extends AbstractManager<?>> T getManager(Class<T> clazz) {
		T manager = getRegisterManager().getManager(clazz);
		if (manager == null) {
			if (clazz == RegisterManager.class) {
				return (T) getRegisterManager();
			} else if (clazz == ConfigManager.class) {
				return (T) getConfigManager();
			} else if (clazz == CommandManager.class) {
				return (T) getCommandManager();
			}
		}
		return manager;
	}
}
