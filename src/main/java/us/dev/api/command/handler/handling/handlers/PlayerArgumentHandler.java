package us.dev.api.command.handler.handling.handlers;

import me.bernieplayshd.misset.plugin.manager.impl.internal.CommandManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import us.dev.api.command.handler.exceptions.ArgumentParseException;
import us.dev.api.command.handler.handling.ArgumentHandler;
import us.dev.api.command.handler.handling.parameter.CommandParameter;

public class PlayerArgumentHandler implements ArgumentHandler<Player> {

    @Override
    public Player parse(CommandManager commandManager, CommandParameter parameter, String input) throws ArgumentParseException {
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (player.getName().equalsIgnoreCase(input)) {
                return player;
            }
        }
        throw new ArgumentParseException("'§e" + input + "§7' ist nicht auf dem Server");
    }

    @Override
    public Class[] getHandledTypes() {
        return new Class[] { Player.class };
    }

    @Override
    public String getSyntax(CommandParameter parameter) {
        return parameter.getLabel() + " - Spieler";
    }
}
