package us.dev.api.command.handler;

import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import us.dev.api.command.Command;
import us.dev.api.command.handler.exceptions.UnsupportedParameterException;
import us.dev.api.command.handler.handling.parameter.CommandParameter;
import us.dev.api.command.handler.annotations.Executes;
import us.dev.api.command.handler.annotations.Flag;

import me.bernieplayshd.misset.plugin.manager.impl.internal.CommandManager;

import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Foundry
 */
public final class CommandExecutor {
    private final Command source;
    private final Method method;
    private final CommandParameter[] parameters;
    private final Executes handler;
    private final CommandManager commandManager;
    private final String permission;

    private boolean isPlayer;

    public CommandExecutor(Command source, Method method, Executes handler, CommandManager commandManager) throws UnsupportedParameterException {
        this.source = source;
        this.method = method;
        this.handler = handler;
        this.commandManager = commandManager;
        this.permission = handler.permission().isEmpty() ? null : handler.permission();

        List<CommandParameter> params = new ArrayList<>();
        int index = 0;
        for (Parameter parameter : method.getParameters()) {
            Class<?> type = parameter.getType();
            if (type == Optional.class) {
                Type generic = method.getGenericParameterTypes()[index];
                if (generic instanceof ParameterizedType) {
                    type = (Class<?>) ((ParameterizedType) generic).getActualTypeArguments()[0];
                } else {
                    throw new UnsupportedParameterException("failed to resolve argument type");
                }
            }
            index++;
            // ignore command sender parameter on first
            if (index == 1 && ((isPlayer = (type == Player.class)) || type == CommandSender.class)) {
                continue;
            }
            if (commandManager.findArgumentHandler(type) == null) {
                throw new UnsupportedParameterException(type);
            }
            params.add(new DeclaredCommandParameter(parameter, type));
        }
        this.parameters = params.toArray(new DeclaredCommandParameter[params.size()]);

        /*int[] index = new int[1];
        this.parameters = Arrays.stream(method.getParameters()).map(parameter -> {
            Class<?> type = parameter.getType();
            if (type == Optional.class) {
                Type generic = method.getGenericParameterTypes()[index[0]];
                if (generic instanceof ParameterizedType) {
                    type = (Class<?>) ((ParameterizedType) generic).getActualTypeArguments()[0];
                } else {
                    throw new UnsupportedParameterException("failed to resolve argument type");
                }
            }
            // ignore command sender parameter
            if (type == CommandSender.class) {
                return null;
            }
            index[0]++;
            if (commandManager.findArgumentHandler(type) == null) {
                throw new UnsupportedParameterException(type);
            }
            return new DeclaredCommandParameter(parameter, type);
        }
        ).toArray(DeclaredCommandParameter[]::new);*/
    }

    public Object execute(Object ... arguments) {
        try {
            this.method.setAccessible(true);
            Object output = this.method.invoke(this.source, arguments);
            if (output instanceof String[]) {
                output = String.join(System.lineSeparator(), (String[]) output);
            }
            return output;
        }
        catch (IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
            return "An error occurred while executing the command.";
        }
    }

    public String getSyntax() {
        return String.join(" ", getSyntaxTree());
    }

    public List<String> getSyntaxTree() {
        final List<String> syntaxBuilder = new ArrayList<>(this.handler.value().length + this.parameters.length);
        //for (String sub : this.handler.value()) {
        if (this.handler.value().length != 0) {
            syntaxBuilder.add(this.handler.value()[0].split("\\|")[0]); //no aliases
        }
        //}
        for (CommandParameter parameter : this.parameters) {
            String syntax = this.commandManager.findArgumentHandler(parameter.getType())
                    .orElseThrow(() -> new UnsupportedParameterException("unsupported parameter type found while creating syntax tree"))
                    .getSyntax(parameter);
            Flag flag = parameter.getAnnotation(Flag.class);
            if (flag != null) {
                syntaxBuilder.add(String.format("(-[%s]%s)", String.join("|", flag.value()), parameter.isBoolean() ? "" : " [" + syntax + "]"));
                continue;
            }
            syntax = parameter.isOptional() ? String.format("<%s>", syntax) : String.format("[%s]", syntax);
            syntaxBuilder.add(syntax);
        }
        return syntaxBuilder;
    }

    public CommandParameter[] getParameters() {
        return this.parameters;
    }

    public Executes getHandler() {
        return this.handler;
    }

    public Optional<String> getPermission() {
        return Optional.ofNullable(permission);
    }

    public boolean isSenderPlayer() {
        return isPlayer;
    }

    private static final class DeclaredCommandParameter implements CommandParameter {
        private final Parameter parameter;
        private final Class type;

        DeclaredCommandParameter(Parameter parameter, Class type) {
            this.parameter = parameter;
            this.type = type;
        }

        @Override
        public <A extends Annotation> A getAnnotation(Class<A> annotationClass) {
            return this.parameter.getAnnotation(annotationClass);
        }

        @Override
        public Annotation[] getAnnotations() {
            return this.parameter.getAnnotations();
        }

        @Override
        public Class<?> getType() {
            return this.type;
        }

        @Override
        public boolean isOptional() {
            return this.parameter.getType() == Optional.class;
        }

        @Override
        public boolean isBoolean() {
            return this.parameter.getType() == Boolean.class && this.parameter.getType() == Boolean.TYPE;
        }

        @Override
        public String getLabel() {
            return parameter.getName();
        }
    }

}

