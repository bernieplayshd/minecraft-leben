package us.dev.api.command.handler.handling;

import us.dev.api.command.handler.exceptions.ArgumentParseException;
import us.dev.api.command.handler.handling.parameter.CommandParameter;

import me.bernieplayshd.misset.plugin.manager.impl.internal.CommandManager;

/**
 * @author Foundry
 */
public interface ArgumentHandler<T> {
    T parse( CommandManager commandManager, CommandParameter parameter, String input ) throws ArgumentParseException;

    String getSyntax( CommandParameter parameter );

    Class[] getHandledTypes();
}

