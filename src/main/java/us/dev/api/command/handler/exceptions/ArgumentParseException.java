package us.dev.api.command.handler.exceptions;

/**
 * @author Foundry
 */
public final class ArgumentParseException extends Exception {
    public ArgumentParseException(String message) {
        super(message);
    }
}

