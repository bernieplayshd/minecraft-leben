package us.dev.api.command.handler.handling.handlers;

import us.dev.api.command.handler.exceptions.ArgumentParseException;
import us.dev.api.command.handler.handling.ArgumentHandler;
import us.dev.api.command.handler.handling.parameter.CommandParameter;

import me.bernieplayshd.misset.plugin.manager.impl.internal.CommandManager;

import java.util.Arrays;

/**
 * @author Foundry
 */
public final class EnumArgumentHandler<T extends Enum> implements ArgumentHandler<T> {

    @SuppressWarnings("unchecked")
    @Override
    public T parse(CommandManager commandManager, CommandParameter parameter, String input) throws ArgumentParseException {
        Enum[] enumConstants = (Enum[]) parameter.getType().getEnumConstants();
        for (Enum e : enumConstants) {
            if (e.toString().equalsIgnoreCase(input) || e.name().equalsIgnoreCase(input)) return (T) e;
        }
        throw new ArgumentParseException("'§e" + input + "§7' ist kein Wert von §7"
                /*+ parameter.getType().getSimpleName() + " §7"*/ + Arrays.toString(parameter.getType().getEnumConstants()));
    }

    @Override
    public String getSyntax(CommandParameter parameter) {
        Object[] enums = parameter.getType().getEnumConstants();
        return parameter.getLabel() + " - Ein Wert von " + (enums.length < 10 ? Arrays.toString(enums)
                : parameter.getType().getSimpleName() + "(" + enums[0] + ", " + enums[1] + ", ...)");
    }

    @Override
    public Class[] getHandledTypes() {
        return new Class[] { Enum.class };
    }
}

