package us.dev.api.command;

import me.bernieplayshd.misset.plugin.MissetPlugin;
import net.md_5.bungee.api.chat.BaseComponent;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import us.dev.api.command.handler.exceptions.ArgumentParseException;
import us.dev.api.command.handler.exceptions.UnsupportedParameterException;
import us.dev.api.interfaces.Aliased;
import us.dev.api.interfaces.Labeled;
import us.dev.api.command.handler.CommandExecutor;
import us.dev.api.command.handler.handling.parameter.CommandParameter;
import us.dev.api.command.handler.annotations.Executes;
import us.dev.api.command.handler.annotations.Flag;
import us.dev.api.command.handler.arguments.ArgumentStack;

import me.bernieplayshd.misset.plugin.manager.impl.internal.CommandManager;

import javax.annotation.Nullable;
import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/*
 * All credits go to https://github.com/foundry27
 * Just translated the messages and implemented Spigot support
 */

/**
 * @author Foundry
 */
public abstract class Command implements Labeled, Aliased {
    protected final String label;
    protected final String[] aliases;
    protected final CommandManager commandManager;
    protected final Set<CommandExecutor> executors = new HashSet<>();
    protected final String permission;

    protected Command(CommandManager manager, String label) {
        this(manager, label, null);
    }

    protected Command(CommandManager manager, String label, @Nullable String permission) {
        this.label = label.replaceAll(" ", "").toLowerCase();
        this.aliases = new String[0];//label.split("\\|");//Stream.of(aliases).map(s -> s.replaceAll(" ", "").toLowerCase()).toArray(x -> aliases);
        this.permission = permission;
        this.commandManager = manager;
        for (Class<?> clazz = this.getClass(); clazz.getSuperclass() != null; clazz = clazz.getSuperclass()) {
            for (Method method : clazz.getDeclaredMethods()) {
                Class returnType;
                Executes handler = method.getAnnotation(Executes.class);
                if (handler == null || (returnType = method.getReturnType()) != Void.TYPE
                        && returnType != String.class && returnType != String[].class && returnType != BaseComponent.class)
                    continue;
                try {
                    this.executors.add(new CommandExecutor(this, method, handler, commandManager));
                } catch (UnsupportedParameterException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public Object execute(CommandSender sender, ArgumentStack argumentStack) {
        String output = MissetPlugin.PREFIX + getFormattedUsage(sender, true);
        final ArgumentStack originalArgumentStack = argumentStack;
        executorBlock: for (CommandExecutor executor : this.executors) {
            CommandParameter parameter;
            argumentStack = originalArgumentStack.copy();
            subHandlerBlock : for (String sub : executor.getHandler().value()) {
                if (!argumentStack.hasNext()) continue executorBlock;
                final String argument = argumentStack.next();
                for (String part : sub.split("\\|")) {
                    if (!argument.equalsIgnoreCase(part)) {
                        continue;
                    }
                    argumentStack.remove();
                    continue subHandlerBlock;
                }
                continue executorBlock;
            }

            final CommandParameter[] parameters = executor.getParameters();
            final boolean[] presents = new boolean[parameters.length];
            final Object[] arguments = new Object[parameters.length + 1]; //+1
            arguments[0] = sender; //setting the sender as the first parameter
            for (int index = 0; index < arguments.length - 1; index++) {//-1
                parameter = parameters[index];
                final Flag flag = parameter.getAnnotation(Flag.class);
                if (flag == null) continue;
                String input = parameter.isBoolean() ? "false" : flag.def();
                boolean present = false;
                argStackBlock : while (argumentStack.hasNext()) {
                    final String argument = argumentStack.next();
                    for (String id : flag.value()) {
                        if (!argument.equalsIgnoreCase("-" + id)) continue;
                        present = true;
                        argumentStack.remove();
                        if (parameter.isBoolean()) {
                            input = "true";
                            break argStackBlock;
                        }
                        if (!argumentStack.hasNext()) break argStackBlock;
                        input = argumentStack.next();
                        argumentStack.remove();
                        break argStackBlock;
                    }
                }
                if (!present && parameter.isOptional()) {
                    presents[index] = true;
                    arguments[index + 1] = Optional.empty();//+1
                    continue;
                }
                try {
                    presents[index] = true;
                    final Object parsed = this.commandManager.findArgumentHandler(parameter.getType())
                            .orElseThrow(() -> new UnsupportedParameterException("unsupported parameter type found while executing command"))
                            .parse(commandManager, parameter, input);
                    arguments[index + 1] = parameter.isOptional() ? Optional.ofNullable(parsed) : parsed; //+1
                } catch (ArgumentParseException e) {
                    if (parameter.isOptional()) {
                        presents[index] = true;
                        arguments[index + 1] = Optional.empty();//+1
                    }
                    CommandParameter param = parameters[index];
                    output = String.format("%s§cKonnte Argument für Parameter §7'§e%s§7' (index: %s) §cnicht verarbeiten. §7(%s)"
                                    + System.lineSeparator() + String.format("%s§cSyntax: §7/%s %s", MissetPlugin.PREFIX, getLabel(), executor.getSyntax()),
                            MissetPlugin.PREFIX, param.getLabel(), index + 1, e.getMessage());
                    continue executorBlock;
                }
                argumentStack.setCursor(0);
            }
            for (int index = 0; index < arguments.length - 1; index++) { //-1
                if (presents[index]) continue;
                if (!argumentStack.hasNext()) {
                    while (index < arguments.length - 1) {//-1
                        parameter = parameters[index];
                        if (!parameter.isOptional()) continue executorBlock;
                        arguments[index++ + 1] = Optional.empty(); //+1
                    }
                    break;
                }
                try {
                    parameter = parameters[index];
                    final Object parsed = this.commandManager.findArgumentHandler(parameter.getType())
                            .orElseThrow(() -> new UnsupportedParameterException("unsupported parameter type found while executing command"))
                            .parse(commandManager, parameter, argumentStack.next());
                    arguments[index + 1] = parameter.isOptional() ? Optional.ofNullable(parsed) : parsed; //+1
                } catch (ArgumentParseException e) {
                    CommandParameter param = parameters[index];
                    output = String.format("%s§cKonnte Argument für Parameter §7'§e%s§7' (index: %s) §cnicht verarbeiten. §7(%s)"
                                    + System.lineSeparator() + String.format("%s§cSyntax: §7/%s %s", MissetPlugin.PREFIX, getLabel(), executor.getSyntax()),
                            MissetPlugin.PREFIX, param.getLabel(), index + 1, e.getMessage());
                    continue executorBlock;
                }
            }
            if (argumentStack.hasNext()) continue;
            String permission = executor.getHandler().permission();
            if (!permission.isEmpty() && !sender.hasPermission(permission)) {
                return MissetPlugin.NO_PERMISSIONS;
            }
            if (executor.isSenderPlayer() && !(sender instanceof Player)) {
                return MissetPlugin.PREFIX + "§cDieser Befehl kann nur als Spieler ausgeführt werden!";
            }
            return executor.execute(arguments);
        }
        return output;
    }

    public String[] getUsage() {
        return getUsage(null, null);
    }

    public String[] getUsage(@Nullable CommandSender sender, String nameBypass) {
        final String parsedLabel = nameBypass != null
                ? nameBypass
                : (label /*+ (aliases.length > 0 //dont need aliases
                    ? "|" + String.join("|", aliases)
                    : "")*/);
        final String prefix = String.format(MissetPlugin.PREFIX + "§7/%s", parsedLabel);
        return executors.stream()
                .filter(executor -> sender == null || !executor.getPermission().isPresent()
                        || sender.hasPermission(executor.getPermission().get()))
                .map(executor -> String.format("%s %s", prefix, executor.getSyntax()))
                .toArray(String[]::new);
    }

    public String[][] getUsageTree() {
        String[][] usageTree = new String[executors.size()][];
        int idx = 0;
        for (CommandExecutor executor : executors) {
            final List<String> executorSyntax = executor.getSyntaxTree();
            usageTree[idx++] = executorSyntax.toArray(new String[executorSyntax.size()]);
        }
        return usageTree;
    }

    public String getFormattedUsage(@Nullable CommandSender sender, boolean multiLine) {
        return getFormattedUsage(sender, null, multiLine);
    }

    public String getFormattedUsage(@Nullable CommandSender sender, String nameBypass, boolean multiLine) {
        final String[] unparsedSyntax = getUsage(sender, nameBypass);
        StringBuilder syntaxBuilder = new StringBuilder("§c§nSyntax§r§c: ").append(multiLine ? System.lineSeparator() : "");
        for (String element : unparsedSyntax) {
            syntaxBuilder.append(element).append(" ").append(multiLine ? System.lineSeparator() : "");
        }
        syntaxBuilder.setLength(syntaxBuilder.length() - (multiLine ? 3 : 2));
        return syntaxBuilder.toString();
    }

    public String getLabel() {
        return this.label;
    }

    public String[] getAliases() {
        return this.aliases;
    }

    public Set<CommandExecutor> getExecutors() {
        return this.executors;
    }

    @Nullable
    public String getPermission() {
        return permission;
    }
}

