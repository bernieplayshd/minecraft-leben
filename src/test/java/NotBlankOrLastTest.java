import static me.bernieplayshd.misset.plugin.Utils.notBlankOrLast;
import static org.junit.Assert.*;
import org.junit.Test;

public class NotBlankOrLastTest {

    @Test
    public void testNotBlankOrLast() {
        String[] toTest = { "", "jajaj  ", "testtest", "  ", "  " };
        assertArrayEquals(new String[] { "jajaj  ", "testtest", "  " }, notBlankOrLast(toTest));
    }
}
