import static me.bernieplayshd.misset.plugin.Utils.getLastNumber;
import static org.junit.Assert.*;
import org.junit.Test;

public class LastNumberTest {

    @Test
    public void testLastNumbers() {
        assertEquals(32, getLastNumber("32"));
        assertEquals(1884, getLastNumber("diniwd 1884"));
        assertEquals(100, getLastNumber("nfi100 dnhwid"));
        assertEquals(46, getLastNumber("25892dnwkdnkwdnwd46"));
        assertEquals(6, getLastNumber("dnuw06"));
        assertEquals(472, getLastNumber("jgnw -472"));
    }
}
