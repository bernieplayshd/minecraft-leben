import static me.bernieplayshd.misset.plugin.Utils.inventorySize;
import static org.junit.Assert.*;
import org.junit.Test;

public class InventorySizeTest {

    @Test
    public void testInventorySize() {
        assertEquals(9,  inventorySize(0));
        assertEquals(9,  inventorySize(4));
        assertEquals(36, inventorySize(36));
        assertEquals(54, inventorySize(90));
        assertEquals(27, inventorySize(20));
    }
}
