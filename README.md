MissetLeben
===========

## Building

*- Requires Java 8*

### Using Gradle

The plugin uses the [Gradle](http://gradle.org/) build automation system.
To build simply execute 

    gradle
    
in the projects root directory. *(If you do not have Gradle installed on your
system you can instead run the supplied Gradle wrapper `gradlew`)*

## Credits

- bernieplayshd
- [foundry27](https://github.com/foundry27) for the command system
  (earlier version of [Mandate](https://github.com/foundry27/Mandate))
